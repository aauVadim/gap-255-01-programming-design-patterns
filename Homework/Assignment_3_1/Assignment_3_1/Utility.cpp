// Utility.cpp
// Name: Vadim Osipov 
// Date: 9/25

#include "Utility.h"
#include "World.h"
#include <iostream>

Utility* Utility::m_instance = nullptr;

extern World* g_pWorld;

Utility* Utility::GetInstance()
{
	if (m_instance == nullptr)
	{
		m_instance = new Utility();
	}
	return m_instance;
}
//-------------------------------------------------------------------------------------------------
// Basic distance formula
//-------------------------------------------------------------------------------------------------
double Utility::GetDistance(int dX0, int dY0, int dX1, int dY1)
{
	return sqrt((dX1 - dX0)*(dX1 - dX0) + (dY1 - dY0)*(dY1 - dY0));
}
//-------------------------------------------------------------------------------------------------
// Converts X and Y to linear value
//-------------------------------------------------------------------------------------------------
int Utility::ConvertToLinear(int x, int y)
{
	int index = (y * g_pWorld->GetWorldWidth()) + x;
 	return index;
}
//-------------------------------------------------------------------------------------------------
// Converts X from linear value
//-------------------------------------------------------------------------------------------------
int Utility::ConvertX(int index)
{
	int posX = index / g_pWorld->GetWorldWidth();
	return posX;
}
//-------------------------------------------------------------------------------------------------
// Converts Y from linear value
//-------------------------------------------------------------------------------------------------
int Utility::ConvertY(int index)
{
	int posY = index % g_pWorld->GetWorldHeight();
	return posY; 
}