// EndTile.h
#ifndef __ENDTILE_H__
#define __ENDTILE_H__

//VO: Just to be safe
#pragma once

#include "Tile.h"

class EndTile : public Tile
{
public:
	EndTile();
    virtual void Draw() override;
    virtual void OnEnter(Actor* pPlayer) override;
};

#endif

