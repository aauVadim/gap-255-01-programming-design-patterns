// Utility.h
// Name: Vadim Osipov 
// Date: 9/25

#pragma once

class Utility
{
private: 
	static Utility* m_instance; 

public:
	static Utility* GetInstance();

	double GetDistance(int dX0, int dY0, int dX1, int dY1);
	int ConvertToLinear(int x, int y);
	int ConvertX(int index);
	int ConvertY(int index);

private: 
	Utility() { };
};