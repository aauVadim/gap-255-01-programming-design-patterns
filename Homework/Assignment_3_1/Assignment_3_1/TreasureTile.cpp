// TreasureTile.cpp
#include "TreasureTile.h"
#include "Actor.h"
#include <iostream>

const TreasureTile::TreasureRange TreasureTile::s_treasureRange(50, 150);

TreasureTile::TreasureTile()
    : m_collected(false)
{
    m_amount = (rand() % (s_treasureRange.second - s_treasureRange.first)) + s_treasureRange.first;
	m_tileType = TileType::k_interactible;
}

void TreasureTile::Draw()
{
    if (!m_collected)
        std::cout << "$";
    else
        std::cout << ".";  // floor
}

void TreasureTile::OnEnter(Actor* pActor)
{
    if (!m_collected)
    {
		pActor->AddGold(m_amount);
        m_collected = true;
    }
}

