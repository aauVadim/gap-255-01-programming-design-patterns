// Enemy_FreeWalk.h
// Name: Vadim Osipov
// Date: 9/25

#pragma once

#include "Enemy_Behavior.h"
#include "Enemy.h"
#include "Player.h"

class Enemy_FreeWalk : public Enemy_Behavior
{
public:
	
	Enemy_FreeWalk(Player* pPlayer, Enemy* pOurBody, int actionThreshold, int distanceToStopBeforePlayer)
		: Enemy_Behavior(pPlayer, pOurBody, actionThreshold, distanceToStopBeforePlayer) { };

	virtual void Act() override
	{
		//if distance between this and player more than action threshold - perform 
		if ((int)Utility::GetInstance()->GetDistance(m_pOurBody->GetX(), m_pOurBody->GetY(), m_pPlayer->GetX(), m_pPlayer->GetY()) > m_actionThreshold)
		{
			m_pOurBody->GetX() += ChooseX();
			m_pOurBody->GetY() += ChooseY();
		}
	}
	
	//Currenly crashes the game
	virtual int ChooseX() override
	{
		int triesCount = 0;

		while (triesCount <= 20)
		{
			int roll = (rand() % 3) - 1;
			
			++triesCount;

			if(IsMoveAllowed((m_pOurBody->GetX() + roll), m_pOurBody->GetY()) 
				&& g_pWorld->GetTile((m_pOurBody->GetX() + roll), (m_pOurBody->GetY()))->GetTileType() == Tile::TileType::k_walkable)
				return roll;
		}
		
		return 0;
	}

	//Currenly crashes the game
	virtual int ChooseY() override
	{
		int triesCount = 0;

		while (triesCount <= 10)
		{
			int roll = (rand() % 3) - 1;

			++triesCount;

			if (IsMoveAllowed((m_pOurBody->GetX()), (m_pOurBody->GetY() + roll))
				&& g_pWorld->GetTile((m_pOurBody->GetX()), (m_pOurBody->GetY() + roll))->GetTileType() == Tile::TileType::k_walkable)
				return roll;
		}
		return 0;
	}
};