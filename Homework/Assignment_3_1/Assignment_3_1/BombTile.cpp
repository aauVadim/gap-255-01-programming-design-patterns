// BombTile.cpp
#include "BombTile.h"
#include "Actor.h"
#include <iostream>


const BombTile::DamageRange BombTile::s_damageRange(3, 6);

BombTile::BombTile()
    : m_state(State::k_active)
{
    //For some reason it has to be here
	m_tileType = TileType::k_interactible;
}

void BombTile::Draw()
{
    if (m_state == State::k_active)
        std::cout << "*";
    else
        std::cout << "#";
}

void BombTile::OnEnter(Actor* pActor)
{
    if (m_state == State::k_active)
    {
        int damage = (rand() % (s_damageRange.second - s_damageRange.first)) + s_damageRange.first;
		pActor->Damage(damage);
        m_state = State::k_dead;
    }
}

