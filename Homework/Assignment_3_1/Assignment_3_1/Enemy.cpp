// Enemy.cpp
// Name: Vadim Osipov 
// Date: 9/25

#include "Enemy.h"
#include "Enemy_Behavior.h"
#include "Enemy_FreeWalk.h"
#include "Enemy_ChasePlayer.h"
#include "Enemy_RunAway.h"


#include <iostream>


//Super complecated constructor
Enemy::Enemy(int x, int y, bool isDefencive, Player* pPlayer, int distanceBeforeAction, int distanceToStopBeforePlayer)
	: Actor(x, y)
	, m_pPlayer(pPlayer)
	, m_isDefencive(isDefencive)
	, m_distanceBeforeAction(distanceBeforeAction)
	, m_distanceToStopBeforePlayer(distanceToStopBeforePlayer)
{

	Enemy_Behavior* newBehav = new Enemy_FreeWalk(m_pPlayer, this, m_distanceBeforeAction, m_distanceToStopBeforePlayer);
	
	AddBehavior(newBehav);
	
	m_isDefencive ? (newBehav = new Enemy_RunAway(m_pPlayer, this, m_distanceBeforeAction, m_distanceToStopBeforePlayer))
		: (newBehav = new Enemy_ChasePlayer(m_pPlayer, this, m_distanceBeforeAction, m_distanceToStopBeforePlayer));
	
	AddBehavior(newBehav);
}

Enemy::~Enemy()
{
	for (Enemy_Behavior* pBehav : m_behaviors)
	{
		delete pBehav;
	}
	m_behaviors.clear();
}

void Enemy::Draw() const
{
	if (!IsDead())
		if (m_isDefencive)
			std::cout << "?";	//Defencive enemy display
		else
			std::cout << "!";	//Offencive enemy display
	else
		std::cout << "~";
}

//-------------------------------------------------------------------------------------------------
// Moves enemy. AI Part. Proper behavior for the distance to the player will be triggered.
//-------------------------------------------------------------------------------------------------
bool Enemy::Update()
{
	//Checking if still alive 
	if (IsDead())
		return false;
	//Run every neccesary Act on enemy
	for (Enemy_Behavior* pBehav : m_behaviors)
	{
		pBehav->Act();
	}
	return true;
}

void Enemy::AddBehavior(Enemy_Behavior* newBehav)
{
	m_behaviors.push_back(newBehav);
}