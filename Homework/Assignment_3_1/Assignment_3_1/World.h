// World.h
#ifndef __WORLD_H__
#define __WORLD_H__

#include <utility>

class Tile;
class Player;
class Enemy;

class World
{
    enum class TileType
    {
        k_floor,
        k_bomb,
        k_treasure,
        k_mimic,
        k_numTiles,
    };

    typedef std::pair<int, TileType> TileProbability;

    static const TileProbability s_tileProbabilities[(int)TileType::k_numTiles];
   
	static const int s_kMaxNumberOfEnemies;

    int m_width, m_height;
	Tile** m_ppGrid;
	Player* m_pPlayer;
	//Enemy* m_pEnemiesArray;
	Enemy** m_ppEnemiesArray;
    bool m_gameOver;

public:
	World();
	~World();

	//Utility - Returns the size of the map. Used for Enemies AI
	int GetWorldHeight()	const	{	return m_height;	}
	int GetWorldWidth()		const	{	return m_width;		}

	// initialization
	void Init(int width, int height);
	void CreatePlayer(int x = 0, int y = 0);
	void GenerateWorld();
	void RollForEnemy(int position);
	// update
	void Draw();
	void Update();

	//Returning the tile
	Tile* GetTile(int x, int y);

    // end
    void EndGame();
    bool IsGameOver() const { return m_gameOver; }
};

#endif
