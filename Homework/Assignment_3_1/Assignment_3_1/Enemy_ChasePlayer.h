// Enemy_ChasePlayer.h
// Name: Vadim Osipov
// Date: 9/25

#pragma once

#include <iostream>

#include "Enemy_Behavior.h"
#include "Enemy.h"
#include "Player.h"

class Enemy_ChasePlayer : public Enemy_Behavior
{
public:
	Enemy_ChasePlayer(Player* pPlayer, Enemy* pOurBody, int actionThreshold, int distanceToStopBeforePlayer)
		: Enemy_Behavior(pPlayer, pOurBody, actionThreshold, distanceToStopBeforePlayer) { };

	virtual void Act() override
	{
		//Distance to player
		int distance = (int)Utility::GetInstance()->GetDistance(m_pOurBody->GetX(), m_pOurBody->GetY(), m_pPlayer->GetX(), m_pPlayer->GetY());
		//If youre whithin your act range but not enter player tile
		if (distance <= m_actionThreshold && distance > m_distanceToStopBeforePlayer)
		{
			m_pOurBody->GetX() += ChooseX();
			m_pOurBody->GetY() += ChooseY();
		}
	}

	virtual int ChooseX() override
	{
		int deltaX = 0;

		if (m_pOurBody->GetX() != m_pPlayer->GetX())
		{
			if (m_pOurBody->GetX() > m_pPlayer->GetX())
			{
				if (IsMoveAllowed(m_pOurBody->GetX() - 1, m_pOurBody->GetY()))
					return deltaX = -1;	//Run toward the player
			}
			else if (m_pOurBody->GetX() < m_pPlayer->GetX())
			{
				if (IsMoveAllowed(m_pOurBody->GetX() + 1, m_pOurBody->GetY()))
					return deltaX = 1; //Run toward the player
			}
		}

		return deltaX; 
	}

	virtual int ChooseY() override
	{
		int deltaY = 0;

		if (m_pOurBody->GetY() != m_pPlayer->GetY())
		{
			if (m_pOurBody->GetY() > m_pPlayer->GetY())
			{
				if (IsMoveAllowed(m_pOurBody->GetX(), m_pOurBody->GetY() - 1))
					return deltaY = -1;	//Run toward the player
			}
			else if (m_pOurBody->GetY() < m_pPlayer->GetY())
			{
				if (IsMoveAllowed(m_pOurBody->GetX(), m_pOurBody->GetY() + 1))
					return deltaY = 1; //Run toward the player
			}
		}

		return deltaY;
	}
};