// Enemy_Behavior.h
// Name: Vadim Osipov
// Date: 9/25

#pragma once
#include "Utility.h"
#include "Tile.h"
#include "World.h"
//Foward declaring
class Player;
class Enemy;

//Allowing our enemies to read tthe grid
extern World* g_pWorld;

class Enemy_Behavior
{
protected:
	Enemy* m_pOurBody;
	Player* m_pPlayer;

public:
	int m_actionThreshold;
	//Stop before player distance
	int m_distanceToStopBeforePlayer;

	Enemy_Behavior(Player* pPlayer, Enemy* pOurBody, int actionThreshold, int distanceToStopBeforePlayer)
		: m_pPlayer(pPlayer)
		, m_actionThreshold(actionThreshold)
		, m_pOurBody(pOurBody)
		, m_distanceToStopBeforePlayer(distanceToStopBeforePlayer)
	{

	};


	virtual void Act() = 0;

protected:

	virtual int ChooseX() = 0;
	virtual int ChooseY() = 0;

	bool IsMoveAllowed(int desiredX, int desiredY)
	{
		if (desiredX >= 0 && desiredX < g_pWorld->GetWorldHeight()
			&& desiredY >= 0 && desiredY < g_pWorld->GetWorldWidth())
			return true;
		else
			return false;
	};
	

	Tile::TileType GetTileType()
	{
	};
};