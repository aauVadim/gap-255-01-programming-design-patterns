// AIPlayer.cpp
// Name: Vadim Osipov
// Date: 9/15

#pragma once

#include "Player.h"
#include "HumanPlayer.h"

struct HitTiles
{
	int hitPosX; 
	int hitPosY;
	//Just making sure it's -1 :)
	HitTiles() {}  //[rez] You should initialize your variables here.
};

class AIPlayer : public Player
{
	//Storing hit tiles
	HitTiles* m_hitTilesArray;
public:
	AIPlayer();
	//Initializing AI Player
	void Initialize();
	//AI Part - Choosing where to shoot
	void TakeAction(HumanPlayer* humanPlayer);
};

