// AIPlayer.cpp
// Name: Vadim Osipov
// Date: 9/15

//#define DEBUG

#include "AIPlayer.h"
#ifdef DEBUG
#include <iostream>
#include <conio.h>
#endif

//---------------------------------------------------------------------------------------------------------------------
// Takes hit atgive player field. 
//---------------------------------------------------------------------------------------------------------------------
void AIPlayer::TakeAction(HumanPlayer* humanPlayer)
{


	//Pure random, thanks Rez
	int randPosX = RandRange(1, m_actualSizeX);
	int randPosY = RandRange(1, m_actualSizeY);

	//
	for (int i = 0; i < (m_actualSizeX * m_actualSizeY); ++i)
	{
		
		//If this record exists
		if (m_hitTilesArray[i].hitPosX != randPosX && m_hitTilesArray[i].hitPosY != randPosY)
		{
			if (m_hitTilesArray[i].hitPosX == -1 && m_hitTilesArray[i].hitPosY == -1)
			{
				m_hitTilesArray[i].hitPosX = randPosX;
				m_hitTilesArray[i].hitPosY = randPosY;
				break;
			}
		}
		//Check if you have this stored already, if so find new numbers
		else if (m_hitTilesArray[i].hitPosX == randPosX && m_hitTilesArray[i].hitPosY == randPosY)
		{
			randPosX = RandRange(1, (m_actualSizeX - 1));
			randPosY = RandRange(1, (m_actualSizeY - 1));
			continue;
		}
	}
	//Send hit to the player
	humanPlayer->ProcessHit(randPosX, randPosY);

#ifdef DEBUG
	std::cout << "AI Player hits:\n";
	for (int i = 0; i < (m_actualSizeX * m_actualSizeY); ++i)
	{
		if (m_hitTilesArray[i].hitPosX != -1 && m_hitTilesArray[i].hitPosY != -1)
		{
			std::cout <<"Record: " << i << " Pos X: " << m_hitTilesArray[i].hitPosX << " Pos Y: " << m_hitTilesArray[i].hitPosY << std::endl;
		}
		else
			break;
	}
	_getch();
#endif
}

AIPlayer::AIPlayer() 
	: Player()
{

}
//---------------------------------------------------------------------------------------------------------------------
// Used to initialize the AI player. Currenly used just for array.
//---------------------------------------------------------------------------------------------------------------------
void AIPlayer::Initialize()
{
	m_hitTilesArray = new HitTiles[m_actualSizeX * m_actualSizeY];
	for (int i = 0; i < (m_actualSizeX * m_actualSizeY); ++i)
	{
		m_hitTilesArray[i].hitPosX = -1;
		m_hitTilesArray[i].hitPosY = -1;
	}
}