// MapTile.cpp
// Name: Vadim Osipov 
// Date: 9/15

//Uncomment to debug ships
//#define DEBUGGING_SHIPS

#include "MapTile.h"

MapTile::MapTile()
{
	//By defaut
	m_isShip = false;
	m_isNumber = false;
	m_hasBeenShotAt = false;
	m_thisDisplayChar = '.';
	m_posX = 0;
	m_posY = 0;
}


//---------------------------------------------------------------------------------------------------------------------
// Helper function. Returns proper char for this current tile. Can be: '@' - Ship, '.' - Field, 'X' - Hit, 'O' - Miss
//---------------------------------------------------------------------------------------------------------------------
char& MapTile::GetTileChar(bool isOurTurn)
{
	if (m_isNumber)
	{
		if (m_posX == 0)
		{
			if (m_posY < 10)
				return m_thisDisplayChar = '0' + m_posY;	//Ductape
			else
				return m_thisDisplayChar = 2;	//didnt have time for double dijut numbers
		}
		if (m_posY == 0)
		{
			if (m_posX < 10)
				return m_thisDisplayChar = '0' + m_posX;	//Ductape
			else
				return m_thisDisplayChar = 1;	//didnt have time for double dijut numbers
		}
	}
	else
	{
		if (isOurTurn)
		{
			if (m_hasBeenShotAt && !m_isShip)	//If you have been shot at and you're not a ship
				return m_thisDisplayChar = 'O';
			if (m_hasBeenShotAt && m_isShip)	//If you're a ship and youve been shot at 
				return m_thisDisplayChar = 'X';
			if (!m_hasBeenShotAt && m_isShip)	//if you havent been shot at and youre a ship
				return m_thisDisplayChar = '@';
			if (!m_hasBeenShotAt && !m_isShip)	//if you havent been shot at and youre no a ship
				return m_thisDisplayChar = '.';
		}
		else
		{
			if (m_hasBeenShotAt && !m_isShip)	//If you have been shot at and you're NOT a ship
				return m_thisDisplayChar = 'O';
			if (m_hasBeenShotAt && m_isShip)	//If you're a ship and youve been shot at 
				return m_thisDisplayChar = 'X';
			if (!m_hasBeenShotAt && !m_isShip)	//if you havent been shot at and youre NOT a ship
				return m_thisDisplayChar = '.';
#ifdef DEBUGGING_SHIPS
			if (!m_hasBeenShotAt && m_isShip)	//if you havent been shot at and youre a ship. Debuggin. Enemmy ship
				return m_thisDisplayChar = '#';
#else
			if (!m_hasBeenShotAt && m_isShip)	//if you havent been shot at and youre a ship
				return m_thisDisplayChar = '.';
#endif
		}
	}
	return m_thisDisplayChar;
}
//---------------------------------------------------------------------------------------------------------------------
// Helper function. Sets this tile to a number with some coordinates
//---------------------------------------------------------------------------------------------------------------------
void MapTile::SetThisToNumber(int posX, int posY)
{
	m_isNumber = true;
	m_posX = posX;
	m_posY = posY;
}

//---------------------------------------------------------------------------------------------------------------------
// Accsessor - Allows game to set his bool. Passed as Reference. It made sence at the time. 
//---------------------------------------------------------------------------------------------------------------------
bool& MapTile::HasBeenHit()
{
	return m_hasBeenShotAt;
}
