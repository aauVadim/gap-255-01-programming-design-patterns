// Assignment_2_1.cpp

// [rez] Grade: C+
// There's a lot of great stuff in here, but you missed the mark on subclass delegation.  There was no 
// subclass delegation at all.  That having been said, you did get everything working including the 
// extra credit stuff.  The big thing that was lacking was subclass delegation.  Take a look at my comments
// for specific notes.

//---------------------------------------------------------------------------------------------------------------------
// Instructions:
// 
// 1) Refactor this code to encapsulate all of the current functionality into a singleton.  Feel free to modify any 
//    of the code or break it up into separate files.  You can also use multiple classes if you want, but there must
//    be at least one singleton.
// 2) Add a computer controlled player that will fire a random shot on their turn.  This should be done using subclass
//    delegation.
// 3) Right now, the ship is always displayed.  Modify the game so that the ship is hidden view.  Each tile should 
//    display '.' for unattacked tiles, 'X' for a hit tile, and 'O' for a missed tile.
// 4) The game should end when the ship is destroyed.
// 5) Make sure you clean up the code.  Refactoring can create messy code if you don't clean it up.  There are also 
//    a number of things subtley wrong with this project.  See how many you can spot and fix.
// 
// Bonus:
//    *: Prevent the enemy AI from attacking the same spot.
// ****: Create a second grid.  The human player attacks the second grid while the computer player attacks the first.
//---------------------------------------------------------------------------------------------------------------------

#include <iostream>
#include <conio.h>
#include <time.h>
#include "World.h"

//#define DEBUGGIN_BOTS
//#define DEBUGGING_MULTI
#define RELEASE

using namespace std;

//---------------------------------------------------------------------------------------------------------------------
// main() function
//---------------------------------------------------------------------------------------------------------------------
void main()
{
	
	srand((unsigned int)time(0));

	cout << "Battleship!\n\n";
    // [rez] Creating these kinds of guards can be dangerous.  I'm not saying you shouldn't do it, just 
    // be careful creating a path that you're not testing as much.  What you have here seems fine since it's 
    // pretty simple.  Just be aware that placing guards against different build modes can be dangerous.  
    // Also, I would call it something other than RELEASE since that can be confused with debug vs release
    // modes.
#ifdef RELEASE
	//UX Sugar ---
	int numOfPlayers = 0;
	cout << "Welcome! Please enter number of human players ";
	cin >> numOfPlayers;

	int sizeX = 0;
	cout << "Please enter the Width of the Game Field (But please be reasonable) "; 
	cin >> sizeX;

	int sizeY = 0;
	cout << "Please enter the Height of the Game Field (But please be reasonable) ";
	cin >> sizeY;

	if (numOfPlayers > 1)
		cout << "Player 1 will go first - Player 2 please dont look \nPress any key when you're ready to play\n";
	else if (numOfPlayers == 1)
		cout << "--- Human vs. Computer player ---\n--- Press any key to begin the game ---\n";
	//Showstopper, taking time gor game to start
	_getch();
	//UX Sugar ---

	World::Instance()->CreateWorld(sizeX, sizeY, numOfPlayers);
#endif
	//Instantly creates game with bots
#ifdef DEBUGGIN_BOTS
	World::Instance()->CreateWorld(10, 10, 1);
#endif
	//Instantly creates 3 people multiplayer game
#ifdef DEBUGGING_MULTI
	World::Instance()->CreateWorld(10, 10, 3);
#endif
	//Main game loop - will run untill someone wins the game :) 
	while (World::Instance()->IsGameRunning())
	{
		cout << "Player: " << World::Instance()->GetCurrentPlayerUp() << " Enter input\n"; 

		//Locals
		int posX, posY; 
		//Taking input
		std::cout << "Enter value X: ";
		std::cin >> posX;
		std::cout << "Enter value Y: ";
		std::cin >> posY;
		//Pass input tio the Players
		World::Instance()->PlayerInput(posX, posY);
	}

	cout << "Player: " << World::Instance()->GetCurrentPlayerUp() << " won the game!\n";
	system("cls");
	cout << "\n\nThanks for playing!\n";
	_getch();
}




