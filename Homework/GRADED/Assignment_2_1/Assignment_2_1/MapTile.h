// MapTile.h
// Name: Vadim Osipov 
// Date: 9/15

#pragma once


class MapTile
{
	//Positions
	int m_posX, m_posY;
	//Helper bool - if was hit
	bool m_hasBeenShotAt;	
	char m_thisDisplayChar;

public:

    // [rez] TODO: It took me a while to figure out this m_isNumber thing.  This is a complete hack to 
    // get your grid values to render.  I think you have an interesting idea to treat them as tiles, but 
    // if you're going to make that decision, you should do it right.  This is a perfect place to have 
    // a subclass for the number tiles.  Then, GetTileChar() can be a virtual function that has different
    // behavior depending on what kind of tile it is.
	bool m_isNumber;	//Number
	bool m_isShip;		//Ship
	//Cosntructor
	MapTile();
	//Returns the proper Tile Char 
	char& GetTileChar(bool isOurTurn);  // [rez] Why is this returning a reference?  If it's a primitive, just return the value.
	//Returns if the tile was hit or not 
	bool& HasBeenHit();  // [rez] Why is this returning a reference?  If it's a primitive, just return the value.
	//Setting this tile to a number on the side of the field
	void SetThisToNumber(int posX, int posY);
};