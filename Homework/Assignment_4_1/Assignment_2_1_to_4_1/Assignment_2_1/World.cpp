//World.cpp
//Name: Vadim Osipov
//Date: 9/15

#pragma once

#include "World.h"


#include <iostream>
#include <conio.h>

World* World::m_pInstance = nullptr;

World::World() 
	: m_isGameRunning(true)
	, m_ppPlayersArray(nullptr)
{

}

World::~World()
{
	if (m_pInstance)
		delete m_pInstance;
	if (m_pAIPlayer)
		delete m_pAIPlayer;
	if (m_ppPlayersArray)
		delete[] m_ppPlayersArray;
}

World* World::Instance(){
	if (!m_pInstance)
	{
		m_pInstance = new World;
	}
	return m_pInstance;
}

//---------------------------------------------------------------------------------------------------------------------
// Creates the world with given grid size and number of players
//---------------------------------------------------------------------------------------------------------------------
void World::CreateWorld(int sizeX, int sizeY, int numOfNumanPlayers)
{

	m_ppPlayersArray = new Player*[numOfNumanPlayers];
	//Initializing the player array 
	//m_pHPlayerArray = new HumanPlayer[numOfNumanPlayers];
	m_numberOfPlayers = numOfNumanPlayers;
	//Storing this for future reference
	m_boardSizeX = sizeX + 1;
	m_boardSizeY = sizeY + 1;

	if (numOfNumanPlayers <= 0)
	{
		std::cout << "Have to have atleast one Human Player" << std::endl;
	}
	else if (numOfNumanPlayers == 1) // Game with bots
	{
		CreateGameWithBots(sizeX, sizeY);
	}
	else if (numOfNumanPlayers > 1)
	{
		CreateMultiplayerGame(sizeX, sizeY, numOfNumanPlayers);
	}
	//Draw a board for human players
	DrawBoard(sizeX, sizeY, numOfNumanPlayers, m_ppPlayersArray, false);
}

void World::CreateGameWithBots(int sizeX, int sizeY)
{
	m_isGameWithBots = true;
	
	//Setting this +1 for the bot to work with rest of the code.
	//Kinda ducttapy of me
	m_numberOfPlayers += 1; 
	//Setting Human
	m_ppPlayersArray[0] = new HumanPlayer(1);
	m_ppPlayersArray[0]->m_isMyTurn = true;

	//Wonky... I hope that is the right way of doing it.
	m_ppPlayersArray[0]->SetPlayerBattlefield(CreateGrid(sizeX, sizeY));
	//Setting bot
	m_ppPlayersArray[1] = new AIPlayer;
	//Wonky... I hope that is the right way of doing it.
	m_ppPlayersArray[1]->SetPlayerBattlefield(CreateGrid(sizeX, sizeY));
}

void World::CreateMultiplayerGame(int sizeX, int sizeY, int numOfHuman)
{
	m_isGameWithBots = false;
	//For every human player - create class, make map 
	for (int i = 0; i < numOfHuman; ++i)
	{
		m_ppPlayersArray[i] = new HumanPlayer(i + 1);
		//Giving the turn to go to first player
		if (i == 0)
			m_ppPlayersArray[i]->m_isMyTurn = true;
		//Creates a grid for every human
		m_ppPlayersArray[i]->SetPlayerBattlefield(CreateGrid(sizeX, sizeY));
	}
}

//---------------------------------------------------------------------------------------------------------------------
// Processses player input. Responsible for switching screens between players
//---------------------------------------------------------------------------------------------------------------------
void World::PlayerInput(int hitPosX, int hitPosY)
{
	for (int i = 0; i < m_numberOfPlayers; ++i)
	{
		//If we have a current player that is not eliminated
		if (m_ppPlayersArray[i]->m_isMyTurn && !m_ppPlayersArray[i]->m_isEliminated)
		{
			//Choosing next player
			int nextPlayer = ((i + 1) < m_numberOfPlayers) ? (i + 1) : 0;
				
			//Check before we fire. 
			int playerCheckOne = CheckIfPlayersIsDirty(m_ppPlayersArray, m_numberOfPlayers, i, nextPlayer);
				
			DrawBoard(m_boardSizeX, m_boardSizeY, m_numberOfPlayers, m_ppPlayersArray, false);
			//Letting next in line know that he was hit
			if (m_ppPlayersArray[playerCheckOne]->ProcessHit(hitPosX, hitPosY, m_ppPlayersArray[i]))
			{
				//Check after we fire
				int newNextPlayer = CheckIfPlayersIsDirty(m_ppPlayersArray, m_numberOfPlayers, i, playerCheckOne);
				//Redraw the board with the hit
				DrawBoard(m_boardSizeX, m_boardSizeY, m_numberOfPlayers, m_ppPlayersArray, true);
				//Switch players
				SwitchPlayers(m_ppPlayersArray[i], m_ppPlayersArray[newNextPlayer]);
				break;
			}
		}
	}
	
}

//---------------------------------------------------------------------------------------------------------------------
// Draws the grid for Human vs. Human game. 
//---------------------------------------------------------------------------------------------------------------------
void World::DrawBoard(int sizeX, int sizeY, int numOfHumanPlayers, Player** pp_playersArray, bool didTakeTurn)
{
	using namespace std;
	
	system("cls");

	for (int player = 0; player < numOfHumanPlayers; ++player)
	{
		if (!pp_playersArray[player]->m_isEliminated)
		{
			pp_playersArray[player]->DrawMap();
		}
	}
	//Keeping the screen up for a second - to show player where/what he hit
	if (didTakeTurn)
	{
		cout << "\n--- Press any key to continue ---\n";
		_getch();
	}
}

//---------------------------------------------------------------------------------------------------------------------
// Utility bool. Returns if game is running or not dependant on how many players are still up
//---------------------------------------------------------------------------------------------------------------------
bool World::IsGameRunning()
{
	int numberOfActivePlayers = 0;
	for (int i = 0; i < m_numberOfPlayers; ++i)
	{
		if (m_ppPlayersArray[i]->GetShipPiecesLeft() >= 1)
			++numberOfActivePlayers;
	}

	if (numberOfActivePlayers == 1)
		return false;
	else
		return true;
}

//---------------------------------------------------------------------------------------------------------------------
// Utility int. Returns current number of players in the game.
//---------------------------------------------------------------------------------------------------------------------
int World::GetCurrentPlayerUp()
{
	for (int i = 0; i < m_numberOfPlayers; ++i)
	{
		if (m_ppPlayersArray[i]->m_isMyTurn && !m_ppPlayersArray[i]->m_isEliminated)
			return i + 1;
	}

	return 0;
}
//---------------------------------------------------------------------------------------------------------------------
// Switches player screens and turns.
//---------------------------------------------------------------------------------------------------------------------
void World::SwitchPlayers(Player* p_currentPlayer, Player* p_nextPlayer)
{
	using namespace std;
	if (IsGameRunning())
	{
		//Do not do this logic for game with AI
		if (!m_isGameWithBots)
		{
			p_currentPlayer->m_isMyTurn = false;
			p_nextPlayer->m_isMyTurn = true;

			system("cls");

			cout << "Player: " << GetCurrentPlayerUp() << " turn.\n";
			cout << "Player: " << GetCurrentPlayerUp() << " --- Press anykey when ready ---\n";
			_getch();
			//Drawing board for next player up
			DrawBoard(m_boardSizeX, m_boardSizeY, m_numberOfPlayers, m_ppPlayersArray, false);

		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
// Checks if next player is not out. If he is out, assigns in to next avalible player
//---------------------------------------------------------------------------------------------------------------------
int World::CheckIfPlayersIsDirty(Player** pp_playerArray, int playersCount, int currentPlayerPos, int nextPlayerPos)
{ 
	int newNextPlayer = nextPlayerPos; 

	//If current next player is eliminated
	if (pp_playerArray[newNextPlayer]->m_isEliminated)
	{
		//Choosing where to stop our loop
		int stopPos = (nextPlayerPos == (currentPlayerPos + 1)) ? playersCount : currentPlayerPos;
		
		for (int i = nextPlayerPos; i < stopPos; ++i)
		{
			if (!pp_playerArray[i]->m_isEliminated)
			{
				return newNextPlayer = i;
			}
			else if (i == (playersCount - 1))	//Just for last player up
			{
				//If you at the end and did not find anything - go from beginning? 
				for (int j = 0; j < currentPlayerPos; ++j)
				{
					if (!pp_playerArray[j]->m_isEliminated)
					{
						return newNextPlayer = j;
					}
				}
			}
		}
	}
	else
		return newNextPlayer;

	return 0;
}

GameField* World::CreateGrid(int sizeX, int sizeY)
{
	GameField* newGameField = new GameField;

	newGameField->CreateGrid(sizeX, sizeY);

	return newGameField;
}
