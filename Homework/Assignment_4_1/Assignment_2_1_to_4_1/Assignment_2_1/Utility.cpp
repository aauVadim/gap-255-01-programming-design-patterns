#include "Utility.h"

Utility* Utility::m_pInstance = nullptr;

Utility* Utility::GetInstance()
{
	if (!m_pInstance)
		m_pInstance = new Utility;
	return m_pInstance;
};

int Utility::RandRange(int minValue, int maxValue)
{
	int val = (rand() % (maxValue - minValue)) + minValue;
	return val;
};