// AIPlayer.cpp
// Name: Vadim Osipov
// Date: 9/15

#pragma once

#include "Player.h"
#include "HumanPlayer.h"

struct HitTiles
{
	int m_hitPosX; 
	int m_hitPosY;

	HitTiles() : m_hitPosX(-1), m_hitPosY(-1) { };  //[rez] You should initialize your variables here.
	// [vo] thanks
};

class AIPlayer : public Player
{
	//Storing hit tiles
	HitTiles* m_hitTilesArray;
public:
	AIPlayer();
	//Initializing AI Player
	void Initialize();
	//AI Part - Choosing where to shoot
	void TakeAction(Player* p_humanPlayer);
	//Override for AI Player to take action right after he was hit
	virtual bool ProcessHit(int posX, int posY, Player* p_playerRef) override;

	virtual void DrawMap() override;

	virtual void SetPlayerBattlefield(GameField* p_gameField)	override;
};

