//World.h
//Name: Vadim Osipov
//Date: 9/15

#pragma once

#include "MapTile.h"
#include "HumanPlayer.h"
#include "AIPlayer.h"

#include "GameField.h"

//SINGLETON! 
class World
{
	//Singleton pointer
	static World *m_pInstance;

	bool m_isGameRunning; 

	//Human player array
	Player** m_ppPlayersArray;
	int m_numberOfPlayers;
	//AI Pointer
	AIPlayer* m_pAIPlayer;
	
	bool m_isGameWithBots;
	//storing the board size
	int m_boardSizeX, m_boardSizeY;

public:
	//returns the instance of this 
	static World* Instance();
	
	//Public interface to creatre the world
	void CreateWorld(int sizeX, int sizeY, int numOfNumanPlayers);
	//Handles player input
	void PlayerInput(int posX, int posY);
	//Utility - main loop run on this
	bool IsGameRunning();
	//Utility - returns the number of current player
	int GetCurrentPlayerUp();

private:
	//Constructor
	World();
	~World();
	
	GameField* CreateGrid(int sizeX, int sizeY);

    // [rez] No, you didn't need to overload these.  If you want different behavior based on the type of 
    // of game, you should have created two subclasses, one for human vs human and one for human vs AI.
    // The game can create the appropriate instance based on the type of game.  DrawBoard() becomes a 
    // virtual function with different implementations.
	// [vo] Got rig of overloading by using one unified array of players. Now it works with one function. 
	void DrawBoard(int sizeX, int sizeY, int numOfHumanPlayers, Player** pp_playersArray, bool isTurnOver);

	void SwitchPlayers(Player* p_currentPlayer, Player* p_nextPlayer);

	int CheckIfPlayersIsDirty(Player** pp_playerArray, int playersCount, int currentPlayerPos, int nextPlayerPos);
	//Helper function to keep code separate
	void CreateGameWithBots(int sizeX, int sizeY);
	//Helper function to keep code separate
	void CreateMultiplayerGame(int sizeX, int sizeY, int numOfHuman);
};
