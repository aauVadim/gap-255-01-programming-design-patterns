// MapTile.h
// Name: Vadim Osipov 
// Date: 9/15

#pragma once

#include "Tile_Type.h"

class MapTile
{
protected: 
	
	TileType* m_pMyBehavior;

public:

	enum class TileTypeEnum
	{
		k_ship, 
		k_number, 
		k_empty
	};
	//I might not even need this.
	TileTypeEnum m_thisTileType;

	//Factory, I hope? 
	void CreateTile(TileTypeEnum tileType, int posX, int posY);

    // [rez] TODO: It took me a while to figure out this m_isNumber thing.  This is a complete hack to 
    // get your grid values to render.  I think you have an interesting idea to treat them as tiles, but 
    // if you're going to make that decision, you should do it right.  This is a perfect place to have 
    // a subclass for the number tiles.  Then, GetTileChar() can be a virtual function that has different
    // behavior depending on what kind of tile it is.
	//[vo] yeah - m_isNumber was kinda of a ductape. Got rid of it with tile behaviors

	//Cosntructor
	MapTile();

	// [vo] - sorry - reference was there for some stupid debug purpose. Not anymore
	//char& GetTileChar(bool isOurTurn);	// [rez] Why is this returning a reference?  If it's a primitive, just return the value.
	//cant make it a pure virtual - making an single pointer array of this class

	//Calls it behavior to draw.
	virtual void Draw(bool isOurTurn) { if(m_pMyBehavior) m_pMyBehavior->Draw(isOurTurn); };		

	//Accsessors.
	// [vo] sorry for using references when not needed. Used for easy accsess to things. Wont do again. 
	bool SetHasBeenHit(bool hit) { return m_pMyBehavior->HasBeenHit(hit); };  // [rez] Why is this returning a reference?  If it's a primitive, just return the value.
	bool GetHasBeenHit() { return m_pMyBehavior->HasBeenHit(); }	

};