// Tile_Number.h
// Name: Vadim Osipov 
// Date: 10/3

#pragma once

#include "Tile_Type.h"

#include <iostream>

using std::cout;
using std::endl;

class TileNumber : public TileType
{
public:
	virtual void Draw(bool isOurTurn) override
	{
		if (m_posX == 0 && m_posY == 0)
		{ 
			cout << '#';
		}
		else if (m_posX == 0 && m_posY != 0)
		{
			if (m_posY < 10)
				cout << m_posY;
			else
				cout << (char)2;
		}
		else if (m_posY == 0 && m_posX != 0)
		{
			if (m_posX < 10)
				cout << m_posX;
			else
				cout << (char)1;
		}
	}
};