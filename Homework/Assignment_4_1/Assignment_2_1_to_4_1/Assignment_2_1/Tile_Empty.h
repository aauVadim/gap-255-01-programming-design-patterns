// Tile_Empty.h
// Name: Vadim Osipov 
// Date: 10/3

#include "Tile_Type.h"

#include <iostream>

using std::cout;
using std::endl;

class TileEmpty : public TileType
{
public:
	virtual void Draw(bool isOurTurn) override
	{
		if (m_hasBeenShotAt)
			cout << 'O';
		else
			cout << '.'; 
	}
};