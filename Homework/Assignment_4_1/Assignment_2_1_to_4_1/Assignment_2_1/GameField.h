// GameField.h
// Name: Vadim Osipov
// Date: 10/2

#pragma once

class MapTile;

class GameField
{
	MapTile** m_ppGrid;

	int m_actualSizeX;
	int m_actualSizeY;

public: 
	bool CreateGrid(int sizeX, int sizeY);
	//Utility - so Player doesnt have to save this
	int GetMapSizeX() { return m_actualSizeX; };
	int GetMapSizeY() { return m_actualSizeY; };

	void DrawField(bool isMyTurn);
	int GetShipPiecesLeft();

	void ProcessHit(int posX, int posY);

private: 
	bool InitializeGrid();

	void SpawnBattleship();
};