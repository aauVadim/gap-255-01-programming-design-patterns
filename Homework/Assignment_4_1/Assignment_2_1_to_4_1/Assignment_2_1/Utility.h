// Utility.h
// Name: Vadim Osipov 
// Date: 10/2

#pragma once

#include <iostream>

class Utility
{
	static Utility* m_pInstance;
	
public:
	static Utility* GetInstance();

	int RandRange(int minValue, int maxValue);

private:
	Utility() { };
	~Utility() { delete m_pInstance; };
};