// Tile_Ship.h
// Name: Vadim Osipov 
// Date: 10/3

#include "Tile_Type.h"

#include <iostream>

using std::cout;
using std::endl;

class TileShip : public TileType
{
public:
	virtual void Draw(bool isOurTurn) override
	{
		if (isOurTurn)
		{
			if (m_hasBeenShotAt)
				cout << 'X';
			else
				cout << '@';
		}
		else
		{
			if (m_hasBeenShotAt)
				cout << 'X';
			else
				cout << '.';
		}
	}
};