// Player.cpp
// Name: Vadim Osipov 
// Date: 9/15

#include "Player.h"
#include "GameField.h"
#include <iostream>

using namespace std;

Player::Player()
	: m_isMyTurn(false)
	, m_isEliminated(false)
{

}


//---------------------------------------------------------------------------------------------------------------------
// Utility - Returns amount of ship pieces left on the field. 
//---------------------------------------------------------------------------------------------------------------------
int Player::GetShipPiecesLeft()
{
	return m_pMyBattleField->GetShipPiecesLeft();
}