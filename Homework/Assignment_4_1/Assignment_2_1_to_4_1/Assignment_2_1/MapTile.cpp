// MapTile.cpp
// Name: Vadim Osipov 
// Date: 9/15

#include "MapTile.h"

#include "Tile_Empty.h"
#include "Tile_Number.h"
#include "Tile_Ship.h"

MapTile::MapTile()
	: m_pMyBehavior(nullptr)
{

}
//---------------------------------------------------------------------------------------------------------------------
// Creates tile. Factory style - if i got that right
//---------------------------------------------------------------------------------------------------------------------
void MapTile::CreateTile(TileTypeEnum tileType, int posX, int posY)
{
	m_thisTileType = tileType;

	//Always overriding old behavior. 
	//Uset to spawn a ship over a empty tile 
	if (m_pMyBehavior)
		delete m_pMyBehavior;

	if (m_thisTileType == MapTile::TileTypeEnum::k_empty)
		m_pMyBehavior = new TileEmpty;
	if (m_thisTileType == MapTile::TileTypeEnum::k_number)
		m_pMyBehavior = new TileNumber;
	if (m_thisTileType == MapTile::TileTypeEnum::k_ship)
		m_pMyBehavior = new TileShip;

	//Storing tiles position - rigth now used only for numbers
	m_pMyBehavior->SetX(posX);
	m_pMyBehavior->SetY(posY);

}