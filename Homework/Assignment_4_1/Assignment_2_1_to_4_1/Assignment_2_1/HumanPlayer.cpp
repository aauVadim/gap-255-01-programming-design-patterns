// HumanPlayer.cpp
// Name: Vadim Osipov
// Date: 9/15

#include "HumanPlayer.h"
#include <iostream>

HumanPlayer::HumanPlayer(int myNumber)
	: Player()
{
	m_playerNumber = myNumber;
}

void HumanPlayer::DrawMap()
{	
	std::cout << "Player: " << m_playerNumber << " Map" << std::endl;

	m_pMyBattleField->DrawField(m_isMyTurn);

	std::cout << "\nPlayer: " << m_playerNumber << " Ship pieces left: " << m_pMyBattleField->GetShipPiecesLeft() << std::endl;
	std::cout << "\n\n- - - - - - - - - - - - " << std::endl;
}

bool HumanPlayer::ProcessHit(int posX, int posY, Player* p_playerRef)
{
	//Processing hit
	m_pMyBattleField->ProcessHit(posX, posY);
	//Kill player is no more pieces left
	if (m_pMyBattleField->GetShipPiecesLeft() == 0)
		m_isEliminated = true;

	return true;
}