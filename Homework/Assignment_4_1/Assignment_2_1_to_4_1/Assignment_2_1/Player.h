// Player.h
#ifndef __PLAYER_H__
#define __PLAYER_H__



//Foward declaration
// [rez] This is not a forward declaration, it's an include.  A forward declaration (which you definitely 
// should have done instead of an include) would look like this:
// [vo] Sorry that was an old comment and I completely forgot to remove it. My bad. 
//    class MapTile;
#include "MapTile.h"
#include "GameField.h"

class Player
{
protected:
	//Battlefield pointer
	GameField* m_pMyBattleField;
	//Player number reference 
	int m_playerNumber;
public:
	bool m_isMyTurn;
	bool m_isEliminated;
	/*---Methods---*/
public:
    // [rez] TODO: No virtual functions?  Where's the subclass delegation?
	// [vo] Added: Now AIPlayer and HumanPlayer classes override those to their needs.

	Player();
	virtual ~Player() { };

	//Could have been pure virtual, but this is setter function so I kept the logic here
	virtual void SetPlayerBattlefield(GameField* p_gameField)	{ m_pMyBattleField = p_gameField; }

	//Made to be overriten
	virtual bool ProcessHit(int posX, int posY, Player* p_playerRef) = 0;
	//Utility - returns how much ship pieces left
	int GetShipPiecesLeft();

	//For some reason it would not let me make arrays of this with pure virtual function
	//So I had to make this not pure virtual
	virtual void DrawMap() = 0;
};

#endif
