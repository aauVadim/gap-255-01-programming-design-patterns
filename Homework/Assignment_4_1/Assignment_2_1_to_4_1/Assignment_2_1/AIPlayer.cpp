// AIPlayer.cpp
// Name: Vadim Osipov
// Date: 9/15

//#define DEBUG_BOTS

#include "AIPlayer.h"
#include "Utility.h"

#ifdef DEBUG_BOTS
#include <iostream>
#include <conio.h>
#endif

//---------------------------------------------------------------------------------------------------------------------
// Takes hit atgive player field. 
//---------------------------------------------------------------------------------------------------------------------
void AIPlayer::TakeAction(Player* p_humanPlayer)
{


	//Pure random, thanks Rez
	int randPosX = Utility::GetInstance()->RandRange(1, m_pMyBattleField->GetMapSizeX());
	int randPosY = Utility::GetInstance()->RandRange(1, m_pMyBattleField->GetMapSizeX());

	//
	for (int i = 0; i < (m_pMyBattleField->GetMapSizeX() * m_pMyBattleField->GetMapSizeX()); ++i)
	{
		
		//If this record exists
		if (m_hitTilesArray[i].m_hitPosX != randPosX && m_hitTilesArray[i].m_hitPosY != randPosY)
		{
			if (m_hitTilesArray[i].m_hitPosX == -1 && m_hitTilesArray[i].m_hitPosY == -1)
			{
				m_hitTilesArray[i].m_hitPosX = randPosX;
				m_hitTilesArray[i].m_hitPosY = randPosY;

				p_humanPlayer->ProcessHit(m_hitTilesArray[i].m_hitPosX, m_hitTilesArray[i].m_hitPosY, this);

				break;
			}
		}
		//Check if you have this stored already, if so find new numbers
		else if (m_hitTilesArray[i].m_hitPosX == randPosX && m_hitTilesArray[i].m_hitPosY == randPosY)
		{
			int randPosX = Utility::GetInstance()->RandRange(1, (m_pMyBattleField->GetMapSizeX() - 1));
			int randPosY = Utility::GetInstance()->RandRange(1, (m_pMyBattleField->GetMapSizeX() - 1));

			continue;
		}
	}

	

#ifdef DEBUG_BOTS
	std::cout << "AI Player hits:\n";
	for (int i = 0; i < (m_actualSizeX * m_actualSizeY); ++i)
	{
		if (m_hitTilesArray[i].hitPosX != -1 && m_hitTilesArray[i].hitPosY != -1)
		{
			std::cout <<"Record: " << i << " Pos X: " << m_hitTilesArray[i].hitPosX << " Pos Y: " << m_hitTilesArray[i].hitPosY << std::endl;
		}
		else
			break;
	}
	_getch();
#endif
}

AIPlayer::AIPlayer() 
	: Player()
{

}

//---------------------------------------------------------------------------------------------------------------------
// Used to initialize the AI player. Currenly used just for array.
//---------------------------------------------------------------------------------------------------------------------
void AIPlayer::Initialize()
{
	int fieldSize = (m_pMyBattleField->GetMapSizeX() * m_pMyBattleField->GetMapSizeY());
	m_hitTilesArray = new HitTiles[fieldSize];
}

void AIPlayer::DrawMap()
{
	std::cout << "AI Player Map" << std::endl;

	m_pMyBattleField->DrawField(m_isMyTurn);

	std::cout << "\n AI Player ship pieces left: " << m_pMyBattleField->GetShipPiecesLeft() << std::endl;
	std::cout << "\n\n- - - - - - - - - - - - " << std::endl;
}

//---------------------------------------------------------------------------------------------------------------------
// Used to initialize the AI player. Currenly used just for array.
//---------------------------------------------------------------------------------------------------------------------
bool AIPlayer::ProcessHit(int posX, int posY, Player* p_playerRef)
{
	//Processing hit
	m_pMyBattleField->ProcessHit(posX, posY);
	
	//Kill player is no more pieces left
	if (m_pMyBattleField->GetShipPiecesLeft() == 0)
		m_isEliminated = true;

	//Taking action for AI PLayer
	TakeAction(p_playerRef);
	return true;
}

void AIPlayer::SetPlayerBattlefield(GameField* p_gameField)
{
	m_pMyBattleField = p_gameField;
	//Override this function just for AI to initialize his hit array
	Initialize();
}