// Tile_Type.h
// Name: Vadim Osipov
// Date: 10/3

#pragma once

//Trying to keep this class as much virtual as possible
class TileType
{
protected:
	//Positions
	int m_posX, m_posY;
	//Helper bool - if was hit
	bool m_hasBeenShotAt;

public:
	//Constructor
	TileType() : m_posX(0), m_posY(0), m_hasBeenShotAt(false) { };
	virtual ~TileType() { };

	virtual void Draw(bool isOurTurn) = 0;
	int SetX(int posX) { return m_posX = posX; }
	int SetY(int posY) { return m_posY = posY; }

	bool HasBeenHit(bool hit){ return m_hasBeenShotAt = hit; }
	bool HasBeenHit(){ return m_hasBeenShotAt; }
};