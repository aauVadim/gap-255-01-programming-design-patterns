// GameField.cpp
// Name: Vadim Osipov
// Date: 10/2

#include "GameField.h"
#include "MapTile.h"
#include "Constants.h"

#include "Utility.h"


#include <iostream>

using std::cout;
using std::endl;

bool GameField::CreateGrid(int sizeX, int sizeY)
{
	//Offset for unitili cells
	int offsetX = 1;
	int offsetY = 1;

	//Size + Offset for numbers
	m_actualSizeX = sizeX + offsetX;
	m_actualSizeY = sizeY + offsetY;

	//allocate memory for the grid
	//Creating a pointer to rows 
	m_ppGrid = new MapTile*[m_actualSizeY];
	//Initializing the rows 
	for (int i = 0; i < m_actualSizeY; ++i)
	{
		m_ppGrid[i] = new MapTile[m_actualSizeX];
	}

	if (InitializeGrid())
		return true;
	else
		return false;

	return false;
}

bool GameField::InitializeGrid()
{
	//Essential loop 
	for (int y = 0; y < m_actualSizeY; ++y)
	{
		for (int x = 0; x < m_actualSizeX; ++x)
		{
			//Numbers of rows of X 
			if (y == 0 || x == 0)
			{
				m_ppGrid[y][x].CreateTile(MapTile::TileTypeEnum::k_number, x, y);
			}
			else	//Blank tiles
			{
				m_ppGrid[y][x].CreateTile(MapTile::TileTypeEnum::k_empty, x, y);
			}
		}
	}
	SpawnBattleship();
	return true;
}

//---------------------------------------------------------------------------------------------------------------------
// Spawns the battleship in a random location and orientation.
//---------------------------------------------------------------------------------------------------------------------
void GameField::SpawnBattleship()
{
	int randX = Utility::GetInstance()->RandRange(k_battleshipLength, (m_actualSizeX - 1) - k_battleshipLength);
	int randY = Utility::GetInstance()->RandRange(k_battleshipLength, (m_actualSizeY - 1) - k_battleshipLength);
	bool isHorizontal = (rand() % 100 < 50) ? (true) : (false);
	int delta = (rand() % 100 < 50) ? (-1) : (1);

	for (int indY = 1; indY < m_actualSizeY - 1; ++indY)
	{
		for (int indX = 1; indX < m_actualSizeX - 1; ++indX)
		{
			if (indY == randY && indX == randX)
			{
				for (int i = 0; i < k_battleshipLength; ++i)
				{
					if (isHorizontal)
					{
						m_ppGrid[indY][indX + (delta + i)].CreateTile(MapTile::TileTypeEnum::k_ship, indX + (delta + i), indY);
					}
					else
					{
						m_ppGrid[indY + (delta + i)][indX].CreateTile(MapTile::TileTypeEnum::k_ship, indX, indY + (delta + i));
					}
				}
			}
		}
	}
}

void GameField::DrawField(bool isMyTurn)
{
	for (int y = 0; y < m_actualSizeY; ++y)
	{
		for (int x = 0; x < m_actualSizeX; ++x)
		{
			//Dropping to a new line
			if (x == 0)
				cout << std::endl;

			m_ppGrid[y][x].Draw(isMyTurn);
			
			cout << ' ';
		}
	}
}
int GameField::GetShipPiecesLeft()
{
	int shipPieces = 0;

	for (int y = 0; y < m_actualSizeY; ++y)
	{
		for (int x = 0; x < m_actualSizeX; ++x)
		{
			if (m_ppGrid[y][x].m_thisTileType == MapTile::TileTypeEnum::k_ship && !m_ppGrid[y][x].GetHasBeenHit())
				++shipPieces;
		}
	}

	return shipPieces;
}

void GameField::ProcessHit(int posX, int posY)
{
	if ((posX >= 1 && posX < m_actualSizeX) && (posY >= 1 && posY < m_actualSizeY))
		m_ppGrid[posY][posX].SetHasBeenHit(true);
}