// HumanPlayer.h
// Name: Vadim Osipov
// Date: 9/15

#pragma once

#include "Player.h"

// [rez] TODO: This class does nothing.  Either kill it or use it.  One idea would be to move the player 
// turn handling to this class.  That would make sense because you could have a virtual function called Update()
// that dealt with all the processing for the player.  The human player would get input from the player while
// the AI player would generate its input.
// [vo] added some overriten logic to this class. Specific only for human players

class HumanPlayer : public Player
{

public: 
	
	HumanPlayer(int myNumber);

	virtual void DrawMap() override;

	virtual bool ProcessHit(int posX, int posY, Player* p_playerRef) override;
};