// Tile.h
// Name: Vadim Osipov
// Date: 10/1

#pragma once

class Tile
{
	//Used to store this behavior
	//Tile may not have more then one behavior on to himself
	Tile* m_pThisBehav;

public: 
	enum class Tile_Type
	{
		k_empty,
		k_ship, 
		k_number
	};
	Tile();
	virtual ~Tile() { };
	//Used to create the tile with the board
	void CreateTile(Tile_Type tileType);
	//Making sure that every tile will draw itself
	virtual void Draw() = 0; 
	//Not pure virtual because nopt all tiles will be hit
	//Ship tiles and Emty tiles need to have it
	virtual void TakeHit() {};

};