// Player.h
// Name: Vadim Osipov
// Date: 10/1

#pragma once


#include "Game_Board.h"
#include "Player_Behavior.h"
// Base class for any player in the game. Human or AI
// Should carry its own Game_Board.
class Player
{


public: 
	//Used to determine what type of players to create 
	enum class Player_Type
	{
		k_human,
		k_AI,
	};
	//Reference to players behavior
	PlayerBehavior* m_pMyBehavior;
	
	Player();
	//Needed if this class used as base
	virtual ~Player() {};
	//To be used by World.
	virtual bool Update();
	//Player will create himseld a map. And own it
	void CreatePlayer(Player_Type typeOfThePlayer, int mapSizeX, int mapSizeY);
};