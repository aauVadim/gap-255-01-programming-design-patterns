// Tile.cpp
// Name: Vadim Osipov
// Date: 10/1

#include "Tile.h"

#include "Tile_Empty_Behavior.h"
#include "Tile_Ship_Behavior.h"
#include "Tile_Number_Behavior.h"

#include <iostream>

Tile::Tile()
	: m_pThisBehav(nullptr)
{

}

void Tile::CreateTile(Tile::Tile_Type tileType)
{
	switch (tileType)
	{
	case Tile::Tile_Type::k_empty:
		//Create this tile as empty
		m_pThisBehav = new TileEmptyBehavior;
		break;
	case Tile::Tile_Type::k_ship:
		//Create ship tile
		m_pThisBehav = new TileShipBehavior;
		break;
	case Tile::Tile_Type::k_number:
		//Create number tile 
		m_pThisBehav = new TileNumberBehavior;
		break;
	}

	if (m_pThisBehav == nullptr)
		std::cout << "Something went wrong in tile creation" << std::endl;
}