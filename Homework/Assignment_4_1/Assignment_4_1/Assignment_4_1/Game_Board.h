// Game_Board.h
// Name: Vadim Osipov
// Date: 10/1

#pragma once

#include "Tile.h"

class GameBoard
{

	Tile** m_ppGrid;
public:
	GameBoard();
	~GameBoard();

	bool CreateBoard(int sizeX, int sizeY);
	void ProcessHit(int posX, int posY);
	//Sends a draw call to every tile
	void Draw();

private:
	Tile** CreateGrid();
	void SpawnShip();
};