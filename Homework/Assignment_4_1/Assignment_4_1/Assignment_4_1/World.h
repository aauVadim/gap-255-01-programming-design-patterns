// World.h
// Name: Vadim Osipov
// Date: 10/1

#pragma once

class Player;

//SINGLETON! 
class World
{
	//Singleton pointer
	static World *m_pInstance;
	
	bool m_isGameRunning;
	int m_numberOfPlayers;
	bool m_isGameWithBots;
	//storing the board size
	int m_boardSizeX, m_boardSizeY;

	Player* m_pPlayers; 

public:
	//returns the instance of this 
	static World* Instance();

	//Public interface to creatre the world
	void CreateWorld(int sizeX, int sizeY, int numOfNumanPlayers);
	//Handles player input
	void PlayerInput(int posX, int posY);
	//Utility - main loop run on this
	//bool IsGameRunning();
	//Utility - returns the number of current player
	int GetCurrentPlayerUp();

	//To be used by ~Main~
	bool Update();

private:
	//Constructor
	World();
	~World();
	
	
	//TO REMOVE
	//I'm not sure if i even need them overloaded - but it sounded good at the time 
	//HUMAN VS HUMAN 
	// [rez] No, you didn't need to overload these.  If you want different behavior based on the type of 
	// of game, you should have created two subclasses, one for human vs human and one for human vs AI.
	// The game can create the appropriate instance based on the type of game.  DrawBoard() becomes a 
	// virtual function with different implementations.
	//void DrawBoard(int sizeX, int sizeY, int numOfHUmanPlayers, HumanPlayer* humanPlayersArray, bool isTurnOver);
	//HUMAN VS AI
	//void DrawBoard(int sizeX, int sizeY, HumanPlayer* humanPlayersArray, AIPlayer* aiPlayer);



	//TO KEEP
	//void SwitchPlayers(HumanPlayer& currentPlayer, HumanPlayer& nextPlayer);
	//TO KEEP
	//int CheckIfPlayersIsDirty(HumanPlayer* playerArray, int playersCount, int currentPlayerPos, int nextPlayerPos);
};
