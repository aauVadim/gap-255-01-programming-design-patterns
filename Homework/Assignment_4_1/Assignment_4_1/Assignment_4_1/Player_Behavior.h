// Player_Behavior.h
// Name: Vadim Osipov
// Date: 10/1

class PlayerBehavior
{
	PlayerBehavior* m_pMyBehav;

public:

	enum class Behavior_Type
	{
		k_ai, 
		k_human
	};

	PlayerBehavior(Behavior_Type typeOfPlayer);
	virtual ~PlayerBehavior();

	//Should be used by Player
	virtual bool Update();

	//virtual void ProcessHit(int posX) = 0;
};