// World.cpp
// Name: Vadim Osipov
// Date: 10/1

#pragma once

#include "World.h"
#include <iostream>
#include <conio.h>

#include "Player.h"

World* World::m_pInstance = nullptr;

World::World()
	: m_isGameRunning(true)
	, m_pPlayers(nullptr)
{

}

World::~World()
{
	if (m_pInstance)
		delete m_pInstance;
}

World* World::Instance(){
	if (!m_pInstance)
	{
		m_pInstance = new World;
	}
	return m_pInstance;
}

//---------------------------------------------------------------------------------------------------------------------
// Creates the world with given grid size and number of players
//---------------------------------------------------------------------------------------------------------------------
void World::CreateWorld(int sizeX, int sizeY, int numOfNumanPlayers)
{
	//If the number of players is correct
	if (numOfNumanPlayers > 0)
	{
		//Initializing array
		m_pPlayers = new Player[numOfNumanPlayers];
		//Create players
		if (numOfNumanPlayers == 1)
		{
			//Game with AI
			m_pPlayers[0].CreatePlayer(Player::Player_Type::k_human);
		}
		else
		{
			//Multiplayer game
			
		}
	}
	else
	{
		//TODO: Lets do something about bad input
		std::cout << "Number of players cannot be 0 or less" << std::endl;
	}
	////Initializing the player array 
	//m_pHPlayerArray = new HumanPlayer[numOfNumanPlayers];
	//m_numberOfPlayers = numOfNumanPlayers;
	////Storing this for future reference
	//m_boardSizeX = sizeX + 1;
	//m_boardSizeY = sizeY + 1;

	////we have human to human game
	//if (numOfNumanPlayers > 1)
	//{
	//	m_isGameWithBots = false;

	//	//For every human player - create class, make map 
	//	for (int i = 0; i < numOfNumanPlayers; ++i)
	//	{
	//		HumanPlayer humanPlayer;
	//		m_pHPlayerArray[i] = humanPlayer;

	//		//Giving the turn to go to first player
	//		if (i == 0)
	//			m_pHPlayerArray[i].m_isMyTurn = true;
	//		//
	//		if (m_pHPlayerArray[i].CreateGrid(sizeX, sizeY, i))
	//		{
	//			//Debuggin'
	//			//std::cout << "\nCreated and Initialized map for Human Player: " << (i + 1) << std::endl;
	//		}
	//	}
	//	//Draw a board for human players
	//	DrawBoard(sizeX, sizeY, numOfNumanPlayers, m_pHPlayerArray, false);
	//}
	//else if (numOfNumanPlayers == 1)	//Human vs. AI
	//{
	//	m_isGameWithBots = true;

	//	std::cout << "Human vs. AI game" << std::endl;

	//	HumanPlayer humanPlayer;
	//	m_pHPlayerArray[0] = humanPlayer;
	//	m_pHPlayerArray[0].m_isMyTurn = true;

	//	if (m_pHPlayerArray[0].CreateGrid(sizeX, sizeY, 0))
	//	{
	//		std::cout << "\nCreated and Initialized map for Human Player: " << (0 + 1) << std::endl;
	//	}

	//	//AI Part
	//	m_pAIPlayer = new AIPlayer();

	//	if (m_pAIPlayer->CreateGrid(sizeX, sizeY, 1))
	//	{
	//		m_pAIPlayer->Initialize();
	//		//std::cout << "\nCreated and Initialized - Computer Player" << std::endl; 
	//	}
	//	//Drawing board the first time
	//	DrawBoard(m_boardSizeX, m_boardSizeY, m_pHPlayerArray, m_pAIPlayer);
	//}
	//else if (numOfNumanPlayers <= 0)
	//{
	//	std::cout << "Have to have atleast one Human Player" << std::endl;
	//}
}

//---------------------------------------------------------------------------------------------------------------------
// Processses player input. Responsible for switching screens between players
//---------------------------------------------------------------------------------------------------------------------
//void World::PlayerInput(int hitPosX, int hitPosY)
//{
//	if (m_isGameWithBots)	//That's kinda obvious
//	{
//		//Giving hit to AI Guy 
//		m_pAIPlayer->ProcessHit(hitPosX, hitPosY);
//		//Letting AI to take action
//		m_pAIPlayer->TakeAction(m_pHPlayerArray);
//		//Redraw the board
//		DrawBoard(m_boardSizeX, m_boardSizeY, m_pHPlayerArray, m_pAIPlayer);
//	}
//	else
//	{
//		for (int i = 0; i < m_numberOfPlayers; ++i)
//		{
//			//If we have a current player that is not eliminated
//			if (m_pHPlayerArray[i].m_isMyTurn && !m_pHPlayerArray[i].m_isEliminated)
//			{
//				//Choosing next player
//				int nextPlayer = ((i + 1) < m_numberOfPlayers) ? (i + 1) : 0;
//
//				//Check before we fire. 
//				int playerCheckOne = CheckIfPlayersIsDirty(m_pHPlayerArray, m_numberOfPlayers, i, nextPlayer);
//
//				DrawBoard(m_boardSizeX, m_boardSizeY, m_numberOfPlayers, m_pHPlayerArray, false);
//				//Letting next in line know that he was hit
//				if (m_pHPlayerArray[playerCheckOne].ProcessHit(hitPosX, hitPosY))
//				{
//					//Check after we fire
//					int newNextPlayer = CheckIfPlayersIsDirty(m_pHPlayerArray, m_numberOfPlayers, i, playerCheckOne);
//					//Redraw the board with the hit
//					DrawBoard(m_boardSizeX, m_boardSizeY, m_numberOfPlayers, m_pHPlayerArray, true);
//					//Switch players
//					SwitchPlayers(m_pHPlayerArray[i], m_pHPlayerArray[newNextPlayer]);
//					break;
//				}
//			}
//		}
//	}
//}

//---------------------------------------------------------------------------------------------------------------------
// Draws the grid for Human vs. Human game. 
//---------------------------------------------------------------------------------------------------------------------
//void World::DrawBoard(int sizeX, int sizeY, int numOfHumanPlayers, HumanPlayer* humanPlayersArray, bool didTakeTurn)
//{
//	using namespace std;
//
//	system("cls");
//
//	for (int player = 0; player < numOfHumanPlayers; ++player)
//	{
//		if (!humanPlayersArray[player].m_isEliminated)
//		{
//			//Referencing the map
//			MapTile** currentPlayerMap = humanPlayersArray[player].GetPlayerBattlefield();
//			cout << "Player: " << (player + 1) << " Map" << endl;
//			//Drawing the map 
//			for (int y = 0; y < sizeY; ++y)
//			{
//				for (int x = 0; x < sizeX; ++x)
//				{
//					//Dropping to a new line
//					if (x == 0)
//						cout << std::endl;
//					cout << currentPlayerMap[y][x].GetTileChar(humanPlayersArray[player].m_isMyTurn) << ' ';
//				}
//			}
//
//			cout << "\nPlayer: " << player + 1 << " Ship pieces left: " << humanPlayersArray[player].GetShipPiecesLeft() << endl;
//			cout << "\n\n- - - - - - - - - - - - " << endl;
//		}
//	}
//	//Keeping the screen up for a second - to show player where/what he hit
//	if (didTakeTurn)
//	{
//		cout << "\n--- Press any key to continue ---\n";
//		_getch();
//	}
//}

//---------------------------------------------------------------------------------------------------------------------
// Draws the grid for Human vs. AI game. Full of duct tape at this point
//---------------------------------------------------------------------------------------------------------------------
//void World::DrawBoard(int sizeX, int sizeY, HumanPlayer* humanPlayersArray, AIPlayer* aiPlayer)
//{
//	using namespace std;
//	system("cls");
//	//Some cheating - will deal with this later
//	//Referencing the map
//	MapTile** currentPlayerMap = humanPlayersArray->GetPlayerBattlefield();
//	cout << "Player Map" << endl;
//	//Drawing the map 
//	for (int y = 0; y < sizeY; ++y)
//	{
//		for (int x = 0; x < sizeX; ++x)
//		{
//			//Dropping to a new line
//			if (x == 0)
//				cout << std::endl;
//			//Getting the current char for this tile
//			cout << currentPlayerMap[y][x].GetTileChar(true) << ' ';
//		}
//	}
//	cout << "\n\n- - - - - - - - - - - - " << endl;
//	//Big chink of ducttape to draw this - probably will bite me in an ass later on 
//	cout << "AI Player Map" << endl;
//	//Drawing the map 
//	//Getting the map for AI Player
//	MapTile** aiPlayerMap = aiPlayer->GetPlayerBattlefield();
//
//	for (int y = 0; y < sizeY; ++y)
//	{
//		for (int x = 0; x < sizeX; ++x)
//		{
//			//Dropping to a new line
//			if (x == 0)
//				cout << std::endl;
//			//Getting the current char for this tile
//			cout << aiPlayerMap[y][x].GetTileChar(false) << ' ';
//		}
//	}
//	cout << "\n\n- - - - - - - - - - - - " << endl;
//
//	cout << "Player ship pieces left: " << humanPlayersArray->GetShipPiecesLeft() << endl;
//	cout << "Computer ship pieces left: " << aiPlayer->GetShipPiecesLeft() << endl;
//}

//---------------------------------------------------------------------------------------------------------------------
// Utility bool. Returns if game is running or not dependant on how many players are still up
//---------------------------------------------------------------------------------------------------------------------
//bool World::IsGameRunning()
//{
//	if (m_isGameWithBots)
//	{
//		if (m_pAIPlayer->GetShipPiecesLeft() == 0)
//			return false;
//		if (m_pHPlayerArray->GetShipPiecesLeft() == 0)
//			return false;
//	}
//	else
//	{
//		int numberOfActivePlayers = 0;
//		for (int i = 0; i < m_numberOfPlayers; ++i)
//		{
//			if (m_pHPlayerArray[i].GetShipPiecesLeft() >= 1)
//				++numberOfActivePlayers;
//		}
//
//		if (numberOfActivePlayers == 1)
//			return false;
//		else
//			return true;
//	}
//	return true;
//}

//---------------------------------------------------------------------------------------------------------------------
// Utility int. Returns current number of players in the game.
//---------------------------------------------------------------------------------------------------------------------
int World::GetCurrentPlayerUp()
{
	//for (int i = 0; i < m_numberOfPlayers; ++i)
	//{
	//	if (m_pHPlayerArray[i].m_isMyTurn && !m_pHPlayerArray[i].m_isEliminated)
	//		return i + 1;
	//}

	return 0;
}
//---------------------------------------------------------------------------------------------------------------------
// Switches player screens and turns.
//---------------------------------------------------------------------------------------------------------------------
//void World::SwitchPlayers(HumanPlayer& currentPlayer, HumanPlayer& nextPlayer)
//{
//	//using namespace std;
//	//if (IsGameRunning())
//	//{
//	//	currentPlayer.m_isMyTurn = false;
//	//	nextPlayer.m_isMyTurn = true;
//
//	//	system("cls");
//
//	//	cout << "Player: " << GetCurrentPlayerUp() << " turn.\n";
//	//	cout << "Player: " << GetCurrentPlayerUp() << " --- Press anykey when ready ---\n";
//	//	_getch();
//	//	//Drawing board for next player up
//	//	DrawBoard(m_boardSizeX, m_boardSizeY, m_numberOfPlayers, m_pHPlayerArray, false);
//	//}
//}
//---------------------------------------------------------------------------------------------------------------------
// Checks if next player is not out. If he is out, assigns in to next avalible player
//---------------------------------------------------------------------------------------------------------------------
//int World::CheckIfPlayersIsDirty(HumanPlayer* playerArray, int playersCount, int currentPlayerPos, int nextPlayerPos)
//{
//	//int newNextPlayer = nextPlayerPos;
//
//	////If current next player is eliminated
//	//if (playerArray[newNextPlayer].m_isEliminated)
//	//{
//	//	//Choosing where to stop our loop
//	//	int stopPos = (nextPlayerPos == (currentPlayerPos + 1)) ? playersCount : currentPlayerPos;
//
//	//	for (int i = nextPlayerPos; i < stopPos; ++i)
//	//	{
//	//		if (!playerArray[i].m_isEliminated)
//	//		{
//	//			return newNextPlayer = i;
//	//		}
//	//		else if (i == (playersCount - 1))	//Just for last player up
//	//		{
//	//			//If you at the end and did not find anything - go from beginning? 
//	//			for (int j = 0; j < currentPlayerPos; ++j)
//	//			{
//	//				if (!playerArray[j].m_isEliminated)
//	//				{
//	//					return newNextPlayer = j;
//	//				}
//	//			}
//	//		}
//	//	}
//	//}
//	//else
//	//	return newNextPlayer;
//
//	return 0;
//}

