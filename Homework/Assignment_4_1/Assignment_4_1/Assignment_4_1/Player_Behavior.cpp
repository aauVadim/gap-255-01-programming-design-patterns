// Player_Behavior.cpp
// Name: Vadim Osipov
// Date: 10/1

//Behavior types
#include "AI_Player_Behavior.h"
#include "Human_Player_Behavior.h"

#include "Player_Behavior.h"

PlayerBehavior::PlayerBehavior(PlayerBehavior::Behavior_Type typeOfPlayer)
	: m_pMyBehav(nullptr)
{
	if (typeOfPlayer == PlayerBehavior::Behavior_Type::k_human)
		m_pMyBehav = new HumanPlayerBehavior;
	else if (typeOfPlayer == PlayerBehavior::Behavior_Type::k_ai)
		m_pMyBehav = new AIPlayerBehavior;
}

