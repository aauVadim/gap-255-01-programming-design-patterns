// AdapterClient.h
#ifndef ADAPTERCLIENT_H
#define ADAPTERCLIENT_H

#include <vector>

class Renderable;

class AdapterClient
{
    std::vector<Renderable*> m_renderList;

public:
    bool Init();
    void Render();

protected:
    virtual Renderable* CreateRenderable() const;

private:
    bool LoadSprite(const char* filename);
};

#endif
