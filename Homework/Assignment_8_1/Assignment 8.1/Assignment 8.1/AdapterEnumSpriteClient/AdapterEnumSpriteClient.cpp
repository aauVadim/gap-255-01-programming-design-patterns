//	AdapterEnumSpriteClient.cpp 
//	Name: Vadim Osipov
//	Date: 11/1

#include "AdapterEnumSpriteClient.h"
#include "../EnumSpriteRenderable/EnumSpriteRenderable.h"


Renderable* AdapterEnumSpriteClient::CreateRenderable() const
{
	return new EnumSpriteRenderable();
}
