//	AdapterEnumSpriteClient.cpp 
//	Name: Vadim Osipov
//	Date: 11/1

#pragma once

#include "../Adapter/AdapterClient.h"
#include "../EnumSprite/EnumSprite.h"

class Renderable;

class AdapterEnumSpriteClient : public AdapterClient
{

protected:
	~AdapterEnumSpriteClient() { }

	virtual Renderable* CreateRenderable() const override;

};
