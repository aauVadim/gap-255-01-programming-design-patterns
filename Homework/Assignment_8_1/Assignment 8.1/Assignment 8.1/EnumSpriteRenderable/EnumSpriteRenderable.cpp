//	EnumSpriteRenderable.cpp
//	Name: Vadim Osipov
//	Date: 11/1

#include "EnumSpriteRenderable.h"

#include <iostream>
#include <string>
#include <fstream>

//	Initializing EnumSprite by by pulling char out of file. 
//	BTW - thanks for telling us about those files. It took a second to figure it out.
bool EnumSpriteRenderable::Load(const char* filename)
{
	//Creating new file read
	std::ifstream file;
	file.open(filename);
	//Getting a char out of it
	char fileChar = file.get();
	//Initialize yourself by char in file.
	switch (fileChar)
	{
	case 'A':
		Init(Type::k_a);
		break;
	case 'B':
		Init(Type::k_b);
		break;
	case 'C':
		Init(Type::k_c);
		break;
	case 'D':
		Init(Type::k_d);
		break;
	case 'E':
		Init(Type::k_e);
		break;
	default:
		std::cout << "Something went wrong in Load\n";
		//Error init
		Init(Type::k_unknown);
		//Close file
		file.close();
		//Bail out with error 
		return false;
	}
	//Closeing file 
	file.close();
	return true;
}
//Using EnumSprite's Draw function + std::endl 
void EnumSpriteRenderable::Render()
{
	Draw();
	std::cout << std::endl;
}
