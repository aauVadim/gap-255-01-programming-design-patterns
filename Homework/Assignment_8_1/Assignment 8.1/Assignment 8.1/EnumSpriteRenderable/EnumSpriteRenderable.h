//	EnumSpriteRenderable.h
//	Name: Vadim Osipov
//	Date: 11/1

#pragma once

#include "../Adapter/Renderable.h"
#include "../EnumSprite/EnumSprite.h"

//I decided to keep it as IS-A instead of HAS-A possibility. 
//I think it would be easiet to accses EnumSprite features this way

class EnumSpriteRenderable : public EnumSprite, public Renderable
{
public:
	~EnumSpriteRenderable() { }

	virtual bool Load(const char* filename) override;
	virtual void Render() override;
};