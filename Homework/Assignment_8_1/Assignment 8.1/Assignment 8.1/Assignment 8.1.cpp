// Assignment 8.1.cpp
//Old - comented out
//#include "Adapter/AdapterClient.h"
#include "AdapterEnumSpriteClient\AdapterEnumSpriteClient.h"
#include <conio.h>

//---------------------------------------------------------------------------------------------------------------------
// Assignment 8.1
// 
// IMPORTANT: Compile and run the program.  You should see a series of three letters.  Make a note of the output 
// because it must remain exactly the same.
// 
// Current Implementation:
// The program works by including & linking a 3rd party Adapter library, which you will find in the Adapter folder.
// You don't have access to any of the code except what you see in the headers and there are comments there explaining
// the interface.
// 
// The general idea is that the AdapterClient has a list of sprites that it will render.  The sprites (currently only 
// a BasicSprite) all inherit from the Renderable interface.
// 
// There is a second library called EnumSprite, which includes a header and a lib file.  Right now, nothing is being 
// done with this library.
// 
// Assignment:
// Your job is to get the EnumSprite class to play nicely with the AdapterClient.  You are not allowed to change 
// anything in those two libraries (and you don't have the code to do so anyway).  You shouldn't need to change
// anything here in main() either, though you can if you need to.  You are free to create/add any files you need.
// 
// Specifically, AdapterClient should render EnumSprite's INSTEAD OF BasicSprite's.  Note that 
// AdapterClient::CreateRenderable() is a factory method.  The default implementation looks like this:
//     return new BasicSprite;
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
//	To Rez: 
//---------------------------------------------------------------------------------------------------------------------
//	AdapterEnumSpriteClient.h/.cpp	- Overriedes factory method CreateRenderable() from AdapterClient.
// 
//	EnumSpriteRenderable.h/.cpp		- Overrides/Extends Load() and Render() Methods from Renderable. 
//---------------------------------------------------------------------------------------------------------------------

void main()
{
    AdapterEnumSpriteClient client;

    if (!client.Init())
        return;

    client.Render();

    _getch();
}

