// Player.cpp
// Name: Vadim Osipov 
// Date: 9/15

#include "Player.h"
#include "Constants.h"
#include <iostream>

using namespace std;

Player::Player()
	: m_isMyTurn(false)
	, m_isEliminated(false)
{

}

Player::~Player()
{
	if (m_pHitPositions)
		delete[] m_pHitPositions;
	if (m_ppGrid)
		delete[] m_ppGrid;
}

//---------------------------------------------------------------------------------------------------------------------
// Creates and initializes the grid.
//---------------------------------------------------------------------------------------------------------------------
bool Player::CreateGrid(int sizeX, int sizeY, int myPlayerNumber)
{
	//Offset for unitili cells
	int offsetX = 1;
	int offsetY = 1;
	
	//Initializing the Hit Array
	m_pHitPositions = new MapTile[sizeY * sizeX];

	//Size + Offset for numbers
	m_actualSizeX = sizeX + offsetX;
	m_actualSizeY = sizeY + offsetY;

	//allocate memory for the grid
	//Creating a pointer to rows 
	m_ppGrid = new MapTile*[m_actualSizeY];
	//Initializing the rows 
	for (int i = 0; i < m_actualSizeY; ++i)
	{
		m_ppGrid[i] = new MapTile[m_actualSizeX];
	}

	if (InitializeGrid())
		return true;
	else
		return false;
}
//---------------------------------------------------------------------------------------------------------------------
// Initializer function. Basically used to set numbered tiles to be numbers
//---------------------------------------------------------------------------------------------------------------------
bool Player::InitializeGrid()
{
	//Essential loop 
	for (int y = 0; y < m_actualSizeY; ++y)
	{
		for (int x = 0; x < m_actualSizeX; ++x)
		{
			//Numbers of rows of X 
			if (y == 0 || x == 0)
			{
				m_ppGrid[y][x].SetThisToNumber(x, y);
			}
		}
	}
	SpawnBattleship();
	return true;
}
//---------------------------------------------------------------------------------------------------------------------
// Returns a random number between minValue and maxValue.
//---------------------------------------------------------------------------------------------------------------------
int Player::RandRange(int minValue, int maxValue)
{
	int val = (rand() % (maxValue - minValue)) + minValue;
	return val;
}
//---------------------------------------------------------------------------------------------------------------------
// Returns the field for current player
//---------------------------------------------------------------------------------------------------------------------
MapTile** Player::GetPlayerBattlefield()
{
	return m_ppGrid;
}

//---------------------------------------------------------------------------------------------------------------------
// Spawns the battleship in a random location and orientation.
//---------------------------------------------------------------------------------------------------------------------
void Player::SpawnBattleship()
{
	int randX = RandRange(k_battleshipLength, (m_actualSizeX - 1) - k_battleshipLength);
	int randY = RandRange(k_battleshipLength, (m_actualSizeY - 1) - k_battleshipLength);
	bool isHorizontal = (rand() % 100 < 50) ? (true) : (false);
	int delta = (rand() % 100 < 50) ? (-1) : (1);

	
	for (int indY = 1; indY < m_actualSizeY-1; ++indY)
	{
		for (int indX = 1; indX < m_actualSizeX-1; ++indX)
		{
			if (indY == randY && indX == randX)
			{
				for (int i = 0; i < k_battleshipLength; ++i)
				{
					if (isHorizontal)
					{
						m_ppGrid[indY][indX + (delta + i)].m_isShip = true;
					}
					else
					{
						m_ppGrid[indY + (delta + i)][indX].m_isShip = true;
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
// Processes player hit. Sets tile to be in hit state if tile is in the field range
//---------------------------------------------------------------------------------------------------------------------
bool Player::ProcessHit(int posX, int posY)
{
	for (int y = 1; y < m_actualSizeY; ++y)
	{
		for (int x = 1; x < m_actualSizeX; ++x)
		{
			if (posY == y && posX == x)
			{
				//hitting desired tile 
				m_ppGrid[y][x].HasBeenHit() = true;
				//Checking if any ship pieces left - if not player is eliminated 
				if (GetShipPiecesLeft() == 0)
					m_isEliminated = true;
				
				return m_ppGrid[y][x].HasBeenHit();
				break;
			}
		}
	}
	return true;
}

//---------------------------------------------------------------------------------------------------------------------
// Utility - Returns amount of ship pieces left on the field. 
//---------------------------------------------------------------------------------------------------------------------
int Player::GetShipPiecesLeft()
{
	int shipPieces = 0;

	for (int y = 0; y < m_actualSizeY; ++y)
	{
		for (int x = 0; x < m_actualSizeX; ++x)
		{
			if (m_ppGrid[y][x].m_isShip && !m_ppGrid[y][x].HasBeenHit())
				++shipPieces;
		}
	}

	//If you're out of ship pieces - you're out. Another safety check
	if (shipPieces == 0)
		m_isEliminated = true;

	return shipPieces;
}
