// Player.h
#ifndef __PLAYER_H__
#define __PLAYER_H__

//Foward declaration
#include "MapTile.h"

class Player
{
protected:
	//Grid pointer
	MapTile** m_ppGrid;
	//Grid sizes
	int m_actualSizeX;
	int m_actualSizeY;
	//Player number reference 
	int m_playerNumber;
public:
	bool m_isMyTurn;
	bool m_isEliminated;
	/*---Methods---*/
public:
	//virtual bool Update();  // returns true if we should quit
	Player();
	~Player();

	MapTile** GetPlayerBattlefield();
	//Initializes player field, ship
	bool CreateGrid(int sizeX, int sizeY, int myPlayerNumber);
	bool ProcessHit(int posX, int posY);
	//Utility - returns how much ship pieces left
	int GetShipPiecesLeft();

protected:
	MapTile* m_pHitPositions = new MapTile;
	//Creates the grid
	//Initializes the grid with: Numbers, Ships, Spacers
	bool InitializeGrid();
	//Utility to return Rand Number
	void SpawnBattleship();
	int RandRange(int minValue, int maxValue);
};

#endif
