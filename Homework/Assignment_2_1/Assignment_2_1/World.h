//World.h
//Name: Vadim Osipov
//Date: 9/15

#pragma once

#include "MapTile.h"
#include "HumanPlayer.h"
#include "AIPlayer.h"

//SINGLETON! 
class World
{
	//Singleton pointer
	static World *m_pInstance;

	bool m_isGameRunning; 

	//Human player array
	HumanPlayer* m_pHPlayerArray;
	int m_numberOfPlayers;
	//AI Pointer
	AIPlayer* m_pAIPlayer;
	
	bool m_isGameWithBots;
	//storing the board size
	int m_boardSizeX, m_boardSizeY;

public:
	//returns the instance of this 
	static World* Instance();
	
	//Public interface to creatre the world
	void CreateWorld(int sizeX, int sizeY, int numOfNumanPlayers);
	//Handles player input
	void PlayerInput(int posX, int posY);
	//Utility - main loop run on this
	bool IsGameRunning();
	//Utility - returns the number of current player
	int GetCurrentPlayerUp();

private:
	//Constructor
	World();
	~World();
	
	//I'm not sure if i even need them overloaded - but it sounded good at the time :)  
	//HUMAN VS HUMAN 
	void DrawBoard(int sizeX, int sizeY, int numOfHUmanPlayers, HumanPlayer* humanPlayersArray, bool isTurnOver);
	//HUMAN VS AI
	void DrawBoard(int sizeX, int sizeY, HumanPlayer* humanPlayersArray, AIPlayer* aiPlayer);
	void SwitchPlayers(HumanPlayer& currentPlayer, HumanPlayer& nextPlayer);
	int CheckIfPlayersIsDirty(HumanPlayer* playerArray, int playersCount, int currentPlayerPos, int nextPlayerPos);
};
