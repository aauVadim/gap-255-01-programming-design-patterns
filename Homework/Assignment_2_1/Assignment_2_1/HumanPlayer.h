// HumanPlayer.h
// Name: Vadim Osipov
// Date: 9/15

#pragma once

#include "Player.h"

class HumanPlayer : public Player
{

public: 
	HumanPlayer();
	void HandleInput(int posX, int posY);
};