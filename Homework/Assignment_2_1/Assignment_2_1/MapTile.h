// MapTile.h
// Name: Vadim Osipov 
// Date: 9/15

#pragma once


class MapTile
{
	//Positions
	int m_posX, m_posY;
	//Helper bool - if was hit
	bool m_hasBeenShotAt;	
	char m_thisDisplayChar;

public:

	bool m_isNumber;	//Number
	bool m_isShip;		//Ship
	//Cosntructor
	MapTile();
	//Returns the proper Tile Char 
	char& GetTileChar(bool isOurTurn);
	//Returns if the tile was hit or not 
	bool& HasBeenHit();
	//Setting this tile to a number on the side of the field
	void SetThisToNumber(int posX, int posY);
};