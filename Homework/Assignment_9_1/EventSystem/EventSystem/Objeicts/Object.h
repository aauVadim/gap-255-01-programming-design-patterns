//	Object.h
//	Name: Vadim Osipov 
//	Date: 11/5

#pragma once

#include "../EventListener/EventListener.h"

//---------------------------------------------------------------------------------------------------------------------
//	Object: 
//		At the moment object inherints from EventListener interface. 
//		Object does not have to subscribe to events. It is optional. 
//---------------------------------------------------------------------------------------------------------------------
class Object : public EventListener
{
	//Every object has to have unique number. 
	unsigned int m_myGUID;

public: 
	Object(unsigned int myGUID);

	//EventListener virtuals
	virtual void SubscribeToEvent(const char* eventKey) override;
	virtual void UnsubscribeFromEvent(const char* eventKey) override;
	virtual void EventUpdate(const char* eventKey) override;
};