//	Object.cpp
//	Name: Vadim Osipov 
//	Date: 11/5

#include <iostream>

#include "Object.h"
#include "../EventHandler/EventHandler.h"


Object::Object(unsigned int myGUID)
	:m_myGUID(myGUID)
{

}

//---------------------------------------------------------------------------------------------------------------------
//	Function that is triggered when event is called
//---------------------------------------------------------------------------------------------------------------------
void Object::EventUpdate(const char* eventKey)
{
	std::cout << "Object: " << m_myGUID << " received Update for " << eventKey << std::endl;
}
	
//---------------------------------------------------------------------------------------------------------------------
//	Allows object to subscribe to EventHandler events/updates
//---------------------------------------------------------------------------------------------------------------------
void Object::SubscribeToEvent(const char* eventKey)
{
	EventHandler::GetInstance()->AddEventListener(eventKey, m_myGUID, this);
}

//---------------------------------------------------------------------------------------------------------------------
//	Allows object to unsubscribe to EventHandler events/updates
//---------------------------------------------------------------------------------------------------------------------
void Object::UnsubscribeFromEvent(const char* eventKey)
{
	EventHandler::GetInstance()->RemoveEventListener(eventKey, m_myGUID);
}

