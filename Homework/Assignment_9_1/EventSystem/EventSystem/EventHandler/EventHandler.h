//	EventHandler.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once

#include <iostream>
#include <map>

#include "../EventListener/EventListener.h"

class Event;

//---------------------------------------------------------------------------------------------------------------------
//	EventHandler if the main class for Event system. 
//	every object that needs to receive updates when event is triggered needs to sign up with this manager.
//	
//	Objects stored in std::map - for ease of addition and removal.
//	Key: Key in this map is eventKey - every event that is created has its own std::map of objects to update. 
//	Value: std::map<> - second map where: 
//		- Key - Is objects GUID - for ease of removal from event lists. 
//		- Value - Object itself. 
//---------------------------------------------------------------------------------------------------------------------
class EventHandler
{
	//Singleton
	static EventHandler* m_pInstance; 
	
	//First Map of: eventKey and std::map<> 
	//Used map  - for ease of removal of elements. 
	//Second Map:      
	//	- unsigned int		- Used to store objects
	//	- EventListener*	- Pointer to an object
	std::map<const char*, std::map<unsigned int, EventListener*>> m_eventListeners;
	
public: 
	//Singleton
	static EventHandler* GetInstance();

	//Add event to listeners structure
	void AddEventListener(const char* eventKey, unsigned int listenerGUID, EventListener* pListener);
    //Remove element from listeners structure
	void RemoveEventListener(const char* eventKey, unsigned int listenerGUID);
    
	//Old function - Will trigger event by string. 
	void TriggerEvent(const char* eventKey = "");
    
	//New function - will trigger event by Event class
	void TriggerEvent(Event* pEvent);

	//Removes event from the board completely
	void RemoveEvent(const char* eventKey); 

	//MUST! Must be used in the end of the programm.
    void ShutDown();

private: 
	EventHandler() { }
};
