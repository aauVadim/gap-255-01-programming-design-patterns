//	EventHandler.cpp
//	Name: Vadim Osipov 
//	Date: 11/4

#include "EventHandler.h"
#include "../EventListener/EventKeys.h"

#include "../Events/Event.h"

#include <map>

EventHandler* EventHandler::m_pInstance = nullptr;

EventHandler* EventHandler::GetInstance()
{
	if (m_pInstance == nullptr)
		m_pInstance = new EventHandler;
	return m_pInstance;
}

//---------------------------------------------------------------------------------------------------------------------
//	Adds event listener by eventKey and listenersGUID. 
//	if eventKey record does not exist - it will create new one. 
//	if listenerGUID record does not exist - it will create new one
//---------------------------------------------------------------------------------------------------------------------
void EventHandler::AddEventListener(const char* eventKey, unsigned int listenerGUID, EventListener* pListener)
{
	//Adding it to listeners
	m_eventListeners[eventKey].emplace(listenerGUID, pListener);
	std::cout << "Added: " << listenerGUID << " to " << eventKey << " event\n";
}

//---------------------------------------------------------------------------------------------------------------------
//	Removes event listener by its GUID.   
//---------------------------------------------------------------------------------------------------------------------
void EventHandler::RemoveEventListener(const char* eventKey, unsigned int listenerGUID)
{
	//Checking if eventKey exists
	if (m_eventListeners.count(eventKey))
	{
		//Erasing listener from value std::map
		m_eventListeners.at(eventKey).erase(listenerGUID);
		std::cout << "Removed: " << listenerGUID << " from " << eventKey << " event\n";
	}
	else
		std::cout << "Error: Trying to remove listener from non existant event\n";
}

//---------------------------------------------------------------------------------------------------------------------
//	Completely removes event by its event key
//---------------------------------------------------------------------------------------------------------------------
void EventHandler::RemoveEvent(const char* eventKey)
{
	if (m_eventListeners.count(eventKey))
	{
		m_eventListeners.erase(eventKey);
		std::cout << "Removed: " << eventKey << " event\n";
	}
	else 
		std::cout << "Error: Trying to remove non existant event\n";
}

//---------------------------------------------------------------------------------------------------------------------
//	Updates every single listener that signed up for this event. 
//	Old: Uses const char* eventKey to trigger events. 
//---------------------------------------------------------------------------------------------------------------------
void EventHandler::TriggerEvent(const char* eventKey)
{
	//Making sure that eventKey exists
	if (m_eventListeners.count(eventKey))
	{
		//Itorating on second(value) std::map 
		auto itor = m_eventListeners.at(eventKey).begin();
		while (itor != m_eventListeners.at(eventKey).end())
		{
			auto tempItor = itor;
			++itor;
			//Updating every object that regestered. 
			tempItor->second->EventUpdate(eventKey);
		}
	}
	else
		std::cout << "Error: " << eventKey << " :Event like that does not exist\n";
}

//---------------------------------------------------------------------------------------------------------------------
//	Updates every single listener that signed up for this event. 
//	New: Takes Event* to trigger event
//---------------------------------------------------------------------------------------------------------------------
void EventHandler::TriggerEvent(Event* pEvent)
{
	//Making sure that eventKey exists, and it is not nullEvent
	if (m_eventListeners.count(pEvent->GetEventKey()) && pEvent->GetEventKey() != g_kNullEvent)
	{
		//Itorating on second(value) std::map 
		auto itor = m_eventListeners.at(pEvent->GetEventKey()).begin();
		while (itor != m_eventListeners.at(pEvent->GetEventKey()).end())
		{
			auto tempItor = itor;
			++itor;
			//Updating every object that regestered. 
			tempItor->second->EventUpdate(pEvent->GetEventKey());
		}
	}
	else
		std::cout << "Error: " << pEvent->GetEventKey() << " :Event like that does not exist\n";

	//Cleaning up
	delete pEvent;
}

//---------------------------------------------------------------------------------------------------------------------
// Used to properly clean up all of the maps. And shut down EventHandler. 
//---------------------------------------------------------------------------------------------------------------------
void EventHandler::ShutDown()
{
	//Cleanin second map   
	auto it = m_eventListeners.begin();
	while (it != m_eventListeners.end())
	{
		auto tempIt = it;
		++it;
		//Dumping things
		tempIt->second.clear();
	}

	//Clearing listeners
	m_eventListeners.clear();

}