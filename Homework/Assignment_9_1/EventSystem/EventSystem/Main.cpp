//	Main.cpp
//	Name: Vadim Osipov
//	Date: 11/4

#include <iostream>
#include <conio.h>

#include "Objeicts\Object.h"
#include "ObjectBuilder\ObjectBuilder.h"

#include "EventListener\EventKeys.h"
#include "EventHandler\EventHandler.h"

//Events
#include "Events\EventOnActionMove.h"
#include "Events\EventOnSpawn.h"
#include "Events\EventOnDeath.h"


//---------------------------------------------------------------------------------------------------------------------
//	To Rez: 
//	At firts I did not realize that you want us to have events to be made as classes(so much for actually reading the assigment)
//	So I made events to be something that can be triggered by the calling a EventHandler::OnEvent(const char* eventKey)
//	That would trigger all of the Event Subscribers to take the action. 
//
//	Added: Event.h - Now events have to iherit from this class, and there is a function in EventHandler::TriggerEvent(Event* pEvent)
//	that takes event pointer, and triggers this event by Event string. Every object that signed up for this even will receive update. 
//
//	Event Listeners: 
//	Event listeners need to register for events 
//		- Event listener can register for event by calling SubscriveToEvent function of EventHandler that will take a const char* 
//		of event name. It is trongly advised to use EventKeys.h to have/use global event names. But user can create hes own runtime events. 
//		and objects can register to those. The only downside of that is that event cannot be triggered by Event class. Runtime event can only be triggered by
//		passing string to EventHandler::TriggerEvent()
//
//	Event Trigger objects: 
//	I have not created those. Event Listeners and Events are being called from Main. I thought that is a easier way to represent it. 
//	I could have put them in separate class - but that class would have nothing to do with event system. 
//---------------------------------------------------------------------------------------------------------------------


void ProcessInput(char input, bool& isRunning);

void main()
{
	bool isRunning = true;
	std::cout << "\t\tEvent System Demo" << std::endl;
	
	std::cout << "---=== Creating objects, Subscribing to events ===---\n";
	//---------------------------------------------------------------------------------------------------------------------
	//	Creating Objects - They have no Idea about events or EventHandler 
	//---------------------------------------------------------------------------------------------------------------------
	Object* objectA = ObjectBuilder::GetInstance()->CreateObject();
	Object* objectB = ObjectBuilder::GetInstance()->CreateObject();
	Object* objectC = ObjectBuilder::GetInstance()->CreateObject();


	
	//---------------------------------------------------------------------------------------------------------------------
	//	Subscribing to events - Pulling events from EventKey.h Also just to test - Creating some random event 
	//---------------------------------------------------------------------------------------------------------------------
	//ObjectA gets one event
	objectA->SubscribeToEvent(g_kOnSpawn);
	//ObjectB gets three events
	objectB->SubscribeToEvent(g_kOnAction_Move);
	objectB->SubscribeToEvent(g_kOnDeath);
	objectB->SubscribeToEvent(g_kOnSpawn);
	//ObjectC gets some dumb events - and breaks stuff 
	objectC->SubscribeToEvent("SomeDumbEvent");


	//---------------------------------------------------------------------------------------------------------------------
	//	Main Game Loop - Just asking for player input. Q, W, E, R, T - Mapped to some events in ProcessInput()
	//---------------------------------------------------------------------------------------------------------------------
	while (isRunning)
	{
		std::cout << "\n\n\n\n---=== In Game Loop ===---\n";
		std::cout << "---=== Press: Q, W, E, R, T - to trigger events ===---\n";
		std::cout << "---=== any other key to exit loop ===---\n\n\n\n";
		ProcessInput(_getch(), isRunning);
	}

	std::cout << "---=== Exit key pressed, Unsubscribing from events ===---\n";

	//---------------------------------------------------------------------------------------------------------------------
	//	Unsubscribing objects from events 
	//---------------------------------------------------------------------------------------------------------------------
	//ObjectA unsubscribing
	objectA->UnsubscribeFromEvent(g_kOnSpawn);
	
	//ObjectB unsubscribing
	objectB->UnsubscribeFromEvent(g_kOnAction_Move);
	objectB->UnsubscribeFromEvent(g_kOnDeath);
	objectB->UnsubscribeFromEvent(g_kOnSpawn);
	
	//ObjectC unsubscribing
	objectC->UnsubscribeFromEvent("SomeDumbEvent");

	_getch();

	//MUST! - Cleaning Event Handler up. 
	EventHandler::GetInstance()->ShutDown();
}

//---------------------------------------------------------------------------------------------------------------------
//	Simple Input switcher. Calls an event by the Keyboard Key pressed. 
//---------------------------------------------------------------------------------------------------------------------
void ProcessInput(char input, bool& isRunning)
{
	switch (input)
	{
	//First three cases use new system with calling classes to trigger events
	case 'q':
		EventHandler::GetInstance()->TriggerEvent(new EventOnSpawn);
		break;
	case 'w':
		EventHandler::GetInstance()->TriggerEvent(new EventOnDeath);
		break;
	case 'e':
		EventHandler::GetInstance()->TriggerEvent(new EventOnActionMove);
		break;
	//Last two cases: Use string to trigger events. I could have completely taken it out. But I decided to live it in. 
	case 'r':
		EventHandler::GetInstance()->TriggerEvent("SomeDumbEvent");
		break;
	case 't':
		EventHandler::GetInstance()->TriggerEvent("SomeDumbEventThatNoOneSubscribedTo");
		break;
	default:
		isRunning = false;
		break;
	}
}