//	EventOnSpawn.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once

#include "Event.h"
#include "../EventListener/EventKeys.h"

class EventOnSpawn : public Event
{
public: 
	EventOnSpawn() : Event(g_kOnSpawn) { }
};