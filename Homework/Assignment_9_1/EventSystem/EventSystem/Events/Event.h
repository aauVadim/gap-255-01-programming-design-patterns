//	Event.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once

#include "../EventListener/EventKeys.h"
//---------------------------------------------------------------------------------------------------------------------
//	To Rez: 
//	This is base event class. Objects that inherit from this object have to give it a key in constructor. 
//	if there is no key - event will not be triggered.
//	
//	To Create an Event: 
//	Inhering from this class - in child constructor pass eventKey string from EventKeys.h
//---------------------------------------------------------------------------------------------------------------------

class Event 
{
	const char* m_pEventKey;

public:
	//Making sure that event gets key
	//Children must provide own key, otherwise event will not be called.
	Event(const char* pEventKey = g_kNullEvent) : m_pEventKey(pEventKey) { }
	virtual ~Event() {}

	//Getter
	const char* GetEventKey() const { return m_pEventKey; }
};