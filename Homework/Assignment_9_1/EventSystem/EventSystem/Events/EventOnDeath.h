//	EventOnDeath.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once

#include "Event.h"
#include "../EventListener/EventKeys.h"


class EventOnDeath : public Event
{
public:
	EventOnDeath() : Event(g_kOnDeath) { }
};