//	EventOnActionMove.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once

#include "Event.h"
#include "../EventListener/EventKeys.h"

class EventOnActionMove : public Event
{
public:
	EventOnActionMove() : Event(g_kOnAction_Move) { }
};