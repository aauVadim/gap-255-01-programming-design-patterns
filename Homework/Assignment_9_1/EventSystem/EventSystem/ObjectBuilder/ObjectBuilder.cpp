//	ObjectBuilder.cpp
//	Name: Vadim Osipov 
//	Date: 11/7

#include "ObjectBuilder.h"
#include "../Objeicts/Object.h"

#include "windows.h"

ObjectBuilder* ObjectBuilder::m_pInstance = nullptr;

ObjectBuilder* ObjectBuilder::GetInstance()
{
	if (m_pInstance == nullptr)
		m_pInstance = new ObjectBuilder;

	return m_pInstance;
}

//---------------------------------------------------------------------------------------------------------------------
//	Method used to create an object. Also gives unique GUID.Data1 to object
//---------------------------------------------------------------------------------------------------------------------
Object* ObjectBuilder::CreateObject()
{
	GUID objectGUID;
	CoCreateGuid(&objectGUID);
	
	Object* newObject = new Object(objectGUID.Data1); 

	return newObject;
}