//	ObjectBuilder.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once

class Object;

//---------------------------------------------------------------------------------------------------------------------
//	This class instantiates the object, 
//	Builer gives every object unique GUID so objects can be unique. 
//---------------------------------------------------------------------------------------------------------------------

//Singleton
class ObjectBuilder
{
	static ObjectBuilder* m_pInstance; 
public: 
	static ObjectBuilder* GetInstance();

	Object* CreateObject();

private:
	ObjectBuilder() { }
};