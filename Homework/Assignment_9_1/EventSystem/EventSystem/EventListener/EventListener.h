//	EventListener.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once

//---------------------------------------------------------------------------------------------------------------------
//	EventListener: 
//		EventListener is an Interface for object to subscribe for events in the "game"
//		Object simply has to override twose three functions for it to work. 
//		For Example please see Object.h/.cpp
//---------------------------------------------------------------------------------------------------------------------
class EventListener
{
public:
    virtual ~EventListener() { }

	//Allows Object to subscribe for events
	//Has to be called if desired to recieve event updates. 
	virtual void SubscribeToEvent(const char* eventKey) = 0;
	
	//Allows Objects to Unsubscribe from events.
	//Has to be called if desired to Unsubscribe from event
	virtual void UnsubscribeFromEvent(const char* eventKey) = 0;

    //Called when Event is triggered. 
    virtual void EventUpdate(const char* eventKey) = 0;
};