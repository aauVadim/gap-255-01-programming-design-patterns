//	EventKeys.h
//	Name: Vadim Osipov 
//	Date: 11/4

#pragma once 

#include <iostream>

//---------------------------------------------------------------------------------------------------------------------
//	Collection of specified event keys. 
//	If you like to add an event - it makes sence to place a key into this file,
//	but it is not neccesary. You can create keys at run time, the only problem with that is that it can break because of typo. 
//---------------------------------------------------------------------------------------------------------------------


static const char* g_kOnDeath          = "OnDeath";
static const char* g_kOnSpawn          = "OnSpawn";
static const char* g_kOnAction_Move    = "OnAction_Move"; 

//Making sure that children pass their event keys. Event will not be triggered if this key is present.
static const char* g_kNullEvent		   = "nullEvent";