// Player.h
#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "Actor.h"
#include "Player_Inventory.h"
#include <deque>

class Player_Inventory;

class Player : public Actor
{
	std::deque<Player_Inventory*> m_inventoryItems;

    static const int k_baseScore;
    static const int k_hitPointsWeight, k_goldWeight, k_moveCountWeight;

public:
	Player(int x, int y);
	~Player();
	//Pure virtual overrrite
	virtual void Draw() const override;

    void DrawUi() const;
    virtual bool Update() override;

    int CalculateScore() const;
	//Only player can end the game
	virtual void EndGame() override;

private:
    void Move(int deltaX, int deltaY);
	bool UseItem(char& input);
	void AddInventoryItem(Player_Inventory* item);
};

#endif
