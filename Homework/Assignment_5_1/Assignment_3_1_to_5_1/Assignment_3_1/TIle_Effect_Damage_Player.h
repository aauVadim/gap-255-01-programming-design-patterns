//	Tile_Effect_Damage_Player.h
//	Name: Vadim Osipov 
//	Date: 10/10

#include "Tile_Effect.h"

#include "Actor.h"

#include <utility>

class TileEffectDamagePlayer : public TileEffect
{
	//[vo] - To Rez I've just used your code for that. Seem to make some sence. 
	typedef std::pair<int, int> DamageRange;
	static const DamageRange s_damageRange; 

public:
	//You will need to specify how many tiles this tile can use its effect 
	TileEffectDamagePlayer(int numberOfTriggers);
	virtual void ApplyEffect(Actor* pActor) override;
};