#pragma once

#include "Utility.h"

#include "Player_Inventory.h"

class Inventory_MimicDetector :
	public Player_Inventory
{
	int m_useRadius = 0;

public:
	Inventory_MimicDetector(Player* pPlayer, int useRadius)
		: Player_Inventory(pPlayer)
		, m_useRadius(useRadius)
	{
		m_useAmount = 3;
		m_usesLeft = 3;
		m_useChar = 'e';
		m_itemID = 1;
	};

	~Inventory_MimicDetector() 
	{

	};

	virtual void Use(char& input) override
	{
		//If the char matches - use item
		if (input == m_useChar)
		{
			Consume();
		}
	};

	virtual void Consume() override
	{
		//Checking if we have any uses left
		if (m_usesLeft > 0)
		{
			--m_usesLeft;

			for (int y = 0; y < g_pWorld->GetWorldHeight(); ++y)
			{
				for (int x = 0; x < g_pWorld->GetWorldWidth(); ++x)
				{
					//In distance
					if ((int)Utility::GetInstance()->GetDistance(m_pPlayer->GetX(), m_pPlayer->GetY(), x, y) <= m_useRadius)
					{
						//disable all tiles in radius
						g_pWorld->GetTile(x, y)->Disarm(m_itemID);
					}
				}
			}
		}
	};
};

