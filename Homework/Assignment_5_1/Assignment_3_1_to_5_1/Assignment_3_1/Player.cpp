// Player.cpp
#include "Player.h"
#include <iostream>
#include <conio.h>

//Inventory
#include "Player_Inventory.h"
#include "Inventory_MimicDetector.h"

using std::cout;


const int Player::k_baseScore = 1000;
const int Player::k_hitPointsWeight = 100;
const int Player::k_goldWeight = 10;
const int Player::k_moveCountWeight = 10;

Player::Player(int x, int y) 
	: Actor(x, y)
{
	Player_Inventory* newItem = new Inventory_MimicDetector(this, 1);
	AddInventoryItem(newItem);
}

Player::~Player()
{
	for (Player_Inventory* item : m_inventoryItems)
	{
		delete item;
	}
	m_inventoryItems.clear();
}

void Player::Draw() const
{
    if (!IsDead())
	    cout << "@";
    else
        cout << "~";
}

void Player::DrawUi() const
{
	int mimicUsedTimes = m_inventoryItems[0]->GetUsedTimes(); //Assuming that mimic is first in array
	int mimicUsesLeft = m_inventoryItems[0]->GetUsesLeft();
	cout << "HP: " << m_hitPoints << "  Gold: " << m_gold << "  Move Count: " << (m_moveCount + mimicUsedTimes) << "  Mimic Uses Left: " << mimicUsesLeft << "  Score: " << CalculateScore() << "\n\n";
}

bool Player::Update()
{
    if (IsDead())
        return false;

    char input = _getch();

    switch (input)
    {
        case 'q':
            return false;  // quitting

        case 'w':
            Move(0, -1);
            break;

        case 'a':
            Move(-1, 0);
            break;

        case 's':
            Move(0, 1);
            break;

        case 'd':
            Move(1, 0);
            break;

        default:
			UseItem(input);
            //cout << "Invalid input";
            break;
    }

    return true;
}

void Player::Move(int deltaX, int deltaY)
{
	m_x += deltaX;
	m_y += deltaY;
    ++m_moveCount;
}


void Player::AddInventoryItem(Player_Inventory* item)
{
	m_inventoryItems.push_back(item);
}

int Player::CalculateScore() const
{
	int score = k_baseScore + (m_hitPoints * k_hitPointsWeight) + (m_gold * k_goldWeight) - (m_moveCount * k_moveCountWeight);
    return score;
}

bool Player::UseItem(char& input)
{
	for (Player_Inventory* item : m_inventoryItems)
	{
		item->Use(input);
	}

	return true;
}

void Player::EndGame()
{
	g_pWorld->EndGame();
}