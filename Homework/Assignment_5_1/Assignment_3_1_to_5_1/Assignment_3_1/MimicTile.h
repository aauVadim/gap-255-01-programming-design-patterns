// MimicTile.h
#ifndef __MIMICTILE_H__
#define __MIMICTILE_H__

#include "Tile.h"

class MimicTile : public Tile
{
	//Special case - TODO: Do I need it? 
	bool m_isHidden; 

public:
	
	int m_itemID; 

    MimicTile();
    virtual void Draw() override;
	
	//Used to disable mimic tiles
	virtual void Disarm(int itemID) override;
	virtual void OnEnter(Actor* pActor) override;
};

#endif

