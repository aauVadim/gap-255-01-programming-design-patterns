//	Tile_Effect.h
//	Name: Vadim Osipov
//	Date: 10/10

#pragma once

class Actor;

class TileEffect
{
protected:
	int m_numberOfTriggers = 0;	//Have to set in child. 

public:

	//Getter
	int GetTriggersRemaning()	{ return m_numberOfTriggers; }

	TileEffect(int numberOfTriggers) : m_numberOfTriggers(numberOfTriggers) { };
	virtual ~TileEffect() { };

	//Apply effect - override in the child if needed. 
	//Does nothing by default 
	virtual void ApplyEffect(Actor* pActor) { };
};