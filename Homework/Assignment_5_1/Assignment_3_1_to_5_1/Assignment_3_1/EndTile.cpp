// EndTile.cpp
#include "EndTile.h"
#include "World.h"
#include "Actor.h"
#include <iostream>

extern World* g_pWorld;  // let's us access a global variable declared in another CPP

EndTile::EndTile()
{
	m_tileType = TileType::k_interactible;
}

void EndTile::Draw()
{
    std::cout << "H";
}

void EndTile::OnEnter(Actor* pPlayer)
{
    pPlayer->EndGame();
}
