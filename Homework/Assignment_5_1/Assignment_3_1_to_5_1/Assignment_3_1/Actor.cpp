// Actor.cpp
// Name: Vadim Osipov
// Date: 9/25

#pragma once

#include "Actor.h"
#include <iostream>

const int Actor::k_maxHitPoints = 10;

Actor::Actor(int x, int y)
	: m_x(x)
	, m_y(y)
	, m_hitPoints(k_maxHitPoints)
	, m_gold(0)
	, m_moveCount(0)
{

}
//-------------------------------------------------------------------------------------------------
// Applyes given damage
//-------------------------------------------------------------------------------------------------
void Actor::Damage(int amount)
{
	m_hitPoints -= amount;

	if (m_hitPoints < 0)
		m_hitPoints = 0;
}
//-------------------------------------------------------------------------------------------------
// Adds gold to inventory
//-------------------------------------------------------------------------------------------------
void Actor::AddGold(int amount)
{
	m_gold += amount;
}