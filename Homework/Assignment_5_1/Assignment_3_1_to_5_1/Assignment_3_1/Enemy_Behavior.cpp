//	Enemy_Behavior.cpp
//	Name: Vadim Osipov
//	Date: 10/10

#include "Enemy_Behavior.h"
#include "Enemy.h"
#include "Player.h"


//Must specify direction fo close range actions
int Enemy_Behavior::GetDirection(bool isX, bool shouldChase)
{
	int deltaX = 1;
	int direction = 1;
	//Choosing direction. Love them one line statements
	(shouldChase) ? (direction *= -1) : (direction *= 1);
	
	if (isX)
	{
		//Check on X and Return X
		if (m_pOurBody->GetX() != m_pPlayer->GetX())
		{
			if (IsMoveAllowed(m_pOurBody->GetX() + (deltaX * direction), m_pOurBody->GetY()))
			{
				if (m_pOurBody->GetX() > m_pPlayer->GetX())
				{
					//Return positive
					return deltaX * direction;
				}
				else if (m_pOurBody->GetX() < m_pPlayer->GetX())
				{
					//Return negative
					return (deltaX * -1) * direction;
				}
			}
		}
	}
	else
	{
		//Check on Y and Return Y
		if (m_pOurBody->GetY() != m_pPlayer->GetY())
		{
			if (IsMoveAllowed(m_pOurBody->GetX(), m_pOurBody->GetY() + (deltaX * direction)))
			{
				if (m_pOurBody->GetY() > m_pPlayer->GetY())
				{
					//Return positive
					return deltaX * direction;
				}
				else if (m_pOurBody->GetY() < m_pPlayer->GetY())
				{
					//Return negative
					return (deltaX * -1) * direction;
				}
			}
		}

	}
	return 0;
}

bool Enemy_Behavior::IsMoveAllowed(int desiredX, int desiredY)
{
	if (desiredX >= 0 && desiredX < g_pWorld->GetWorldHeight()
		&& desiredY >= 0 && desiredY < g_pWorld->GetWorldWidth())
		return true;
	else
		return false;
};