// TreasureTile.cpp
#include "TreasureTile.h"
#include "Actor.h"
#include <iostream>

#include "Tile_Effect_Add_Gold.h"

const TreasureTile::TreasureRange TreasureTile::s_treasureRange(50, 150);

TreasureTile::TreasureTile()
{
	//Some random amount
    int amount = (rand() % (s_treasureRange.second - s_treasureRange.first)) + s_treasureRange.first;
	m_pEffect = new TileEffectAddGold(1, amount);

	m_tileType = TileType::k_interactible;

}

void TreasureTile::Draw()
{
    if (m_pEffect->GetTriggersRemaning() > 0)
        std::cout << "$";
    else
        std::cout << ".";  // floor
}