// BombTile.cpp
#include "BombTile.h"
#include "Actor.h"
#include <iostream>

#include "TIle_Effect_Damage_Player.h"

BombTile::BombTile()
{
    //For some reason it has to be here
	m_tileType = TileType::k_interactible;
	//Adding a Bomb effect to this tile
	m_pEffect = new TileEffectDamagePlayer(1);
}

void BombTile::Draw()
{
    if (m_pEffect->GetTriggersRemaning() > 0)
        std::cout << "*";
    else
        std::cout << "#";
}