// MimicTile.cpp
#include "MimicTile.h"
#include "Actor.h"
#include <iostream>

#include "TIle_Effect_Damage_Player.h" 

using std::cout;

MimicTile::MimicTile()
	: m_isHidden(true)
{
	m_tileType = TileType::k_interactible;
	m_itemID = 1;

	//Adding a bomb/damage effect
	m_pEffect = new TileEffectDamagePlayer(1);
}

void MimicTile::Draw()
{
	//Special case
	if (m_isHidden)
		cout << "$";
	else
	{
		if (m_pEffect->GetTriggersRemaning() > 0)
			cout << "*";
		else
			cout << "#";
	}
}

void MimicTile::Disarm(int itemID)
{
	if (itemID == m_itemID)
	{
		m_isHidden = false;
	}
}

void MimicTile::OnEnter(Actor* pActor)
{
	//Special case
	m_isHidden = false;

	if (m_pEffect)
		m_pEffect->ApplyEffect(pActor);
}
