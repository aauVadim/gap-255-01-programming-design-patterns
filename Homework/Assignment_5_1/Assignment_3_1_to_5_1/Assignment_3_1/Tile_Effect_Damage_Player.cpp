//	Tile_Effect_Damage_Player.h
//	Name: Vadim Osipov 
//	Date: 10/10

#include "TIle_Effect_Damage_Player.h"

const TileEffectDamagePlayer::DamageRange TileEffectDamagePlayer::s_damageRange(3, 6);

TileEffectDamagePlayer::TileEffectDamagePlayer(int numberOfTriggers)
	:TileEffect(numberOfTriggers)
{

}

void TileEffectDamagePlayer::ApplyEffect(Actor* pActor)
{
	if (m_numberOfTriggers > 0)
	{
		int damage = (rand() % (s_damageRange.second - s_damageRange.first)) + s_damageRange.first;
		pActor->Damage(damage);
		--m_numberOfTriggers;
	}
};