// Assignment_3_1.cpp
#include "World.h"
#include <time.h>
#include <stdlib.h>
#include <conio.h>

//---------------------------------------------------------------------------------------------------------------------
// Game Design:
// 
// Player:
// The game is a grid-based world where the player is represented by the '@' symbol.  The player moves around with 
// WASD.  You can quit the game by pressing 'q'.  The player starts with 10 hit points.
// 
// Bombs:
// Scattered in the world are bombs, which are shown with an asterisk (*).  Touching one of this tiles will cause it 
// explode, damaging the player, and change its icon to a pound sign (#).  The bomb will now be destroyed, so touching
// it further won't do anything.
// 
// Treasure Chests:
// Treasure chests are shown with a dollar sign ($).  Collecting a treasure chest treasure will add a random amount 
// of gold to the player's inventory and cause the object to disappear.  Be careful!  Some treasure chests are 
// actually mimics and will explode just like a bomb!  There is guaranteed to be at least one mimic in each level.
// 
// Exit:
// At the bottom right is the exit, show with a capital H.  Entering this tile will end the game and give you your 
// score.
// 
// Win/Lose:
// The object of the game is to rack up as many points as possible.  You lose points when your hit points drop and 
// for every move you make.  You gain points for collecting gold.  If you can make it to the exit tile, you win.  If 
// your hit points drop to 0 or you fall off the edge of the map, you die and the game is over.
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Assignment:
// 
// Overall Grade: B+
// General Notes:
//     * You stumbled a bit on the first two items since you never actually met the criteria.
//     * You did very well on the other items, beyond what I was expecting.  The only reason you didn't get an 
//       A on this was because of those first two items.
// 
// 1) The tiles have some shared behavior.  Specifically, the MimicTile looks like a treasure tile but acts like a 
//    bomb tile.  Using the strategy pattern, encapsulate all the behaviors of the tiles and use it so that none of 
//    the behaviors are duplicated.
// [rez] 
//     * The mimic tile and bomb tiles still have duplicate code in OnEnter(), so this item didn't really get done.
// 
// 2) Several of the tiles use a state.  The current implementation just uses an enum and handles in code.  Can you 
//    modify these tiles to use a state machine instead?  Is this even necessary?  Defend your answer (see below).
// [rez]
//     * The mimic tile and bomb tiles still have states.  I didn't see an explanation as to whether or not this 
//       is necessary.
// 
// 3) Add a mimic detector to the game.  It should work like this:
//        a) When the player presses the 'e' key, it looks at all 8 adjacent tiles.  If any are mimics, they are 
//           immediately revealed as bombs.
//        b) The player can do this three times per game.
//        c) Using the mimic detector costs a single move, but it doesn't effect the score in any other way.
// [rez]
//     * This was handled very well.  Good job!
// 
// 4) Add enemies to the game.  They should be effected by tiles just like the player.  For example:
//        a) If they trigger a bomb, the enemy will die and the bomb will be destroyed.
//        b) If they land on a treasure chest, they will collect the treasure.
//        c) If they land on a mimic, the bomb will be triggered and they will die.
//        d) If they land on a teleporter tile (see below), they will teleport.
// [rez]
//     * Excellent.
// 
// 5) The enemy behaviors should be as follows:
//        a) Enemy #1:
//               1) Wander randomly by choosing one of the four empty adjacent tiles.  They will avoid all special 
//                  tiles.
//               2) If the player is close (within 5 tiles), the enemy will always move towards the player.  They 
//                  no longer avoid special tiles and will trigger any bomb, mimic, teleporter, or treasure chest.
//               3) Their symbol should be an exclamation point (!).
//        b) Enemy #2:
//               1) Wander randomly by choosing one of the four empty adjacent tiles.  They will avoid all special 
//                  tiles.
//               2) If the player is close (within 5 tiles), the enemy will always move away from the player.  They 
//                  no longer avoid special tiles and will trigger any bomb, mimic, teleporter, or treasure chest.
//               3) Their symbol should be a question mark(?).
// [rez] 
//     * The behaviors were good, though there's some duplicated code (we talked about it in my office).
// 
// 6) Once again, there are a few places where the existing architecture is not ideal.  In fact, there at least two 
//    instances of things I've explicitly said to always watch out for.  See how many you can find.  Justify your 
//    decisions (see below).
// 7) Keep notes about your architectural decisions and defend them.  What was your motivation?  Why did you choose
//    one technique over another?  Those notes should be in the comment block below.
// [rez]
//     * You made some good decisions here.  It feels like this stuff is sinking in for you.
// 
// ***8) Add a teleporter tile.  It should work like this:
//        a) Teleporter tiles always come in pairs.
//        b) This pair is linked.  When the player steps onto one, he will be teleported to the other.
//        c) There is a small chance you will get one or more teleporter pairs when the world is first generated.
//           Play with the probability to make it feel right.
//        d) Their symbol should be an ampersand (&).
//        e) For this feature, you will be graded HEAVILY on how clean the teleporter's architecture has been 
//           implemented.  If it works but the code is super messy and/or hacky, you might as well have not done at 
//           all.
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
// Vadim Osipov 
// Date: 9/27
//
// Created "Actor" class where Player and Enemy inherit from. 
//		- Actor class has most of the functions that Enemy and Player would share. 
// [rez]
//     * Good.
// 
// Enemies: 
//		- Enemies have Enemy_Behavior class(base) array on them. Every behavior has its overwritten Act() Function.  
//		- Enemy Constructor - Takes if evemy is defencive or offencive. Also posX and posY of Unit. Reference to its body and to the Player. Also takes action threshold(see below)
//		- Act() Function on Behavior is called from Update() of Enemy Class
//		- Action is "executed" or "not" according to the distance to the Player. Action threshold is specified in enemy constructor
//		- For now there is three classes: 
//			- Enemy_FreeWalk - Makes enemy freely wander arond the map. Every evemy has such behavior added to them by default.
//			  free walk will be execudet as long as player is out of "action radius" - meaning that if player gets whith in 5 units
//			  enemy will stop wondering around and start to execute any another action(RunAway, ChasePlayer) given to him. 
//			- Enemy_ChasePlayer - follows player. If it reaches distance of 1. It will stop.
//			- Enemy_RunAway - Runs away from the player. will trigger all the Traps and Chests on the way
//			- I kept Enemy_RunAway and Enemy_ChasePlayer in separate files though they are practically the same only thing they do differently is one sets "1" and another sets "-1".
//			  idea behind of that is every behavior type might be different.and I'd like to inforce that every type is kept separately - so its clealy seen what does what in future additions to the game
//			  example: Archer Class - does not get close to enemy but shoots and runs away, Hellhound Class - chages at the evemy and explodes. I hope I'm making my point of view clear. 
// [rez] 
//     * Yes, excellent.  Creating these as different behaviors is correct.
// 
// Utility Singleton: - Just a helper Singleton. Very useful.
//		- GetDistance() function - returns a double of distance between x1, y1 and x2, y2. 
//		- ConvertToLiniar() function - takes X and Y and returns linear value for them. 
//		- ConvertX() and ConvertY() - take linear value and return X or Y accordinly.  
// [rez]
//     * Good.
// 
// Player Inventory system:
//		- Base class: Player_Inventory responsible for holding virtuals, pure virtuals and shared functions between children. 
//		  also base class is responsible for holding itemID variable and children need to set it.
//		- Constructor: Takes reference to Player that owns the item, "itemID"(see below) and item effect radius(example: Mimic Detector radius: 1). 
//		- "itemID" - variable that is send to affected tiles by the item. if "itemID" of Item and "itemID" of Tile match - Item will affect tile(Mimic detector will reveal Mimic Chest). 
//		- Inventory_MimicDetector - Simple class that searches within radius of 1 around player and reveals any Mimics that were found.
// [rez]
//     * This is one of the strongest pieces of your project.  Making the mimic detector a generic inventory item 
//       is great.
// 
// Additions/Changes to Tile class: 
//		- Tile::TileType enumirator - checked if Enemy can step on the item in FreeWalk mode. 
//		- itemID - checked if this tile should be affected by the item player used. Right now used for MimicDetector only. 
// [rez] 
//     * The itemID concept is fine.  You're basically doing a match to see if your inventory item can affect the 
//       tile.  This is a good way to keep them separate, so the player doesn't have to care about the tile itself.
//       I might actually turn this into a bitfield of flags so that tiles can have multiple types.
// 
// Side notes: 
//		- I wasn't sure if I should keep behaviors(Inventory/Enemy) in .cpp and .h or just .h so I decided to live them in only .h file. 
//		  That way project has less files and it is easier for me to find/edit behaviors. 
// [rez]
//     * Default to creating a CPP rather than putting behaviors in the header file.  Here are a few reasons why:
//     * It keeps header files clean.
//     * It keeps you from having to include a bunch of stuff in the header file.
//     * Certain things, like global variables, don't play nicely in header files.
// 
//		- For "!" enemies - end behavior was never specified. Meaning that there is no behavior when enemy "!" gets next to the player.
// [rez] 
//     * What would you do in a real game?  You'd probably ask the designer (who is me in this case).
//---------------------------------------------------------------------------------------------------------------------

//---------------------------------------------------------------------------------------------------------------------
//	Assigment 5.1 Notes 
//---------------------------------------------------------------------------------------------------------------------
//	Vadim Osipov
//	Date: 10/10
//
//	Enemie_Behavior Class: 
//		- Created GetDirection(bool isX, bool shouldChase) function that allows Enemies to pick their new direction from just specifing which coordinate and should
//		that enemy run away or chase player. For FreeWalk mode - GetDirection is overriten by behavior. In order to give random new coordinate every turn.  
//		Every new direction is taked from this one function instead of ChooseX and ChooseY functions.
//	Actor Class:
//		- Made SetX(int x) and SetY(int y) in Actor Class to get rid of returning variable by reference. I figured that you dond really like that strategy.  
//	Added: 
//		- Tile_Effect - Base class for effects that tile can posest. Idea is that tile can Damage, Poison, Paralize, Heal and etc. and that is all can be done with 
//		Effects. Right now tile can hold only one effect - easly can be changed to hold multiple effects.
//		With using this Effect system - I have actual tiles only for two things: Draw() different Char's and hold some special Effect if needed. Exit Tile is Exeption.  
//			- Tile_Effect_Damage_Player - Applies some number of damage to the Player/Enemy
//			- Tile_Effect_Add_Gold - Adds gold to the Player/Enemy
//---------------------------------------------------------------------------------------------------------------------


const int k_worldWidth = 20;
const int k_worldHeight = 20;

World* g_pWorld = nullptr;

void main()
{
	srand((unsigned int)time(nullptr));

	// create the world
	g_pWorld = new World;
	g_pWorld->Init(k_worldWidth, k_worldHeight);

	// create the player
	g_pWorld->CreatePlayer();

	// now that the player has been placed, generate the world
	g_pWorld->GenerateWorld();

    // main game loop
    while (!g_pWorld->IsGameOver())
    {
        g_pWorld->Draw();
        g_pWorld->Update();
    }

    delete g_pWorld;
    g_pWorld = nullptr;
    _getch();
}

