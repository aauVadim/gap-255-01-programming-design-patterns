// Player_Inventory.h
// Name: Vadim Osipov
// Date: 9/26

#pragma once

#include "World.h"
#include "Tile.h"

extern World* g_pWorld;

class Player;

class Player_Inventory
{
protected: 
	int m_useAmount = 0;	//Max Uses; Defauld set; Needs to be changed in child
	int m_usesLeft = 0;		//Current uses left;
	int m_useChar = '/';	//Defauld set; Needs to be changed in child
	Player* m_pPlayer;

public: 

	int m_itemID = 0;		//Defauld set; Needs to be changed in child

	Player_Inventory(Player* pPlayer)
		: m_pPlayer(pPlayer){ };

	virtual ~Player_Inventory() { };
	virtual void Use(char& input) = 0;

	//Utility - returns amount of uses left on item
	int GetUsesLeft() const { return m_usesLeft; }

	//Utility - returns amount of times item has been used
	int GetUsedTimes() const { return (m_useAmount - m_usesLeft); }

protected: 
	virtual void Consume() = 0;
};