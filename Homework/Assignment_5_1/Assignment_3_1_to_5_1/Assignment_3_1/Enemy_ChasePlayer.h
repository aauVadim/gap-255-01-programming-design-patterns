// Enemy_ChasePlayer.h
// Name: Vadim Osipov
// Date: 9/25

#pragma once

#include <iostream>

#include "Enemy_Behavior.h"
#include "Enemy.h"
#include "Player.h"

class Enemy_ChasePlayer : public Enemy_Behavior
{
public:
	Enemy_ChasePlayer(Player* pPlayer, Enemy* pOurBody, int actionThreshold, int distanceToStopBeforePlayer)
		: Enemy_Behavior(pPlayer, pOurBody, actionThreshold, distanceToStopBeforePlayer) { };

	virtual void Act() override
	{
		//Distance to player
		int distance = (int)Utility::GetInstance()->GetDistance(m_pOurBody->GetX(), m_pOurBody->GetY(), m_pPlayer->GetX(), m_pPlayer->GetY());
		//If youre whithin your act range but not enter player tile
		if (distance <= m_actionThreshold && distance > m_distanceToStopBeforePlayer)
		{
			m_pOurBody->SetX(GetDirection(true, true));
			m_pOurBody->SetY(GetDirection(false, true));
		}
	}
};