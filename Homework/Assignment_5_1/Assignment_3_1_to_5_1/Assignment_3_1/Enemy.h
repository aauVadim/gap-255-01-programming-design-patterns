// Enemy.h
// Name: Vadim Osipov 
// Date: 9/25

#pragma once

#include "Actor.h"
#include "Player.h"
#include <deque>

class Enemy_Behavior;

class Enemy : public Actor
{
	std::deque<Enemy_Behavior*> m_behaviors;
	//Action change distance
	int m_distanceBeforeAction;
	//Stop before player distance
	int m_distanceToStopBeforePlayer; 
	Player* m_pPlayer;

public:
	bool m_isDefencive; 

public: 
	//Constructor
	//@pragma: 
	Enemy(int x, int y, bool isDefencive, Player* pPlayer, int distanceBeforeAction, int distanceToStopBeforePlayer);
	~Enemy();

	//pure virtual
	virtual void Draw() const override;  // assumes the cursor is at the right place
	virtual bool Update() override;



private: 
	void AddBehavior(Enemy_Behavior* pBehav);
};