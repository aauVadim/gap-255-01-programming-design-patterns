// Enemy_RunAway.h
// Name: Vadim Osipov
// Date: 9/25

#pragma once

#include <iostream>

#include "Enemy_Behavior.h"
#include "Enemy.h"
#include "Player.h"

class Enemy_RunAway : public Enemy_Behavior
{
public:
	Enemy_RunAway(Player* pPlayer, Enemy* pOurBody, int actionThreshold, int distanceToStopBeforePlayer)
		: Enemy_Behavior(pPlayer, pOurBody, actionThreshold, distanceToStopBeforePlayer) { };
	
	
	virtual void Act() override
	{
		int distance = (int)Utility::GetInstance()->GetDistance(m_pOurBody->GetX(), m_pOurBody->GetY(), m_pPlayer->GetX(), m_pPlayer->GetY());
		//if distance between this and player less/equals than action threshold - perform 
		if ( distance <= m_actionThreshold && distance > m_distanceToStopBeforePlayer)
		{
			m_pOurBody->SetX(GetDirection(true, false));
			m_pOurBody->SetY(GetDirection(false, false));
		}
	}
};