// Tile.h
#ifndef __TILE_H__
#define __TILE_H__

class Actor;

#include "Tile_Effect.h"


class Tile
{

public:
	//Used for items - Something like: Disable bomb, Disable Mimic, Disable Tram etc. 
	int m_itemID = 0; //Defauld set; Needs to be changed in child
	
	enum TileType
	{
		k_walkable, 
		k_interactible
	};
	TileType m_tileType;


public:
	Tile();
    virtual ~Tile() { }  // if your class is being used as a base class, it's best to have a virtual destructor
	virtual void Draw() = 0;

	//Defaulting to apply effect that is on this tile
	virtual void OnEnter(Actor* pActor);
	
	TileType GetTileType(){ return m_tileType; }

	//May be used in the future for Bombs, Mimics, Traps, with player Inventory things
	virtual void Disarm(int itemID) { };

protected:
	//Effect to triger for this tile to trigger
	TileEffect* m_pEffect;	//For now tile can have one effect, if needed in the future array of effects can be created (Stun, Paralize, Damage, Poison, etc.)
};

#endif

