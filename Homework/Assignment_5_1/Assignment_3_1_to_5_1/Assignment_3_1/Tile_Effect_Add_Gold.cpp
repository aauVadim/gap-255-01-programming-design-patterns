//	Tile_Effect_Add_Gold.cpp
//	Name: Vadim Osipov 
//	Date: 10/10

#include "Tile_Effect_Add_Gold.h"

TileEffectAddGold::TileEffectAddGold(int numberOfTriggers, int amountOfGold)
	: TileEffect(numberOfTriggers)
	, m_amountOfGold(amountOfGold)
{

}

void TileEffectAddGold::ApplyEffect(Actor* pActor)
{
	if (m_numberOfTriggers > 0)
	{
		pActor->AddGold(m_amountOfGold);
		--m_numberOfTriggers;
	}
}