//	Tile_Effect_Add_Gold.h
//	Name: Vadim Osipov 
//	Date: 10/10

#pragma once

#include "Tile_Effect.h"

#include "Actor.h"

class TileEffectAddGold : public TileEffect
{
	int m_amountOfGold;

public:
	TileEffectAddGold(int numberOfTriggers, int amountOfGold);
	virtual void ApplyEffect(Actor* pActor) override;
};