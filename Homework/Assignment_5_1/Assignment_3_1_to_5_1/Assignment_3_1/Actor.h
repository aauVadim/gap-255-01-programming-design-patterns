// Actor.h
// Name: Vadim Osipov
// Date: 9/25

#pragma once

class Actor
{
	static const int k_maxHitPoints;

	//Variables
protected:
	int m_x, m_y;
	int m_hitPoints;
	int m_gold;
	int m_moveCount;

	//Methods
public:
	//Base constructor
	Actor(int x, int y);
	virtual ~Actor(){};
	//Getter - Returns position 
	int GetX() { return m_x; }
	int GetY() { return m_y; }
	//Setter - Sets position
	int SetX(int x) { return m_x += x; }
	int SetY(int y) { return m_y += y; }

	int& GetMoveCount()	{ return m_moveCount; }
	//pure virtual
	virtual void Draw() const = 0;  // assumes the cursor is at the right place
	//Update yourself
	virtual bool Update() = 0;


	//Kill/Die
	bool IsDead() const { return (m_hitPoints <= 0); }
	void Damage(int amount);
	void Kill() { m_hitPoints = 0; }
	//Gold
	void AddGold(int amount);

	//Ending game with the actor - overritten in player
	virtual void EndGame() { };

};