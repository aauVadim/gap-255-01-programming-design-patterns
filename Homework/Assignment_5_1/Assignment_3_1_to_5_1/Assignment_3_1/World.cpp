// World.cpp
#include "World.h"
#include "Tile.h"
#include "Player.h"
#include "Enemy.h"

#include "FloorTile.h"
#include "TreasureTile.h"
#include "BombTile.h"
#include "MimicTile.h"
#include "EndTile.h"

#include "Utility.h"

#include <iostream>
#include <assert.h>

using std::cout;
using std::endl;

const World::TileProbability World::s_tileProbabilities[(int)TileType::k_numTiles] =
{
    World::TileProbability(800, TileType::k_floor),	//800
    World::TileProbability(75, TileType::k_bomb),	//75
    World::TileProbability(75, TileType::k_treasure),
    World::TileProbability(50, TileType::k_mimic),
};

const int World::s_kMaxNumberOfEnemies = 10;

World::World()
	: m_ppGrid(nullptr)
	, m_width(0)
	, m_height(0)
	, m_pPlayer(nullptr)
	, m_ppEnemiesArray(nullptr)
    , m_gameOver(false)
{
	//
}

World::~World()
{
    for (int i = 0; i < m_width * m_height; ++i)
    {
        delete m_ppGrid[i];
    }
	delete[] m_ppGrid;
	m_ppGrid = nullptr;

    delete m_pPlayer;
    m_pPlayer = nullptr;
}

void World::Init(int width, int height)
{
	// if we have a grid, destroy it
	if (m_ppGrid)
	{
        for (int i = 0; i < m_width * m_height; ++i)
        {
            delete m_ppGrid[i];
        }
        delete[] m_ppGrid;
        m_ppGrid = nullptr;
    }

	// create and fill the grid with nothing
	m_ppGrid = new Tile*[width*height];
	for (int i = 0; i < width*height; ++i)
	{
		m_ppGrid[i] = nullptr;
	}

	// set the width & height
	m_width = width;
	m_height = height;
	
	//Initializing enemies array
	m_ppEnemiesArray = new Enemy*[s_kMaxNumberOfEnemies];
	for (int i = 0; i < s_kMaxNumberOfEnemies; ++i)
	{
		m_ppEnemiesArray[i] = nullptr;
	}

}

void World::CreatePlayer(int x, int y)
{
	assert(x >= 0 && x < m_width);
	assert(y >= 0 && y < m_height);
	m_pPlayer = new Player(x, y);
}



void World::GenerateWorld()
{
    // calculate the max probability
    int maxProbability = 0;
    for (int i = 0; i < (int)TileType::k_numTiles; ++i)
    {
        maxProbability += s_tileProbabilities[i].first;
    }

    // create the start and end tiles
    int lastIndex = (m_width * m_height) - 1;
    m_ppGrid[lastIndex] = new EndTile;  // special tile for ending the level; there is only one of these
    m_ppGrid[0] = new FloorTile;  // guarantee that the starting location is open

    // guarantee at least one mimic
    int randomTile = (rand() % (lastIndex - 1)) + 1;
    assert(m_ppGrid[randomTile] == nullptr);  // if this fires, it means our math is wrong
    m_ppGrid[randomTile] = new MimicTile;

	// populate the rest of the world
	for (int tileIndex = 1; tileIndex < (m_width * m_height) - 1; ++tileIndex)
	{
        // skip this tile if we've already set it
        if (m_ppGrid[tileIndex] != nullptr)
            continue;

		int roll = rand() % maxProbability;
        int probabilityIndex = 0;
        while (true)
        {
            roll -= s_tileProbabilities[probabilityIndex].first;
            if (roll < 0)
                break;
            ++probabilityIndex;
        }

        assert(probabilityIndex >= 0 && probabilityIndex < (int)TileType::k_numTiles);

		switch (s_tileProbabilities[probabilityIndex].second)
		{
            case TileType::k_floor:
                m_ppGrid[tileIndex] = new FloorTile;
				RollForEnemy(tileIndex);	//If you created a floor tile. Roll some for enemy
				break;

            case TileType::k_bomb:
                m_ppGrid[tileIndex] = new BombTile;
				break;

            case TileType::k_treasure:
                m_ppGrid[tileIndex] = new TreasureTile;
				break;

            case TileType::k_mimic:
                m_ppGrid[tileIndex] = new MimicTile;
                break;

			default:
                cout << "ERROR: Invalid type type.\n";
                break;
		}
	}
}

void World::RollForEnemy(int position)
{
	//roll to create enemy
	int roll = (rand() % 10);
	bool shouldSpawnEnemy = (roll == 1) ? true : false;

	if (shouldSpawnEnemy)
	{
		//roll to what type of enemy to create
		int enemyType = (rand() % 2);	//Roll for bool value
		bool isDefencive = (enemyType == 1) ? true : false;
		//Converting back to 2D coordinates
		int x = Utility::GetInstance()->ConvertX(position);
		int y = Utility::GetInstance()->ConvertY(position);

		if (x > 1 && x < (m_width - 2) && y > 1 && (m_height - 2))
		{
			Enemy* newEnemy = new Enemy(x, y, isDefencive, m_pPlayer, 5, 1);

			for (int i = 0; i < s_kMaxNumberOfEnemies; ++i)
			{
				if (m_ppEnemiesArray[i] == nullptr)
				{
					m_ppEnemiesArray[i] = newEnemy;
					break;
				}
			}
		}
	}
}
void World::Draw()
{
    system("cls");

    m_pPlayer->DrawUi();

	for (int y = 0; y < m_height; ++y)
	{
		for (int x = 0; x < m_width; ++x)
		{
			Enemy* pEnemy = nullptr;

			for (int i = 0; i < s_kMaxNumberOfEnemies; ++i)
			{
				if (m_ppEnemiesArray[i] && m_ppEnemiesArray[i]->GetX() == x && m_ppEnemiesArray[i]->GetY() == y)
				{
					pEnemy = m_ppEnemiesArray[i];
				}
			}
			//Draw player
			if (m_pPlayer && m_pPlayer->GetX() == x && m_pPlayer->GetY() == y)
			{
				m_pPlayer->Draw();
				cout << ' ';
			}
			//Draw enemy
			else if (pEnemy && pEnemy->GetX() == x && pEnemy->GetY() == y)
			{
				pEnemy->Draw();
				cout << ' ';
			}
			//Draw tile
			else
			{
				int index = Utility::GetInstance()->ConvertToLinear(x, y);
				m_ppGrid[index]->Draw();
				cout << ' ';
			}
		}
		cout << endl;
	}
}

void World::Update()
{
    if (!m_pPlayer->Update())
    {
        m_pPlayer->Kill();  // ending the game prematurely does not result in a win
        EndGame();
        return;
    }
	//Enemies behaviuors
	for (int i = 0; i < s_kMaxNumberOfEnemies; ++i)
	{
		if (m_ppEnemiesArray[i] != nullptr)
		{
			if (m_ppEnemiesArray[i]->Update())
			{
				//Check what tile enemy is on
				int index = Utility::GetInstance()->ConvertToLinear(m_ppEnemiesArray[i]->GetX(), m_ppEnemiesArray[i]->GetY());
				//[CRASH] ???

				m_ppGrid[index]->OnEnter(m_ppEnemiesArray[i]);
				//Check if enemy is still on the field. Of not - Kill him
				if (m_ppEnemiesArray[i]->GetX() < 0 || m_ppEnemiesArray[i]->GetY() < 0 || m_ppEnemiesArray[i]->GetX() >= m_width || m_ppEnemiesArray[i]->GetY() >= m_height)
				{
					m_ppEnemiesArray[i]->Kill();
				}
			}
			else
			{
				m_ppEnemiesArray[i]->Kill();
			}
		}
	}

    int x = m_pPlayer->GetX();
    int y = m_pPlayer->GetY();

    // this is a death arena, so check to see if we went over the edge of the world
    if (x < 0 || y < 0 || x >= m_width || y >= m_height)
    {
        m_pPlayer->Kill();
        EndGame();
        return;
    }

    // process the tile the player is on
    int index = Utility::GetInstance()->ConvertToLinear(x, y);
    m_ppGrid[index]->OnEnter(m_pPlayer);
}

void World::EndGame()
{
	if (!m_pPlayer->IsDead())
	{
		cout << "\n\nYou won!\n\n";
		cout << "Your final score is: " << m_pPlayer->CalculateScore() << "\n";
	}
	else
	{
		cout << "\n\nYou have died.\n\n";
	}

	m_gameOver = true;
}

Tile* World::GetTile(int x, int y)
{
	//Linear grid accsess
	int index = Utility::GetInstance()->ConvertToLinear(x, y);
	return m_ppGrid[index];
}
