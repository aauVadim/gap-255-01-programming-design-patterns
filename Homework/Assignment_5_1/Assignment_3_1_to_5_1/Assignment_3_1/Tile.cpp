//	Tile.cpp
//	Name: Vadim Osipov
//	Date: 10/10

#include "Tile.h"

Tile::Tile()
	: m_pEffect(nullptr)
{

}

void Tile::OnEnter(Actor* pActor) 
{ 
	if (m_pEffect) 
		m_pEffect->ApplyEffect(pActor); 
}