// TreasureTile.h
#ifndef __TREASURETILE_H__
#define __TREASURETILE_H__

#include "Tile.h"
#include <utility>

class TreasureTile : public Tile
{
	//[vo] TO_REZ: Decided to keep this here - Due to different chests can have different gold amonts to give.
	//I did it differently with damage effect - just to show you that I know how else it can be done. 
    typedef std::pair<int, int> TreasureRange;
    static const TreasureRange s_treasureRange;

public:
    TreasureTile();
    virtual void Draw() override;
};

#endif
