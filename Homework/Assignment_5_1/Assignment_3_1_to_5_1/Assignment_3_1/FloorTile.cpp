// FloorTile.cpp
#include "FloorTile.h"
#include <iostream>

using std::cout;

FloorTile::FloorTile()
{
	m_tileType = TileType::k_walkable;
}

void FloorTile::Draw()
{
	cout << ".";
}
