// Event.h
#ifndef EVENT_H
#define EVENT_H

#include "EventTypes.h"

class Event
{
    EventId m_guid;

public:
    Event(const EventId& guid) : m_guid(guid) { }
	virtual ~Event() { };

    const EventId& GetEventId() const { return m_guid; }
};

#endif
