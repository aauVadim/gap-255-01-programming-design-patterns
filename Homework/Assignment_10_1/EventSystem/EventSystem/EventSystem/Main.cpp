// Main.cpp
#include "Event.h"
#include "EventSystem.h"
#include "EventListener.h"
#include "Macros.h"

#include "windows.h"

#include <iostream>
#include <conio.h>
#include <string>

//---------------------------------------------------------------------------------------------------------------------
// Assignment 10.1
// 
// 1) Turn this into a deferred event system. Events go to a queue and are processed later.
// 2) Use a double-buffered queuing system.  In other words, there are two queues, one of 
//    which we execute, the other is pushed to.  Swapping queues should be O(1) time.
// 3) Try to make as few code changes as possible.
//---------------------------------------------------------------------------------------------------------------------


//---------------------------------------------------------------------------------------------------------------------
//	To Rez: 
//	I've done two std::queue's and two pointers(pActiveQueue, pInactiveQueue).
//	Added a function: AddEventForExecution(Event* pEvent) which adds event to Inactive queue.
//	Added a function TriggerAllEvents() that triggets all of the events in the queue by calling 
//	TriggerEvent(const Event* pEvent) for every event in the queue,
//	Main looks ugly - I know it. And I'm sorry for it. 
//	There are tree while loops that take 'q' to exit and any other key to loop again. Made it that way to demonstrate it in slow paste looping. I hope I make sense here.
//	Events are added right before every single while loop. And executed/deleted after loop run.
//---------------------------------------------------------------------------------------------------------------------


const EventId k_deathEvent = 0xbbb0c3d3;
const EventId k_questEvent = 0x55f58298;

EventSystem* g_pEventSystem = nullptr;

class QuestEvent : public Event
{
	std::string m_questName;

public:
	QuestEvent(const std::string& questName)
		: Event(k_questEvent)
		, m_questName(questName)
	{
		//
	}

	const std::string& GetQuestName() const { return m_questName; }
};


class QuestSystem : public EventListener
{
public:
	virtual void OnEvent(const Event* pEvent) override
	{
		if (pEvent->GetEventId() == k_questEvent)
		{
			const QuestEvent* pQuestEvent = static_cast<const QuestEvent*>(pEvent);
			std::cout << "Quest system received quest event: " << pQuestEvent->GetQuestName() << "\n";
		}
		else if (pEvent->GetEventId() == k_deathEvent)
		{
			std::cout << "Quest system received death event\n";
		}
	}
};


class PartySystem : public EventListener
{
public:
	virtual void OnEvent(const Event* pEvent) override
	{
		if (pEvent->GetEventId() == k_deathEvent)
		{
			std::cout << "Party system received death event\n";
		}
	}
};


void main()
{
	bool isRunning = true;

	g_pEventSystem = new EventSystem;

	QuestSystem* pQuestSystem = new QuestSystem;
	PartySystem* pPartySystem = new PartySystem;

	// attach all listeners
	g_pEventSystem->AttachListener(k_questEvent, pQuestSystem);
	g_pEventSystem->AttachListener(k_deathEvent, pQuestSystem);
	g_pEventSystem->AttachListener(k_deathEvent, pPartySystem);

	// send out the events
	g_pEventSystem->AddEventForExecution(new QuestEvent("Kegger"));
	g_pEventSystem->AddEventForExecution(new Event(k_deathEvent));

	std::cout << "Press any key to execute Events, or Q to exit loop\n";
	//First loop
	while (_getch() != 'q')
	{
		std::cout << "In First loop, going to execute events" << std::endl;
		g_pEventSystem->TriggerAllEvents();
	}
	
	// Second send out the events
	g_pEventSystem->AddEventForExecution(new QuestEvent("Kegger"));
	g_pEventSystem->AddEventForExecution(new Event(k_deathEvent));

	std::cout << "\nExited loop - Press any key to execute Events is Second Loop, or Q to exit loop\n";
	while (_getch() != 'q')
	{
		std::cout << "In Second loop, going to execute events" << std::endl;
		g_pEventSystem->TriggerAllEvents();
	}
	
	// send out the events
	g_pEventSystem->AddEventForExecution(new QuestEvent("Kegger"));
	g_pEventSystem->AddEventForExecution(new Event(k_deathEvent));

	std::cout << "\nExited loop - Press any key to execute Events is Third Loop, or Q to exit loop\n";
	while (_getch() != 'q')
	{
		std::cout << "In Third loop, going to execute events" << std::endl;
		g_pEventSystem->TriggerAllEvents();
	}

	// remove all listeners
	g_pEventSystem->RemoveListener(k_questEvent, pQuestSystem);
	g_pEventSystem->RemoveListener(k_deathEvent, pQuestSystem);
	g_pEventSystem->RemoveListener(k_deathEvent, pPartySystem);

	SAFE_DELETE(pPartySystem);
	SAFE_DELETE(pQuestSystem);
	SAFE_DELETE(g_pEventSystem);

	_getch();
}

