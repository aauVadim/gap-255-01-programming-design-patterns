// EventSystem.cpp
#include "EventSystem.h"
#include "Event.h"
#include "EventListener.h"
#include "Macros.h"

EventSystem::EventSystem()
	: m_isQueueOne(true)
	, m_pActiveQueue(nullptr)
	, m_pInactiveQueue(nullptr)
{

}

void EventSystem::AttachListener(const EventId& eventId, EventListener* pListener)
{
	m_listeners[eventId].push_back(pListener);
}

void EventSystem::RemoveListener(const EventId& eventId, EventListener* pListener)
{
	// find the list we care about
	auto findIt = m_listeners.find(eventId);
	if (findIt == m_listeners.end())
		return;

	// loop through the list and find the listener to remove
	EventListenerList& eventListenerList = findIt->second;
	eventListenerList.remove(pListener);
}

// event management
void EventSystem::TriggerAllEvents()
{
	// I love this line <3 
	// Swithicn Queue Bollean and Queue Poiner
	m_isQueueOne = !m_isQueueOne;
	m_pActiveQueue = (m_isQueueOne) ? (&m_queueOne) : (&m_queueTwo);

	//Safety check
	if (!m_pActiveQueue->empty())
	{
		//Maybe lame way Iterating trough the queue
		for (int i = 0; i < m_pActiveQueue->size() + 1; ++i)
		{
			//Getting first in the line element
			//Event* pEvent = m_pActiveQueue->front();
			TriggerEvent(m_pActiveQueue->front());
			//Poping it
			m_pActiveQueue->pop();
			//Calling it
		}
	}
}

void EventSystem::TriggerEvent(const Event* pEvent)
{
	// find the list we care about
	auto findIt = m_listeners.find(pEvent->GetEventId());
	if (findIt == m_listeners.end())
		return;

	// send the event to every listener
	for (auto* pEventListener : findIt->second)
	{
		pEventListener->OnEvent(pEvent);
	}
	SAFE_DELETE(pEvent);
}

void EventSystem::AddEventForExecution(Event* pEvent)
{
	//Pushing things to Inactive Queue
	m_pInactiveQueue = (m_isQueueOne) ? (&m_queueTwo) : (&m_queueOne);
	m_pInactiveQueue->push(pEvent);
}




