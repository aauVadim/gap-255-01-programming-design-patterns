// EventSystem.h
#ifndef EVENTSYSTEM_H
#define EVENTSYSTEM_H

#include "EventTypes.h"
#include <unordered_map>
#include <queue>

class Event;
class EventListener;

class EventSystem
{
    typedef std::list<EventListener*> EventListenerList;
    std::unordered_map<EventId, EventListenerList> m_listeners;


	bool m_isQueueOne;
	//Queues
	std::queue<Event*> m_queueOne;
	std::queue<Event*> m_queueTwo;
	//Queue that gets executed 
	std::queue<Event*>* m_pActiveQueue;
	std::queue<Event*>* m_pInactiveQueue; 


public:
	EventSystem();

    // listener management
    void AttachListener(const EventId& eventId, EventListener* pListener);
    void RemoveListener(const EventId& eventId, EventListener* pListener);

    // event management
	void TriggerAllEvents();
	void AddEventForExecution(Event* pEvent);

private:
	void TriggerEvent(const Event* pEvent);

};

#endif

