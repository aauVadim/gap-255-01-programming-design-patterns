//	Name: Osipov Vadim
//	Component.h

#ifndef __COMPONENT_H__
#define __COMPONENT_H__

#include "../../GameObject/GameObject.h"
class ObjectFactory;

class Component
{
	GameObject* m_pMyBase;
public:
	Component(GameObject* pOwner) : m_pMyBase(pOwner) { }
	virtual ~Component() { }
	virtual void UpdateRotation(int posX, int posY) { }
	virtual void UpdatePosition(int plusPosX = 0, int plusPosY = 0) { } 
	virtual void OnClick(int posX = 0, int posY = 0) { }
	virtual void TickUpdate() { }

protected:
	GameObject* GetOwner() const { return m_pMyBase; }
};

#endif 