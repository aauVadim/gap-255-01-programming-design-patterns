//	Name: Vadim Osipov
//	Enemy.cpp

#include "Enemy.h"
#include "../../../Utility/Macros.h"
#include "../../../Constants/ConstantVariables.h"

Enemy::Enemy(GameObject* pOwner, GameObject* pTarget)
	: Component(pOwner)
	, m_pTarget(pTarget)
{

}

void Enemy::TickUpdate()
{
	UpdateRotation(m_pTarget->GetObjPosX(), m_pTarget->GetObjPosY());
	UpdatePosition(m_pTarget->GetObjPosX(), m_pTarget->GetObjPosY());
}
void Enemy::UpdateRotation(int posX, int posY)
{
	GetOwner()->SetObjectRotation(GetRotationAngle(posX, posY));
}
void Enemy::UpdatePosition(int posX, int posY)
{
	int newPosX = (posX - GetOwner()->GetObjPosX()) / 20;
	int newPosY = (posY - GetOwner()->GetObjPosY()) / 20;
	GetOwner()->UpdatePosition(newPosX, newPosY);
}

double Enemy::GetRotationAngle(int targetPosX, int targetPosY)
{
	double xDiff = GetOwner()->GetObjPosX() - targetPosX;
	double yDiff = GetOwner()->GetObjPosY() - targetPosY;

	return atan2(yDiff, xDiff) * (180 / PI);
}