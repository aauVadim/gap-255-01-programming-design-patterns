//	Name: Osipov vadim 
//	Enemy.h

#ifndef __ENEMY_H__
#define __ENEMY_H__

#include "../Component.h"

class Enemy : public Component
{
	GameObject* m_pTarget;

public: 
	Enemy(GameObject* pOwner, GameObject* pTarget);
	~Enemy() { }

	virtual void TickUpdate();
	virtual void UpdateRotation(int posX, int posY) override;
	virtual void UpdatePosition(int posX, int posY) override;
private:
	double GetRotationAngle(int mousePosX, int mousePosY);
};

#endif

