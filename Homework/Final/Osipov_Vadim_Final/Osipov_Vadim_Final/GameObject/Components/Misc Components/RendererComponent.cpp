
#include "SDL.h"
#include "SDL_image.h"
#include "RendererComponent.h"

#include <assert.h>

#include "../../Utility/Macros.h"
#include "../../../Managers/Renderer.h"


RendererComponent::RendererComponent(SDL_Texture* texture, GameObject* pOwner, Renderer* pRenderer)
	: Component(pOwner)
	, m_pTexture(texture)
	, m_pRect(nullptr)
	, m_pRenderer(pRenderer)
{
	m_pRect = CreateRect();	//Creating a rect for game object
	m_pRenderer->RegisterComponent(this);	//Regestering RendererComp with Renderer 
}

RendererComponent::~RendererComponent()
{
	m_pRenderer->RemoveComponent(this);	//Unregestering/Deleting the component from Renderer
	SDL_DestroyTexture(m_pTexture);		//SDL_Testure removal 
	m_pTexture = nullptr;
	_SAFE_DELETE_(m_pRect);
}

SDL_Rect* RendererComponent::CreateRect()
{
	m_pRect = new SDL_Rect;
	//Referensing where to render from, according to our owner game object
	m_pRect->x = GetOwner()->GetObjPosX();
	m_pRect->y = GetOwner()->GetObjPosY();
	
	SDL_QueryTexture(m_pTexture, NULL, NULL, &m_pRect->h, &m_pRect->w);

	//Cheating
	GetOwner()->SetObjectRect(m_pRect);
	
	return m_pRect;
}

const SDL_Point RendererComponent::GetPivotPoint() const
{
	SDL_Point point; 
	point.x = GetOwner()->GetObjPosX();
	point.y = GetOwner()->GetObjPosY();
	return point; 
}

SDL_Rect* RendererComponent::GetRect() const
{
	//Adjusting it to the middle 
	m_pRect->x = GetOwner()->GetObjPosX() - (m_pRect->w / 2);
	m_pRect->y = GetOwner()->GetObjPosY() - (m_pRect->h / 2);

	return m_pRect;
}

