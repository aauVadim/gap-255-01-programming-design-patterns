//	Name: Osipov Vadim
//	RendererComponent.h

#ifndef __RENDERERCOMPONENT_H__
#define __RENDERERCOMPONENT_H__

#include "../Component.h"
#include "SDL.h"

class Renderer;

class RendererComponent : public Component
{
	
	SDL_Texture* m_pTexture;
	SDL_Rect* m_pRect;
	Renderer* m_pRenderer;

public:
	RendererComponent(SDL_Texture* texture, GameObject* pOwner, Renderer* pRenderer);
	~RendererComponent();
	
	//Getters
	SDL_Texture* GetTexture() const { return m_pTexture; }
	SDL_Rect* GetRect() const;
	const SDL_Point GetPivotPoint() const;
	const double GetAngleRotation() const { return GetOwner()->GetObjectRotation(); }

private:
	SDL_Rect* CreateRect();
};

#endif //__RENDERERCOMPONENT_H__