//	Name: Osipov vadim 
//	Projectile.cpp

#include "Projectile.h"

Projectile::Projectile(int targetPosX, int targetPosY, GameObject* pOwner)
	: Component(pOwner)
	, m_magnitudeX(CalculateMagnitudeX(targetPosX) / 5)	//Calculating magnitude X
	, m_magnitudeY(CalculateMagnitudeY(targetPosY) / 5)	//Calculating magnitude Y
{

}


void Projectile::TickUpdate()
{
	//Maybe not the right way to do it 
	GetOwner()->UpdatePosition(m_magnitudeX, m_magnitudeY);
}

int Projectile::CalculateMagnitudeX(int targetPosX)
{
	return targetPosX - GetOwner()->GetObjPosX();
}

int Projectile::CalculateMagnitudeY(int targetPosY)
{
	return targetPosY - GetOwner()->GetObjPosY();
}