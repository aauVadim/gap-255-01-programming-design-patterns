//	Name: Osipov Vadim 
//	Projectile.h

#ifndef __PROJECTILE_H__
#define __PROJECTILE_H__

#include "../../Component.h"

class Projectile : public Component
{
	//int m_targetPosX, m_targetPosY;	//Click position
	int m_magnitudeX, m_magnitudeY; //magnitude 

public:
	Projectile(int targetPosX, int targetPosY, GameObject* pOwner);
	virtual void TickUpdate() override;
private:
	void MoveToTarget();
	int CalculateMagnitudeX(int targetPosX);
	int CalculateMagnitudeY(int targetPosY);
};

#endif