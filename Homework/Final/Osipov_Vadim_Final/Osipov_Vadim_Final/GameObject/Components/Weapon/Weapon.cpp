#include "Weapon.h"

#include "../../Factory/ObjectFactory.h"

Weapon::Weapon(int ammo, int rate, GameObject* pOwner)
	: Component(pOwner)
	, m_ammo(ammo)
	, m_shootRate(rate)
{

}

void Weapon::OnClick(int posX, int posY)
{
	//Getting the center of the rect
	int shootPosX = GetOwner()->GetObjectRect()->x + (GetOwner()->GetObjectRect()->w / 2);
	int shootPosY = GetOwner()->GetObjectRect()->y + (GetOwner()->GetObjectRect()->h / 2);

	ObjectFactory::CreateProjectile(5, shootPosX, shootPosY, posX, posY);
}
