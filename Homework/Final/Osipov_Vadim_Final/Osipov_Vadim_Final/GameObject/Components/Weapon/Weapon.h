//	Name: Osipov Vadim
//	Weapon.h

#ifndef __WEAPON_H__
#define __WEAPON_H__

#include "../Component.h"

class Weapon : public Component
{
	int m_ammo;
	int m_shootRate;
	
public: 
	Weapon(int ammo, int rate, GameObject* pOwner);

	virtual void OnClick(int posX = 0, int posY = 0) override;
};

#endif