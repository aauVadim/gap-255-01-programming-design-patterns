//	Name: Osipov Vadim
//	Player.cpp

#include "Player.h"
#include "../Weapon/Weapon.h"
#include "../../GameObject.h"
#include "../../Utility/Macros.h"
#include "../../../Constants/ConstantVariables.h"
#include "SDL.h"

#include <math.h>
#include <assert.h>


Player::Player(int health, float speed, GameObject* pOwner)
	: Component(pOwner)
	, m_speed(speed) 
	, m_health(health)
{

}

void Player::UpdateRotation(int posX, int posY)
{
	double angle = GetRotationAngle(posX, posY);
	GetOwner()->SetObjectRotation(angle);
}

double Player::GetRotationAngle(int mousePosX, int mousePosY)
{
	double xDiff = GetOwner()->GetObjPosX() - mousePosX;
	double yDiff = GetOwner()->GetObjPosY() - mousePosY;

	return atan2(yDiff, xDiff) * (180 / PI);
}
