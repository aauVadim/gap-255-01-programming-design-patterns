//	Name: Osipov Vadim
//	Player.h

#ifndef __PLAYER_H__
#define __PLAYER_H__

#include "../Component.h"

#include <queue>

class GameObject;

//---------------------------------------------------------------------------------------------------------------------
//	Basically no variables of this class are used. 
//	The only thing that is really used from this class is Update rotation
//---------------------------------------------------------------------------------------------------------------------
class Player : public Component
{
	int m_health;	//not used
	float m_speed;  //Not used
	
public: 
	Player(int health, float speed, GameObject* pOwner);
	~Player() { }

	//Component Virtuals
	virtual void UpdateRotation(int posX, int posY) override;
	//Component Virtuals - end

private: 
	double GetRotationAngle(int mousePosX, int mousePosY);
};

#endif