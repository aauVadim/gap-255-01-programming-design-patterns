//	Name: Osipov Vadim 
//	GameObject.h

#ifndef __GAMEOBJECT_H__
#define __GAMEOBJECT_H__

#include <SDL.h>

#include <vector>
#include "../Constants/Enums.h"

class Component;
class Behavior;

class GameObject
{
	//Position
	int m_posX, m_posY;
	//Rotation
	double m_rotation;
	std::vector<Component*> m_components;
	SDL_Rect* m_pRect;
	GameObjectType m_ObjectType;

public:
	GameObject(int posX, int posY, double rotation);
	~GameObject();
	//Adding components
	void AddComponent(Component* pComp) { m_components.push_back(pComp); }

	double GetObjectRotation() const { return m_rotation; }
	void SetObjectRotation(double newRotation) { m_rotation = newRotation; }

	int GetObjPosX() const { return m_posX; }
	int GetObjPosY() const { return m_posY; }

	void UpdatePosition(int plusX, int plusY) { m_posX += plusX; m_posY += plusY; }

	void TickUpdate();

	//Input
	void OnClick(int posX, int posY);
	void OnMousePositionClange(int posX, int posY);
	void OnKeyDown();

	SDL_Rect* GetObjectRect() const { return m_pRect; }
	void SetObjectRect(SDL_Rect* pRect) { m_pRect = pRect; }

	GameObjectType GetObjectType() const { return m_ObjectType; }
	void SetObjectType(GameObjectType type) { m_ObjectType = type; }
};

#endif