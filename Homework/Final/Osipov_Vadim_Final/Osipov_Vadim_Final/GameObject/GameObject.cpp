//	Name: Osipov Vadim 
//	GameObject.cpp

#include "GameObject.h"
#include "../Utility/Macros.h"

#include "Components\Component.h"


GameObject::GameObject(int posX, int posY, double rotation)
	: m_posX(posX)
	, m_posY(posY)
	, m_rotation(rotation)
	, m_pRect(nullptr)
	, m_ObjectType(GameObjectType::k_none)
{

}

GameObject::~GameObject()
{
	for (Component* pComp : m_components)
	{
		_SAFE_DELETE_(pComp);
	}
	m_components.clear();
}

void GameObject::OnClick(int posX, int posY)
{
	for (Component* pComp : m_components)
	{
		pComp->OnClick(posX, posY);
	}
}
void GameObject::OnMousePositionClange(int posX, int posY)
{
	for (Component* pComp : m_components)
	{
		pComp->UpdateRotation(posX, posY);
	}
}
void GameObject::OnKeyDown()
{
	//Do input 
}

void GameObject::TickUpdate()
{
	for (Component* pComp : m_components)
	{
		pComp->TickUpdate();
	}
}
