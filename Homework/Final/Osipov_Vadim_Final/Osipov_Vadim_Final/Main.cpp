//	Name: Osipov Vadim
//	Main.cpp

#include <vld.h>

#include<SDL.h>
#include <stdlib.h>
#include"Managers\Game.h"

#include "Constants\Enums.h"


//---------------------------------------------------------------------------------------------------------------------
// Description: 
// As I pitched - It is a Crimsonland clone. Well Atleast some parts of it. 
// I'm not sure if I chose to have right architectual structure for this game or if I went the right way with what I chose. 
// It is defeantely not one of my best creatins and I should have done some things differentely. 
//
// For right now: Player Cannot lose the game. You can only win. By killing 10 enemies game will be won.
//
// Controlls: 
//    - W,A,S,D - Move player around. Right now there is no ground texture, so its is hhard to see player moving. But when enemies spawn, it is way esier to see the movement. 
//    - Click to Shoot.
// Classes: 
//    - Game.h - The big deal! This is my main game manager that is responsible for owning Renderer, Game Objects and takes care of collision. 
//    everything else is basically helper for that.
//    - Renderer.h - Probably second class by weight. The only responsobility of it is to render textures and open and close SDL Window. 
//    - GameObject.h - This class is base for anything that is present in the game. Game Object is a basically holder and accesor of all components that can be placed on it. 
//    - Component.h - Not pure vitrual - It has default bodies for all functions. But it is a commot interface for amy component to exist. Component must inherit from this interface.
//    - Player.h - a player movement part. Unfortunantely I had no time to make Player Game object as Generic as rest of the game objects. So Player Stands out of Game::m_gameObjects array due to that. 
//    - RendererComponent.h - Another unique class that has to register with Renderer.h in order to behave well. Responsobilities: Texture, SDL_Rect. 
//    - Rest of the Classes are simple Driving components of Game Object
//---------------------------------------------------------------------------------------------------------------------

int main(int args, char* argv[])
{
	//Game 
	Game game; 
	game.Initialize();
	//Random
	srand(unsigned int(0));

	//While app is running
	while (game.GetGameState() != GameState::k_GameOver)
	{
		game.Update();
	}

	return 0;
}