//	Name: Osipov Vadim
//	Game.cpp
#include <SDL.h>
#include <SDL_rect.h>


#include "Game.h"
#include "Renderer.h"
#include "../Constants/ConstantVariables.h"

#include "../Utility/Macros.h"

#include "../Factory/ObjectFactory.h"

#include "../GameObject/Components/Player/Player.h"
#include "../GameObject/GameObject.h"


Game::Game()
	: m_window(nullptr)
	, m_renderer(nullptr)
	, m_frameCap(33.3f)
	, m_frame(0.0f)
	, m_gameState(GameState::k_MainMenu)
	, m_pPlayer(nullptr)
	, m_overAllGameTime(0)
	, m_lastSpawnTime(-1)
	, m_x(0)
	, m_y(0)
{

}

Game::~Game()
{
	//Making sure that renderer shuts down properly
	//Deleting Renderer
	//Player goes
	_SAFE_DELETE_(m_pPlayer);

	for (GameObject* pGO : m_gameObjects)
	{
		_SAFE_DELETE_(pGO);
	}
	
	m_renderer->Shutdown();
	_SAFE_DELETE_(m_renderer);

	SDL_DestroyWindow(m_window);
	SDL_Quit();
}

void Game::Initialize()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		SDL_Log("Something went wront in Game::Initialize() -> SDL_Init");
		return;
	}

	m_window = SDL_CreateWindow("Crimsonland Remade", SDL_WINDOWPOS_UNDEFINED, SDL_WINDOWPOS_UNDEFINED, SCREEN_WIDTH, SCREEN_HEIGHT, SDL_WINDOW_SHOWN);
	
	m_renderer = new Renderer();
	m_renderer->CreateRenderer(SDL_CreateRenderer(m_window, -1, SDL_RENDERER_ACCELERATED));

	//Framerate
	m_frequency = (int)SDL_GetPerformanceFrequency();
	m_lastCounter = (int)SDL_GetPerformanceCounter();

	ObjectFactory::Initialize(this);
	//Creating player
	m_pPlayer = ObjectFactory::CreatePlayer();
}
//---------------------------------------------------------------------------------------------------------------------
// Main update function. This is where sdl events are happening. 
//---------------------------------------------------------------------------------------------------------------------
void Game::Update()
{
	SDL_Event event;
	SDL_PumpEvents();

	//Capping the framerate to 60fps/33.3ms
	if (GetFrame() > m_frameCap)
	{
		while (SDL_PollEvent(&event))
		{
			if (event.type == SDL_WINDOWEVENT)
			{
				//Handling the close window event 
				if (event.window.event == SDL_WINDOWEVENT_CLOSE)
				{
					m_gameState = GameState::k_GameOver;
				}
			}
			//Reading the mouse input
			else if (event.type == SDL_MOUSEBUTTONUP)
			{
				//Click Handling
				if (event.button.button == SDL_BUTTON_LEFT)
				{
					SDL_GetMouseState(&m_x, &m_y);
					m_pPlayer->OnClick(m_x, m_y);
				}
			}
			else if (event.type == SDL_KEYDOWN)
			{
				//Movement - Keyboar input
				ProcessInput(event);
			}
		}

		//Updating players rotation and Game Objects Tick functions
		if (m_gameState != GameState::k_GameOver)
		{
			SDL_GetMouseState(&m_x, &m_y);
			m_pPlayer->OnMousePositionClange(m_x, m_y);
			UpdateGameObjects();
		}

		//Drawing things out
		m_renderer->Draw();

		//Saving overall time
		m_overAllGameTime += (int)m_frame;
		//Setting frame back to 0
		m_frame = 0.0f;

		//Clening up game objects that are placed for deletion
		EraseGameObjects();
		//Respawning enemies
		SpawnEnemies();
	}
}

//---------------------------------------------------------------------------------------------------------------------
//	Frame rate cap
//---------------------------------------------------------------------------------------------------------------------
float Game::GetFrame()
{
	m_currentCounter = (int)SDL_GetPerformanceCounter();
	float delta = (m_currentCounter - m_lastCounter) / static_cast<float>(m_frequency)* 1000;
	m_lastCounter = m_currentCounter;

	return m_frame += delta;
}

//---------------------------------------------------------------------------------------------------------------------
//	Loop trough registered objects to Update them
//---------------------------------------------------------------------------------------------------------------------
void Game::UpdateGameObjects()
{
	for (GameObject* pGO : m_gameObjects)
	{
		pGO->TickUpdate();
		CalculateCollision(pGO);
	}
}
//---------------------------------------------------------------------------------------------------------------------
//	Input Swithcer - I'm very sertain that I did not do this at the best of my ability. 
//---------------------------------------------------------------------------------------------------------------------
void Game::ProcessInput(SDL_Event& event)
{
	if (event.key.keysym.sym == SDLK_w)	//W
	{
		UpdateGameObjectsPosition(0, PLAYER_MOVEMENT_SPEED);
	}
	else if (event.key.keysym.sym == SDLK_a)	//A
	{
		UpdateGameObjectsPosition(PLAYER_MOVEMENT_SPEED, 0);
	}
	else if (event.key.keysym.sym == SDLK_s)	//S
	{
		UpdateGameObjectsPosition(0, -PLAYER_MOVEMENT_SPEED);
	}
	else if (event.key.keysym.sym == SDLK_d)	//D
	{
		UpdateGameObjectsPosition(-PLAYER_MOVEMENT_SPEED, 0);
	}
}
void Game::UpdateGameObjectsPosition(int plusPosX, int plusPosY)
{
	for (GameObject* pGO : m_gameObjects)
	{
		pGO->UpdatePosition(plusPosX, plusPosY);
	}
}

//---------------------------------------------------------------------------------------------------------------------
//	Collision - Big O(n^2). 
//	I was looking for better ways to do that. But looks like I'll stick with this one
//---------------------------------------------------------------------------------------------------------------------
void Game::CalculateCollision(GameObject* pGOChecking)
{
	/*Collision
	   a     b
		+---+
		|   |
		+---+
	   c     d
	*/

	if (pGOChecking->GetObjectRect() != nullptr)
	{
		SDL_Point pointA;
		SDL_Point pointB;
		SDL_Point pointC;
		SDL_Point pointD;

		//A 
		pointA.x = pGOChecking->GetObjectRect()->x;
		pointA.y = pGOChecking->GetObjectRect()->y;
		//B
		pointB.x = pGOChecking->GetObjectRect()->x + pGOChecking->GetObjectRect()->w;
		pointB.y = pGOChecking->GetObjectRect()->y;
		//C 
		pointC.x = pGOChecking->GetObjectRect()->x;
		pointC.y = pGOChecking->GetObjectRect()->y + pGOChecking->GetObjectRect()->h;
		//D
		pointD.x = pGOChecking->GetObjectRect()->x + pGOChecking->GetObjectRect()->w;
		pointD.y = pGOChecking->GetObjectRect()->y + pGOChecking->GetObjectRect()->h;

		//For all Game Objects
		for (GameObject* pGO : m_gameObjects)
		{
			//Do not check yourself
			if (pGOChecking != pGO)
			{
				//Compare Bullets to Enemies and Enemies to Player
				if ((pGOChecking->GetObjectType() == GameObjectType::k_bullet && pGO->GetObjectType() == GameObjectType::k_enemy)
					|| (pGOChecking->GetObjectType() == GameObjectType::k_enemy && pGO->GetObjectType() == GameObjectType::k_player))
				{
					//If Object has Rect
					if (pGO->GetObjectRect() != nullptr)
					{
						if (SDL_PointInRect(&pointA, pGO->GetObjectRect()))
						{
							PlaceToForDeletion(pGO);
							PlaceToForDeletion(pGOChecking);
							break;
						}
						if (SDL_PointInRect(&pointB, pGO->GetObjectRect()))
						{
							PlaceToForDeletion(pGO);
							PlaceToForDeletion(pGOChecking);
							break;
						}
						if (SDL_PointInRect(&pointC, pGO->GetObjectRect()))
						{
							PlaceToForDeletion(pGO);
							PlaceToForDeletion(pGOChecking);
							break;
						}
						if (SDL_PointInRect(&pointD, pGO->GetObjectRect()))
						{
							PlaceToForDeletion(pGO);
							PlaceToForDeletion(pGOChecking);
							break;
						}
					}
				}
			}
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
//	Erasing the objects that are placed for Deletion
//---------------------------------------------------------------------------------------------------------------------
void Game::EraseGameObjects()
{
	if (m_objectsToDelete.size() > 0 && !m_objectsToDelete.empty())
	{
		auto it = m_objectsToDelete.begin();
		while (it != m_objectsToDelete.end())
		{
			auto tempDeleteIt = it; 
			++it;
			
			GameObject* pTempObject = (*tempDeleteIt);
			m_objectsToDelete.erase(tempDeleteIt);

			if (!m_gameObjects.empty())
			{
				auto it = m_gameObjects.begin();
				while (it != m_gameObjects.end() && *it != pTempObject)
				{
					++it;
				}

				if (it != m_gameObjects.end())
				{
					m_gameObjects.erase(it);
					_SAFE_DELETE_(pTempObject);
				}
			}
		}
	}
}

//---------------------------------------------------------------------------------------------------------------------
//	The least function that I'm proud of - but this is a part of gameplay. 
//	Spawns Enemy every tho seconds wn what not random location 
//---------------------------------------------------------------------------------------------------------------------
void Game::SpawnEnemies()
{
	int currentSpawnTime = (m_overAllGameTime / 1000);
	if ((currentSpawnTime % 2) == 0 && currentSpawnTime != 0)
	{
		if (m_lastSpawnTime != currentSpawnTime)
		{
			m_lastSpawnTime = currentSpawnTime;
		
			if ((rand() % 2))
			{
				GameObject* pNewEnemy = ObjectFactory::CreateEnemy(ChooseRandom(SCREEN_HEIGHT, 1000), ChooseRandom(SCREEN_WIDTH, 1000));
				m_gameObjects.push_back(pNewEnemy);
				
			}
			else
			{
				GameObject* pNewEnemy = ObjectFactory::CreateEnemy(ChooseRandom(-1000, SCREEN_WIDTH), ChooseRandom(-1000, SCREEN_HEIGHT));
				m_gameObjects.push_back(pNewEnemy);
			}

			m_enemiesSpawnedInTotal += 1;

			if (m_enemiesSpawnedInTotal == 10)
				m_gameState = GameState::k_GameOver;
		}
	}
}
//---------------------------------------------------------------------------------------------------------------------
//	Random in Range
//---------------------------------------------------------------------------------------------------------------------
int Game::ChooseRandom(int min, int max)
{
	int limit = max - min; 
	return (rand() % max) + min;
}