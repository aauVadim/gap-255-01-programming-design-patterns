//	Name: Osipov Vadim
//	Renderer.cpp

#include"Renderer.h"
#include "../GameObject/Components/Misc Components/RendererComponent.h"
#include "../Utility/Macros.h"
#include "SDL.h"
#include <algorithm>

Renderer::~Renderer()
{
	SDL_DestroyRenderer(m_pRenderer);
	//m_renderables.clear();
}

void Renderer::Shutdown()
{

}

void Renderer::Draw()
{
	SDL_SetRenderDrawColor(m_pRenderer, 0, 0, 0, 255);
	SDL_RenderClear(m_pRenderer);
	//Draw things out here

	for (RendererComponent* pComp : m_renderables)
	{
		//Rendering and Rotationg every component according to center pivot
		SDL_RenderCopyEx(m_pRenderer, pComp->GetTexture(), NULL, pComp->GetRect(), pComp->GetAngleRotation(), NULL, SDL_FLIP_NONE);
	}

	SDL_RenderPresent(m_pRenderer);
}

RendererComponent* Renderer::RegisterComponent(RendererComponent* pComponent)
{
	m_renderables.push_back(pComponent);
	return pComponent;
}
void Renderer::RemoveComponent(RendererComponent* pComponent)
{
	if (!m_renderables.empty())
	{
		auto it = m_renderables.begin();
		while (it != m_renderables.end() && *it != pComponent)
		{
			++it;
		}

		if (it != m_renderables.end())
			m_renderables.erase(it);
	}
}