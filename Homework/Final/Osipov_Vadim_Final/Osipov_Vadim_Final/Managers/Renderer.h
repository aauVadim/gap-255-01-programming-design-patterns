//	Name: Osipov Vadim
//	Renderer.h

#ifndef __RENDERER_H__
#define __RENDERER_H__

#include<SDL.h>

#include <list>

class RendererComponent;

class Renderer
{
	SDL_Renderer* m_pRenderer;
	std::list<RendererComponent*> m_renderables;
	
public: 
	~Renderer();

	void CreateRenderer(SDL_Renderer* renderer) { m_pRenderer = renderer; };
	RendererComponent* RegisterComponent(RendererComponent* pComponent);
	void RemoveComponent(RendererComponent* pComponent);

	SDL_Renderer* GetRenderer() const { return m_pRenderer; }

	void Draw();
	void Shutdown();
};

#endif