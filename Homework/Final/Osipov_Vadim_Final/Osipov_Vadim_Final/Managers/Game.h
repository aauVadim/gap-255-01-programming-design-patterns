//	Name: Vadim Osipov
//	Game.h

#ifndef __GAME_H__
#define __GAME_H__

#include "../Constants/Enums.h"
#include <list>

class Renderer;
class GameObject;

class Game
{
	SDL_Window* m_window;
	Renderer* m_renderer;

	//Enum Switcher
	GameState m_gameState;
	int m_x, m_y;

	//Framerate
	Uint32 m_currentCounter;
	Uint32 m_lastCounter;
	float m_frame; 
	Uint32 m_frequency;
	float m_frameCap;

	//Enemies Spawn 
	int m_overAllGameTime;
	int m_lastSpawnTime;
	int m_enemiesSpawnedInTotal; 

	//Player reference
	GameObject* m_pPlayer;
	//Game objects that needs to be Updated every frame
	std::list<GameObject*> m_gameObjects; 
	std::list<GameObject*> m_objectsToDelete;

public:
	Game();
	~Game();

	void Initialize();

	//Getter fo rgame state
	GameState GetGameState() const { return m_gameState; }
	
	//-----------------------------------------------------------------------------------------------------------------
	//	Main game update
	//-----------------------------------------------------------------------------------------------------------------
	void Update();

	void AddGameObjectForUpdates(GameObject* pGO) { m_gameObjects.push_back(pGO); }
	Renderer* GetRenderer() const { return m_renderer; }
	//Just a reference to a player
	GameObject* GetPlayer() const { return m_pPlayer; }

private:
	float GetFrame();
	void UpdateGameObjects(); 
	void ProcessInput(SDL_Event& event);
	void UpdateGameObjectsPosition(int plusPosX, int plusPosY);
	void CalculateCollision(GameObject* pGOChecking);
	//Erasing objects
	void PlaceToForDeletion(GameObject* pGO) { m_objectsToDelete.push_back(pGO); };
	void EraseGameObjects();
	//Spawning Enemies
	void SpawnEnemies();
	int ChooseRandom(int min, int max);
};

#endif