//	Name: Osipov Vadim
//	Textures.h 

#ifndef __TEXTURES_H__
#define __TEXTURES_H__

const char* g_kPlayerTexture	= "Images/Player/Player.png";
const char* g_kEnemyTexture		= "Images/Player/Enemy.png";
const char* g_kHandgunBulletTexture = "Images/Weapons/HandgunBullet.png";

#endif //__TEXTURES_H__