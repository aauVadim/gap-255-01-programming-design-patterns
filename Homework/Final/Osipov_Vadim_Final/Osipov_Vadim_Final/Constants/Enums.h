//	Name: Vadim Osipov
//	Enums.h

#ifndef __ENUMS_H__
#define __ENUMS_H__

enum class GameState
{
	k_MainMenu, 
	k_InGame, 
	k_PostGame,
	k_GameOver
};

enum class GameObjectType
{
	k_none, 
	k_player, 
	k_bullet, 
	k_enemy
};

#endif