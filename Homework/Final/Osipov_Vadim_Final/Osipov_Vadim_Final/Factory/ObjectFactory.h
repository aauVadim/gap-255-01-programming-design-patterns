//	Name: Osipov Vadim
//	ObjectFactory.h

#ifndef __OBJECTFACTORY_H__
#define __OBJECTFACTORY_H__

#include "SDL.h"

class GameObject;
class Renderer;
class Game;

class ObjectFactory
{
	static Game* m_pGame; 
public:
	
	static void Initialize(Game* pGame) { ObjectFactory::m_pGame = pGame; }
	static GameObject* CreatePlayer();
	static SDL_Texture* CreateGroundTexture(); 
	static GameObject* CreateProjectile(int damage, int startPosX, int startPosY, int targetPosX, int targetPosY);
	static GameObject* CreateEnemy(int posX, int posY);

private:
	static SDL_Texture* LoadTexture(const char* textureLocation);
};

#endif