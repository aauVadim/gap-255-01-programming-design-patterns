//	Name: Osipov Vadim
//	ObjectFactory.cpp

#include "SDL.h"
#include "SDL_image.h"

#include "../Managers/Game.h"

#include "ObjectFactory.h"
#include "../Constants/Textures.h"
#include "../Constants/ConstantVariables.h"

#include "../GameObject/Components/Player/Player.h"
#include "../GameObject/Components/Enemy/Enemy.h"

#include "../GameObject/Components/Misc Components/RendererComponent.h"
#include "../GameObject/GameObject.h"

#include "../GameObject/Components/Weapon/Weapon.h"
#include "../GameObject/Components/Weapon/Projectile/Projectile.h"

#include "../Constants/Enums.h"

#include "../Managers/Renderer.h"
#include "../Managers/Game.h"

Game* ObjectFactory::m_pGame = nullptr;

//---------------------------------------------------------------------------------------------------------------------
//	Player Creation 
//---------------------------------------------------------------------------------------------------------------------
GameObject* ObjectFactory::CreatePlayer()
{
	//Creating blank Game Object 
	GameObject* pNewGO = new GameObject((SCREEN_WIDTH / 2), (SCREEN_HEIGHT / 2), 0.0f);
	//Creating Player Component
	Player* pNewPlayer = new Player(100, 0.5f, pNewGO);
	//Creating Rendere Component with Plater Teture
	RendererComponent* pRendererComopnent = new RendererComponent(LoadTexture(g_kPlayerTexture), pNewGO, m_pGame->GetRenderer());

	Weapon* pNewHandgun = new Weapon(10, 1, pNewGO);
	//Adding components to Player Game Object
	pNewGO->AddComponent(pRendererComopnent);	//Render component
	pNewGO->AddComponent(pNewPlayer);			//Player Behavior
	pNewGO->AddComponent(pNewHandgun);			//Weapon

	pNewGO->SetObjectType(GameObjectType::k_player);

	return pNewGO;
}


//---------------------------------------------------------------------------------------------------------------------
//	Projectile Creation
//---------------------------------------------------------------------------------------------------------------------
GameObject* ObjectFactory::CreateProjectile(int damage, int startPosX, int startPosY, int targetPosX, int targetPosY)
{
	//Base
	GameObject* pNewGO = new GameObject(startPosX, startPosY, 0.0);
	//Renderer
	RendererComponent* pRenderComp = new RendererComponent(LoadTexture(g_kHandgunBulletTexture), pNewGO, m_pGame->GetRenderer());
	//Brain
	Projectile* pNewProjectile = new Projectile(targetPosX, targetPosY, pNewGO);
	
	pNewGO->AddComponent(pRenderComp);
	pNewGO->AddComponent(pNewProjectile);

	pNewGO->SetObjectType(GameObjectType::k_bullet);

	//Adding this guy for updates
	m_pGame->AddGameObjectForUpdates(pNewGO);
	return pNewGO;
}

//---------------------------------------------------------------------------------------------------------------------
//	Enemy Creation
//---------------------------------------------------------------------------------------------------------------------
GameObject* ObjectFactory::CreateEnemy(int posX, int posY)
{
	GameObject* pNewGO = new GameObject(posX, posY, 0.0);

	RendererComponent* pRenderComp = new RendererComponent(LoadTexture(g_kEnemyTexture), pNewGO, m_pGame->GetRenderer());
	//Brain
	Enemy* pNewEnemy = new Enemy(pNewGO, m_pGame->GetPlayer());

	pNewGO->AddComponent(pRenderComp);
	pNewGO->AddComponent(pNewEnemy);

	pNewGO->SetObjectType(GameObjectType::k_enemy);

	return pNewGO;
}
//---------------------------------------------------------------------------------------------------------------------
//	Texture Loader
//---------------------------------------------------------------------------------------------------------------------
SDL_Texture* ObjectFactory::LoadTexture(const char* textureLocation)
{
	SDL_Surface* pSurface = IMG_LoadPNG_RW(SDL_RWFromFile(textureLocation, "rb"));
	if (!pSurface)
	{
		SDL_Log("Failed to load: %s, Error: %s", textureLocation, SDL_GetError());
		return nullptr;
	}

	SDL_Texture* pTexture = SDL_CreateTextureFromSurface(m_pGame->GetRenderer()->GetRenderer(), pSurface);
	if (!pTexture)
	{
		SDL_Log("Failed to create texture: %s, Error: %s", textureLocation, SDL_GetError());
		return nullptr;
	}
	//I'm not really sure if I need it here. 
	SDL_FreeSurface(pSurface);

	return pTexture;
}