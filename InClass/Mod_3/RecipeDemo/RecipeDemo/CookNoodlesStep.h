// CookNoodlesStep.h
#ifndef __COOKNOODLESSTEP_H__
#define __COOKNOODLESSTEP_H__

#include "RecipeStep.h"
#include <iostream>

class CookNoodlesStep : public RecipeStep
{
public:
	virtual void RunStep() override
	{
		std::cout << "Cooking Noodles\n";
	}
};

#endif
