// ChopVegetablesStep.h
#ifndef __CHOPVEGETABLESSTEP_H__
#define __CHOPVEGETABLESSTEP_H__

#include "RecipeStep.h"
#include <iostream>

class ChopVegetablesStep : public RecipeStep
{
public:
	virtual void RunStep() override
	{
		std::cout << "Chopping Vegetables\n";
	}
};

#endif
