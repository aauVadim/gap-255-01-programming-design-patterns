// RecipeStep.h
#ifndef __RECIPESTEP_H__
#define __RECIPESTEP_H__

class RecipeStep
{
public:
	virtual ~RecipeStep() { }
	virtual void RunStep() = 0;
};

#endif

