// Recipe.cpp
#include "Recipe.h"
#include "RecipeStep.h"

Recipe::~Recipe()
{
	for (RecipeStep* pStep : m_steps)
	{
		delete pStep;
	}
	m_steps.clear();
}

void Recipe::AddStep(RecipeStep* pStep)
{
	m_steps.push_back(pStep);
}

void Recipe::CookRecipe()
{
	for (RecipeStep* pStep : m_steps)
	{
		pStep->RunStep();
	}
}

