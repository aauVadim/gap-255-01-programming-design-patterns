// ServeStep.h
#ifndef __SERVESTEP_H__
#define __SERVESTEP_H__

#include "RecipeStep.h"
#include <iostream>

class ServeStep : public RecipeStep
{
public:
	virtual void RunStep() override
	{
		std::cout << "Serving\n";
	}
};

#endif


