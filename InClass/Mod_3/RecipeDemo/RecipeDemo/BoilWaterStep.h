// BoilWaterStep.h
#ifndef __BOILWATERSTEP_H__
#define __BOILWATERSTEP_H__

#include "RecipeStep.h"
#include <iostream>

class BoilWaterStep : public RecipeStep
{
public:
	virtual void RunStep() override
	{
		std::cout << "Boiling Water\n";
	}
};

#endif
