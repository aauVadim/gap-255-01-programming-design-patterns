// Recipe.h
#ifndef __RECIPE_H__
#define __RECIPE_H__

#include <deque>
#include <vector>

class RecipeStep;

class Recipe
{
	std::deque<RecipeStep*> m_steps;

public:
	~Recipe();

	// public interface
	void AddStep(RecipeStep* pStep);
	void CookRecipe();
};

#endif
