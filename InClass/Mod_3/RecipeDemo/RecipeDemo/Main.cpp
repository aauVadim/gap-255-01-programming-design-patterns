// Main.cpp
#include <iostream>
#include <conio.h>
#include <time.h>

#include "Recipe.h"

#include "BoilWaterStep.h"
#include "ChopVegetablesStep.h"
#include "ServeStep.h"
#include "CookNoodlesStep.h"

void main()
{
	srand((unsigned int)time(0));

	Recipe recipe;

	RecipeStep* pStep = new BoilWaterStep;
	recipe.AddStep(pStep);
	pStep = new CookNoodlesStep;
	recipe.AddStep(pStep);
	pStep = new ChopVegetablesStep;
	recipe.AddStep(pStep);
	pStep = new ServeStep;
	recipe.AddStep(pStep);

	recipe.CookRecipe();

	_getch();
}
