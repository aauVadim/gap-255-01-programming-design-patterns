#include "DumplingRecipe.h"
#include <iostream>
#include <windows.h>

bool DumplingRecipe::DoPrep()
{
	std::cout << "Starting Prep:\n";
	if (!Recipe::DoPrep())
		return false;

	// cook the rice
	std::cout << "\tCooking Dough:\n";
	Sleep(1000);

	// cool the rice
	std::cout << "\tCooling Meat:\n";
	//Sleep(14400000);  // four hours
	Sleep(1000);

	// choose ingredients
	std::cout << "\tChoosing ingredients:\n";
	if (!ChooseIngredients())
		return false;
	std::cout << "\t\t" << "Cooking: " << m_typeOf  << std::endl;
	Sleep(1000);



	return true;
}

bool DumplingRecipe::Cook()
{
	std::cout << "Cooking:\n";
	return true;
}

bool DumplingRecipe::Eat()
{
	std::cout << "Eating:\n";
	return true;
}

bool DumplingRecipe::Cleanup()
{
	std::cout << "Cleaning up:\n";
	return true;
}


bool DumplingRecipe::ChooseIngredients()
{
	int choice = rand() % 2;
	switch (choice)
	{
	case 0:
		m_typeOf = "Chicken Dumplings";
		break;
	case 1:
		m_typeOf = "Beef Dumplings";
		break;
	default:
		m_typeOf = "Sweet Dumplings";
		break;
	}

	return true;
}
