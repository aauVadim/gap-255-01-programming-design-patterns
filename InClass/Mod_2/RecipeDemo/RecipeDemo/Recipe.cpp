// Recipe.cpp
#include "Recipe.h"
#include <iostream>

void Recipe::CookRecipe()
{
	if (!DoPrep())
		return;

	if (!Cook())
		return;

	if (!Eat())
		return;

	if (!Cleanup())
		return;
}

bool Recipe::DoPrep()
{
	std::cout << "Starting Prep:\n";
	return true;
}

bool Recipe::Cook()
{
	std::cout << "Cooking:\n";
	return true;
}

bool Recipe::Eat()
{
	std::cout << "Eating:\n";
	return true;
}

bool Recipe::Cleanup()
{
	std::cout << "Cleaning up:\n";
	return true;
}

