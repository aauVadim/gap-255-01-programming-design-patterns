// FriedRiceRecipe.h
#include "Recipe.h"
#include <string>

class FriedRiceRecipe : public Recipe
{
	std::string m_ingredientList;

public:
	virtual const char* GetName() const override { return "Fried Rice"; }

protected:
	virtual bool DoPrep() override;
	virtual bool Cook() override;

private:
	bool ChooseIngredients();
};
