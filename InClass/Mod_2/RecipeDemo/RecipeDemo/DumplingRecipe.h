#include "Recipe.h"
#include <string>
class DumplingRecipe : public Recipe
{
	std::string m_typeOf;
public: 
	virtual const char* GetName() const override { return "Dumplings "; }
protected:
	virtual bool DoPrep();
	virtual bool Cook();
	virtual bool Eat();
	virtual bool Cleanup();
private:
	bool ChooseIngredients();
};