// Main.cpp
#include <iostream>
#include <conio.h>
#include <time.h>

#include "FriedRiceRecipe.h"
#include "DumplingRecipe.h"

#define _VO_



void main()
{
	srand((unsigned int)time(0));

	

#ifdef _REZ_
//#define _REZ_
	Recipe* pRecipe = new FriedRiceRecipe;
#endif

#ifdef _VO_
	Recipe* pRecipe = new DumplingRecipe;
#endif

	//std::cout << "Size: " << sizeof(Recipe) << std::endl;
	//std::cout << "Size: " << sizeof(FriedRiceRecipe) << std::endl;

	pRecipe->CookRecipe();

	delete pRecipe;
	pRecipe = nullptr;

	_getch();
}
