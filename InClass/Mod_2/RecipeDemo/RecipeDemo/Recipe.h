// Recipe.h
#ifndef __RECIPE_H__
#define __RECIPE_H__

class Recipe
{
public:
	virtual const char* GetName() const { return "<NO NAME>"; }

	// public interface
	void CookRecipe();

protected:
	virtual bool DoPrep();
	virtual bool Cook();
	virtual bool Eat();
	virtual bool Cleanup();
};

#endif
