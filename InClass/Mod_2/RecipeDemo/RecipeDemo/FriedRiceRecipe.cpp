// FriedRiceRecipe.cpp
#include "FriedRiceRecipe.h"
#include <iostream>
#include <Windows.h>  // :(

bool FriedRiceRecipe::DoPrep()
{
	if (!Recipe::DoPrep())
		return false;

	// cook the rice
	std::cout << "\tCooking Rice:\n";
	Sleep(1000);

	// cool the rice
	std::cout << "\tCooling Rice:\n";
	//Sleep(14400000);  // four hours
	Sleep(1000);

	// choose ingredients
	std::cout << "\tChoosing ingredients:\n";
	if (!ChooseIngredients())
		return false;
	std::cout << "\t\t" << m_ingredientList << std::endl;
	Sleep(1000);

	return true;
}

bool FriedRiceRecipe::Cook()
{
	if (!Recipe::Cook())
		return false;

	std::cout << "\t\tCooking " << m_ingredientList << std::endl;

	return true;
}

bool FriedRiceRecipe::ChooseIngredients()
{
	int choice = rand() % 3;

	switch (choice)
	{
		case 0:
			m_ingredientList = "egg, chicken, corn, string beans, green onions";
			break;
		case 1:
			m_ingredientList = "green onions, egg, msg";
			break;
		case 2:
			m_ingredientList = "msg";
			break;
		default:
			std::cout << " :( ";
			return false;
	}

	return true;
}







