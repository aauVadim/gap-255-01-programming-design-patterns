﻿//My
#include "UI.h"
#include "Game.h"
//System's
#include <iostream>
#include <windows.h>
#include <string>
#include <conio.h>
#include <stdio.h>
#include <fstream>
#include <sstream>
#include <vector>
#include <algorithm>
///UNICODE
#include <io.h>		//used for Unicode Characters
#include <fcntl.h>	//used for Unicode Characters

#include<stdio.h>


using namespace std;

//HANDLE console = GetStdHandle(STD_OUTPUT_HANDLE); //Windows console handler, used to change colors. 




void UI::DrawDescription()
{
	cout << "Welcome to the Minesweeper game. \n";
	cout << "The rules are simple, mark/clear all mines without triggiring them \n";
	cout << "The Controlls: \n";
	cout << "W, A, S, D - Move player cursor around the game field \n";
	cout << "Space - Open the tile \n";
	cout << "E - Place teh Marker \n";
}

void UI::DrawGreeting()
{

	//To sad that I never got a chance to plug this in. 
	//
	/*
	wcout <<	"███╗   ███╗██╗███╗   ██╗███████╗███████╗██╗    ██╗███████╗███████╗██████╗ ███████╗██████╗	\n";
	wcout <<	"████╗ ████║██║████╗  ██║██╔════╝██╔════╝██║    ██║██╔════╝██╔════╝██╔══██╗██╔════╝██╔══██╗ \n";
	wcout <<	"██╔████╔██║██║██╔██╗ ██║█████╗  ███████╗██║ █╗ ██║█████╗  █████╗  ██████╔╝█████╗  ██████╔╝ \n";
	wcout <<	"██║╚██╔╝██║██║██║╚██╗██║██╔══╝  ╚════██║██║███╗██║██╔══╝  ██╔══╝  ██╔═══╝ ██╔══╝  ██╔══██╗ \n";
	wcout <<	"██║ ╚═╝ ██║██║██║ ╚████║███████╗███████║╚███╔███╔╝███████╗███████╗██║     ███████╗██║  ██║ \n";	
	wcout <<	"╚═╝     ╚═╝╚═╝╚═╝  ╚═══╝╚══════╝╚══════╝ ╚══╝╚══╝ ╚══════╝╚══════╝╚═╝     ╚══════╝╚═╝  ╚═╝ \n";
	*/
	cout << "  " << endl;
	cout << "-- M I N E S W E E P E R --\n";
	cout << "  " << endl;

}

int UI::UpdateScore(int& playerScore)
{
	cout << "Player score: " << playerScore << endl;
	return playerScore; 
}

void UI::SelectDifficultyLevel()
{
	cout << "  " << endl;
	cout << "Plese select difficultu level \n"; 
	cout << "1. Easy		- Small size map \n";
	cout << "2. Medium	- Medium size map \n";
	cout << "3. Hard		- Large size map \n\n";
	cout << "... or press S to see Scoreboard. \n";
}

void UI::DrawMainMenu()
{
	system("CLS");
	//Greeting
	DrawGreeting();
	//Description
	DrawDescription();
	//Difficulty Level
	SelectDifficultyLevel();

	int input = _getch();
	switch (input)
	{
	case '1':
		std::cout << "Map: Easy. \n";
		GetDifficultyLevel() = 1;
		break;
	case '2':
		std::cout << "Map: Medium. \n";
		GetDifficultyLevel() = 2; 
		break;
	case '3':
		std::cout << "Map: Hard. \n";
		GetDifficultyLevel() = 3;
		break;
	case 's':
		ReadScoresFromFile();
		break;
	default:
		std::cout << "Bad Input!. \n";
		break;
	}
}

int& UI::GetDifficultyLevel()
{
	return m_difficultyLevel;
}
//Maybe make this work with new librarie
int UI::DrawGameStatisticScreen(const int& bombsOnTheField, const int& playerMarkedBombs, const int& playerMarkersLeft)
{
	wcout << "+	----------------------------------------------------------	+" << endl;
	wcout << "|	 Player Markers Left: " << playerMarkersLeft << "					|" << endl;
	wcout << "+	----------------------------------------------------------	+" << endl;
	return 0; 
}
//Game Over Screen - Can be Win or Lose. Depends on the amoun of bombs marked
void UI::GameOverScreen(const int& bombsOnTheField, const int& markedBombs, const int& tilesLeft, Player& playerReference)
{
	//If you marked all of the bombs. WIN 
	if (bombsOnTheField == markedBombs)
	{
		cout << "--YOU WON!--" << endl;
		cout << "Your score: " << playerReference.SetPlayerScore(markedBombs, tilesLeft) << endl;
		cout << "Bombs marked: " << markedBombs << "\\" << bombsOnTheField << endl;
		cout << "Closed tiles left: " << tilesLeft << endl;

		cout << "Would you like to save the score?" << endl;
		int input = _getch();
		if (input == 'Y' || input == 'y')
		{
			std::string playerName; 
			cout << "Please enter your name: " << endl;
			cin >> playerName;
			playerReference.m_playerName = playerName;
			//Saving to a file
			SaveProgressToFile(playerReference);
			ReadScoresFromFile();
		}
		else
		{
			DrawMainMenu();
		}
	}
	//Else Lose!
	else
	{
		cout << "YOU LOST!" << endl;
		cout << "no score for you" << endl;
		//Just to have user to press a button
		cout << "Press Unikey to continue....." << endl;
		_getch();
		DrawMainMenu();
	}
}
//Saves progress to existing Score.txt. If none exists - creates one. 
void UI::SaveProgressToFile(Player& currentPlayer)
{
	ofstream scoreBoard;
	scoreBoard.open("Scores.txt", iostream::app | iostream::out);
	if (scoreBoard.is_open())
	{
		scoreBoard << currentPlayer.m_playerName << " Score: " << currentPlayer.m_score << '\n';
		scoreBoard.close();
	}
	//If you did not find the file
	else
	{ 
		//Create it
		ofstream scoreBoard("Scores.txt"); 
		scoreBoard.open("Scores.txt");
		if (scoreBoard.is_open())
		{
			//TODO: Run some magical functions to sort this shit out
			scoreBoard << currentPlayer.m_playerName << " Score: " << currentPlayer.m_score << '\n';
			scoreBoard.close();
		}
		else
			cout << "SaveProgressToFile() couldn't open Scores.txt file. FIXME" << endl;

	}
}
//Little ugly, might want to fix later
//Pretti much shows the scoreboard
void UI::ReadScoresFromFile()
{
	//Quick struct to store scores
	struct ScoreRecords
	{
		std::string m_name;
		int m_score;
		ScoreRecords(std::string name,int score)
		{
			m_name = name;
			m_score = score;
		}
	};

	//Sorting struct
	//Returnd bigger value
	struct SortStruct
	{
		bool operator()(const ScoreRecords& first, const ScoreRecords& second) const
		{
			return first.m_score > second.m_score;
		}
	};

	system("CLS");
	ifstream scoreBoard;
	scoreBoard.open("Scores.txt");
	if (scoreBoard.is_open())
	{
		//TODO: Read fro file
		std::string line;
		vector<ScoreRecords> sortingVec;

		while (getline(scoreBoard, line))
		{
			int score = 0; 
			string name; 
			string garbage; 
			stringstream ss(line);
			//Reading the line
			ss >> name >> garbage >> score;
			ScoreRecords newRecord(name, score);
			sortingVec.push_back(newRecord);
		}
		std::sort(sortingVec.begin(), sortingVec.end(), SortStruct());
		cout << "Player scores: \n\n"; 
		for (auto iter = sortingVec.begin(); iter != sortingVec.end(); ++iter)
		{
			cout << iter->m_name << " Score:		" << iter->m_score << endl;
		}
		scoreBoard.close();
	}
	//If you did not find the file
	else
		cout << "No scores have been set yet. Play first" << endl;

	cout << "\n\nPress Unikey to continue....." << endl;
	_getch();
	DrawMainMenu();
}

/* COLORS
0   BLACK
1   BLUE
2   GREEN
3   CYAN
4   RED
5   MAGENTA
6   BROWN
7   LIGHTGRAY
8   DARKGRAY
9   LIGHTBLUE
10  LIGHTGREEN
11  LIGHTCYAN
12  LIGHTRED
13  LIGHTMAGENTA
14  YELLOW
15  WHITE
*/