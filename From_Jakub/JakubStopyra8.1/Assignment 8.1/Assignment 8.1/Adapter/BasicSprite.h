// BasicSprite.h
#ifndef BASICSPRITE_H
#define BASICSPRITE_H

#include "Renderable.h"

class BasicSprite : public Renderable
{
    char m_image;

public:
    virtual bool Load(const char* filename) override;
    virtual void Render() override;
};

#endif
