// Renderable.h
#ifndef RENDERABLE_H
#define RENDERABLE_H

class Renderable
{
public:
    // Destructor.
    virtual ~Renderable() { }

    // Loads the image for the sprite.
    //     -filename: The name of the file, which should be an ASCII file that contains a simple character.  This
    //                is the character to render.
    //     -return:   Returns true if successful, false if not.
    virtual bool Load(const char* filename) = 0;

    // Renders the object, followed by a new line.
    virtual void Render() = 0;
};

#endif
