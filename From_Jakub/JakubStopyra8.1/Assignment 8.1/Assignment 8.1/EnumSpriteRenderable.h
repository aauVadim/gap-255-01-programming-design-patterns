//EnumSpriteRenderable.h

#ifndef ENUMSPRITERENDERABLE_H
#define ENUMSPRITERENDERABLE_H

#include "Adapter\Renderable.h"
#include "EnumSprite\EnumSprite.h"
class EnumSpriteRenderable : public Renderable
{
private:
	EnumSprite m_enumSprite;

public:
	virtual bool Load(const char* filename) override;
	virtual void Render() override;

};

#endif