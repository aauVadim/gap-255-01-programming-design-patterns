//EnumSpriteRenderable.cpp
#include <iostream>
#include "fstream"
#include "EnumSpriteRenderable.h"
#include <string>
enum class EnumSprite::Type;
bool EnumSpriteRenderable::Load(const char* filename)
{
	EnumSprite::Type type;
	std::fstream myFile;
	myFile.open(filename);
	char myChar = myFile.get();
	switch (myChar)
	{
	case 'A':
		type = EnumSprite::Type::k_a;
		break;
	case 'B':
		type = EnumSprite::Type::k_b;
		break;
	case 'C':
		type = EnumSprite::Type::k_c;
		break;
	case 'D':
		type = EnumSprite::Type::k_d;
		break;
	case 'E':
		type = EnumSprite::Type::k_e;
		break;
	default:
		type = EnumSprite::Type::k_unknown;
		break;
	}
	
	m_enumSprite.Init(type);
	return true;
}

void EnumSpriteRenderable::Render()
{
	m_enumSprite.Draw();
	std::cout << std::endl;
}