#include "EnumAdapterClient.h"

#include "EnumSpriteRenderable.h"

Renderable* EnumAdapterClient::CreateRenderable() const
{
	Renderable* returnRenderable = new EnumSpriteRenderable();
	return returnRenderable;
}
