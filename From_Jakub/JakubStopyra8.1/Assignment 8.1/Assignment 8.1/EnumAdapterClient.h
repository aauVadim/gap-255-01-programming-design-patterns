// AdapterClient.h
#ifndef ENUMADAPTERCLIENT_H
#define ENUMADAPTERCLIENT_H

#include "Adapter\AdapterClient.h"

class EnumAdapterClient : public AdapterClient
{
	virtual Renderable* CreateRenderable() const override;
	//bool Init();
};
#endif