// EnumSprite.h
#ifndef ENUMSPRITE_H
#define ENUMSPRITE_H

// This is the adaptee, which is the thing you're adapting.
class EnumSprite
{
public:
    enum class Type
    {
        k_a,
        k_b,
        k_c,
        k_d,
        k_e,
        k_unknown,
        k_count,
    };

private:
    static const char s_typeToImageMap[(int)Type::k_count];
    Type m_type;

public:
    EnumSprite() : m_type(Type::k_unknown) { }

    // Initializes the sprite.  Takes in the the type of sprite this is, which determines the image.
    void Init(Type type);

    // Draws the sprite.
    void Draw();
};

#endif
